package security;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfiguration;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import utils.Constantes;

/**
 * 
 */

/**
 * @author pj
 *
 */
@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
    JwtTokenProvider jwtTokenProvider;
	
    public  SpringSecurityConfig() {
    	super();
    }
    	 
	protected void configure(final HttpSecurity http) throws Exception 
	{
	
	  //Hacer list de string con los distintos roles
//		ArrayList<String> constantesUser = new ArrayList();
//		constantesUser.add(String.valueOf(Constantes.ROLE_PLUGIN));
		String PLUGIN = String.valueOf(Constantes.ROLE_PLUGIN);
		
		http
        //HTTP Basic authentication
        .httpBasic().disable()
        .csrf().disable()
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .authorizeRequests()
        .antMatchers("/domainUser/login", "public", "secret","hello-world").permitAll()		
        .antMatchers(HttpMethod.GET, "/fingerPrint/**").permitAll()
        .antMatchers(HttpMethod.POST, "/fingerPrint").permitAll()
//        .antMatchers(HttpMethod.PUT, "/fingerPrint/**").hasRole(PLUGIN)
//        .antMatchers(HttpMethod.PATCH, "/fingerPrint/**").hasRole(PLUGIN)
//        .antMatchers(HttpMethod.DELETE, "/fingerPrint/**").hasRole(PLUGIN)
        .and()
        .formLogin().disable()
        .httpBasic().disable()
        .apply(new JwtConfigurer(jwtTokenProvider));
	}
	
	
}
