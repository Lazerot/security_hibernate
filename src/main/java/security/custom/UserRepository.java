/**
 * 
 */
package security.custom;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import bbdd.tables.DomainuserEntity;

/**
 * @author pj
 *
 */
public interface UserRepository extends JpaRepository <DomainuserEntity,Long>
{

	Optional<DomainuserEntity> findByUsername(String username);
	
	Optional<DomainuserEntity> findByUseremail(String useremail);
}
