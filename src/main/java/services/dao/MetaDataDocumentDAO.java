/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.MetadatadocumentEntity;

/**
 * @author pmartinez
 *
 */
public interface MetaDataDocumentDAO {

	public MetadatadocumentEntity selectbyMetadatadocumentEntity(int id);

	public List<MetadatadocumentEntity> selectAll();

	public void insert (MetadatadocumentEntity metadatadocument);
	
	public void update (MetadatadocumentEntity metadatadocument);
	
	public void delete (MetadatadocumentEntity metadatadocument);

}
