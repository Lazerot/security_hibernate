/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.FsmmultipleopstatusEntity;

/**
 * @author pmartinez
 *
 */
public interface FsmMultipleOpStatusDAO {

	public FsmmultipleopstatusEntity selectbyFsmmultipleopstatusEntity(int id);

	public List<FsmmultipleopstatusEntity> selectAll();

	public void insert (FsmmultipleopstatusEntity fsmmultipleopstatus);
	
	public void update (FsmmultipleopstatusEntity fsmmultipleopstatus);
	
	public void delete (FsmmultipleopstatusEntity fsmmultipleopstatus);

}
