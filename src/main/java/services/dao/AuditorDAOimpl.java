/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.AuditorEntity;

/**
 * @author pmartinez
 *
 */
public class AuditorDAOimpl implements AuditorDAO {

	@Override
	public AuditorEntity selectbyAuditorEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		AuditorEntity auditorEntity = (AuditorEntity) session.get(AuditorEntity.class, id);
		return auditorEntity;
	}

	@Override
	public List<AuditorEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<AuditorEntity> criteria = builder.createQuery(AuditorEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<AuditorEntity> root = criteria.from(AuditorEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<AuditorEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<AuditorEntity> auditorEntity = q.getResultList();

		return auditorEntity;
	}

	@Override
	public void insert(AuditorEntity auditor) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(auditor);
		auditor.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(AuditorEntity auditor) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(auditor);
		session.getTransaction().commit();
	}

	@Override
	public void delete(AuditorEntity auditor) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(auditor);
		session.getTransaction().commit();	
	}

}
