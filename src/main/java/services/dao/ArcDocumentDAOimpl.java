/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.ArcdocumentEntity;

/**
 * @author pmartinez
 *
 */
public class ArcDocumentDAOimpl implements ArcDocumentDAO{

	@Override
	public ArcdocumentEntity selectbyArcdocumentEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		ArcdocumentEntity arcDocumentEntity = (ArcdocumentEntity) session.get(ArcdocumentEntity.class, id);

		return arcDocumentEntity;
	}

	@Override
	public List<ArcdocumentEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<ArcdocumentEntity> criteria = builder.createQuery(ArcdocumentEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<ArcdocumentEntity> root = criteria.from(ArcdocumentEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<ArcdocumentEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<ArcdocumentEntity> arcDocumentEntity = q.getResultList();

		return arcDocumentEntity;
	}

	@Override
	public void insert(ArcdocumentEntity arcdocument) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id =  (int) session.save(arcdocument);
		arcdocument.setId(id);
		session.getTransaction().commit();
		
	}

	@Override
	public void update(ArcdocumentEntity arcdocument) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(arcdocument);
		session.getTransaction().commit();
		
	}

	@Override
	public void delete(ArcdocumentEntity arcdocument) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(arcdocument);
		session.getTransaction().commit();
		
	}

}
