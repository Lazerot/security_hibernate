/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.OperacionauditorialoteEntity;

/**
 * @author pmartinez
 *
 */
public interface OperacionAuditoriaLoteDAO {

	public OperacionauditorialoteEntity selectbyOperacionauditorialoteEntity(int id);

	public List<OperacionauditorialoteEntity> selectAll();

	public void insert (OperacionauditorialoteEntity operacionauditorialote);
	
	public void update (OperacionauditorialoteEntity operacionauditorialote);
	
	public void delete (OperacionauditorialoteEntity operacionauditorialote);
}
