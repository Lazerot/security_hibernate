package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.TemplatetypeEntity;

public class TemplateTypeDAOimpl implements TemplateTypeDAO{

	@Override
	public TemplatetypeEntity selectbyTemplatetypeEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		TemplatetypeEntity templateTypeEntity =(TemplatetypeEntity) session.get(TemplatetypeEntity.class, id);
		return templateTypeEntity;
	}

	@Override
	public List<TemplatetypeEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<TemplatetypeEntity> criteria = builder.createQuery(TemplatetypeEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<TemplatetypeEntity> root = criteria.from(TemplatetypeEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<TemplatetypeEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<TemplatetypeEntity> templateTypeEntity = q.getResultList();

		return templateTypeEntity;
	}

	@Override
	public void insert(TemplatetypeEntity templatetype) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(templatetype);
		templatetype.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(TemplatetypeEntity templatetype) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(templatetype);
		session.getTransaction().commit();
	}

	@Override
	public void delete(TemplatetypeEntity templatetype) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(templatetype);
		session.getTransaction().commit();
	}

}
