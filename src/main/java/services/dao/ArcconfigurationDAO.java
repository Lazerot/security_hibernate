/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.ArcconfigurationEntity;

/**
 * @author pmartinez
 *
 */
public interface ArcconfigurationDAO {
	
	public ArcconfigurationEntity selectbyAllowedcasEntity(int id);

	public List<ArcconfigurationEntity> selectAll();

	public void insert (ArcconfigurationEntity arcconfiguration);
	
	public void update (ArcconfigurationEntity arcconfiguration);
	
	public void delete (ArcconfigurationEntity arcconfiguration);

}
