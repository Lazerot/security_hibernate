/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.FsmopstatusEntity;

/**
 * @author pmartinez
 *
 */
public interface FsmOpStatusDAO {

	public FsmopstatusEntity selectbyFsmopstatusEntity(int id);

	public List<FsmopstatusEntity> selectAll();

	public void insert (FsmopstatusEntity fsmstatus);
	
	public void update (FsmopstatusEntity fsmstatus);
	
	public void delete (FsmopstatusEntity fsmstatus);

}
