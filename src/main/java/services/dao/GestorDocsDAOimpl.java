/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.GestordocsEntity;

/**
 * @author pmartinez
 *
 */
public class GestorDocsDAOimpl implements GestorDocsDAO {

	@Override
	public GestordocsEntity selectbyGestordocsEntity(int uuid) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		GestordocsEntity gestorDocsEntity = (GestordocsEntity) session.get(GestordocsEntity.class, uuid);
		return gestorDocsEntity;
	}

	@Override
	public List<GestordocsEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<GestordocsEntity> criteria = builder.createQuery(GestordocsEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<GestordocsEntity> root = criteria.from(GestordocsEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<GestordocsEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<GestordocsEntity> gestorDocsEntity = q.getResultList();

		return gestorDocsEntity;
	}

	@Override
	public void insert(GestordocsEntity gestordocs) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int uuid = (int) session.save(gestordocs);
		gestordocs.setUuid(uuid);
		session.getTransaction().commit();
	}

	@Override
	public void update(GestordocsEntity gestordocs) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(gestordocs);
		session.getTransaction().commit();
	}

	@Override
	public void delete(GestordocsEntity gestordocs) {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(gestordocs);
		session.getTransaction().commit();
	}

}
