/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.VcappointmentEntity;

/**
 * @author pmartinez
 *
 */
public class VcapPointmentDAOimpl implements VcapPointmentDAO{

	@Override
	public VcappointmentEntity selectbyVcappointmentEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		VcappointmentEntity vCapPointmentEntity = (VcappointmentEntity) session.get(VcappointmentEntity.class, id);
		return vCapPointmentEntity;
	}

	@Override
	public List<VcappointmentEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<VcappointmentEntity> criteria = builder.createQuery(VcappointmentEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<VcappointmentEntity> root = criteria.from(VcappointmentEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<VcappointmentEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<VcappointmentEntity> vCapPointmentEntity = q.getResultList();

		return vCapPointmentEntity;
	}

	@Override
	public void insert(VcappointmentEntity vcappointment) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(vcappointment);
		vcappointment.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(VcappointmentEntity vcappointment) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(vcappointment);
		session.getTransaction().commit();
	}

	@Override
	public void delete(VcappointmentEntity vcappointment) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(vcappointment);
		session.getTransaction().commit();
	}

}
