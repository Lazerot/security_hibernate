/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.VcshareddocumentEntity;

/**
 * @author pmartinez
 *
 */
public class VcSharedDocumentDAOimpl implements VcSharedDocumentDAO{

	@Override
	public VcshareddocumentEntity selectbyVcshareddocumentEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		VcshareddocumentEntity vcSharedDocumentEntity = (VcshareddocumentEntity) session.get(VcshareddocumentEntity.class, id);
		return vcSharedDocumentEntity;
	}

	@Override
	public List<VcshareddocumentEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<VcshareddocumentEntity> criteria = builder.createQuery(VcshareddocumentEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<VcshareddocumentEntity> root = criteria.from(VcshareddocumentEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<VcshareddocumentEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<VcshareddocumentEntity> vcshareddocumentEntities = q.getResultList();

		return vcshareddocumentEntities;
	}

	@Override
	public void insert(VcshareddocumentEntity vcshareddocument) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(vcshareddocument);
		vcshareddocument.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(VcshareddocumentEntity vcshareddocument) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(vcshareddocument);
		session.getTransaction().commit();
	}

	@Override
	public void delete(VcshareddocumentEntity vcshareddocument) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(vcshareddocument);
		session.getTransaction().commit();
	}

}
