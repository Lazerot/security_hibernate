/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.VcconnectionEntity;

/**
 * @author pmartinez
 *
 */
public class VcConnectionDAOimpl implements VcConnectionDAO{

	@Override
	public VcconnectionEntity selectbyVcconnectionEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		VcconnectionEntity vcConnectionEntity = (VcconnectionEntity) session.get(VcconnectionEntity.class, id);
		return vcConnectionEntity;
	}

	@Override
	public List<VcconnectionEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<VcconnectionEntity> criteria = builder.createQuery(VcconnectionEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<VcconnectionEntity> root = criteria.from(VcconnectionEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<VcconnectionEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<VcconnectionEntity> vcConnectionEntity = q.getResultList();

		return vcConnectionEntity;
	}

	@Override
	public void insert(VcconnectionEntity vcconnection) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(vcconnection);
		vcconnection.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(VcconnectionEntity vcconnection) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(vcconnection);
		session.getTransaction().commit();
	}

	@Override
	public void delete(VcconnectionEntity vcconnection) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(vcconnection);
		session.getTransaction().commit();
	}

}
