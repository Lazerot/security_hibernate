package services.dao;

import java.util.List;

import bbdd.tables.CuentausuarioEntity;

/**
 * @author pmartinez
 *
 */

public interface CuentaUsuarioDAO {

	public CuentausuarioEntity selectbyCuentausuarioEntity(int idusuario);
	
	public List<CuentausuarioEntity> selectAll();
	
	public void insert (CuentausuarioEntity cuentausuario);
	
	public void update (CuentausuarioEntity cuentausuario);
	
	public void delete (CuentausuarioEntity cuentausuario);
}
