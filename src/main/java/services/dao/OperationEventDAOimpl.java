/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.OperationeventEntity;

/**
 * @author pmartinez
 *
 */
public class OperationEventDAOimpl implements OperationEventDAO{

	@Override
	public OperationeventEntity selectbyOperationeventEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		OperationeventEntity operationEventEntity = (OperationeventEntity) session.get(OperationeventEntity.class, id);
		return operationEventEntity;
	}

	@Override
	public List<OperationeventEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<OperationeventEntity> criteria = builder.createQuery(OperationeventEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<OperationeventEntity> root = criteria.from(OperationeventEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<OperationeventEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<OperationeventEntity> operationEventEntity = q.getResultList();

		return operationEventEntity;
	}

	@Override
	public void insert(OperationeventEntity operationevent) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(operationevent);
		operationevent.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(OperationeventEntity operationevent) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(operationevent);
		session.getTransaction().commit();
	}

	@Override
	public void delete(OperationeventEntity operationevent) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(operationevent);
		session.getTransaction().commit();
	}

}
