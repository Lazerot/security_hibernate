/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.VcconfigurationEntity;

/**
 * @author pmartinez
 *
 */
public interface VcConfigurationDAO {

	public VcconfigurationEntity selectbyVcconfigurationEntity(int id);

	public List<VcconfigurationEntity> selectAll();

	public void insert (VcconfigurationEntity vcconfiguration);
	
	public void update (VcconfigurationEntity vcconfiguration);
	
	public void delete (VcconfigurationEntity vcconfiguration);
}
