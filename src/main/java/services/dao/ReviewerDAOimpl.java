/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.ReviewerEntity;

/**
 * @author pmartinez
 *
 */
public class ReviewerDAOimpl implements ReviewerDAO{

	@Override
	public ReviewerEntity selectbyReviewerEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		ReviewerEntity reviewerEntity = (ReviewerEntity) session.get(ReviewerEntity.class, id);
		return reviewerEntity;
	}

	@Override
	public List<ReviewerEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<ReviewerEntity> criteria = builder.createQuery(ReviewerEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<ReviewerEntity> root = criteria.from(ReviewerEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<ReviewerEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<ReviewerEntity> reviewerEntity = q.getResultList();

		return reviewerEntity;
	}

	@Override
	public void insert(ReviewerEntity reviewer) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(reviewer);
		reviewer.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(ReviewerEntity reviewer) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(reviewer);
		session.getTransaction().commit();
	}

	@Override
	public void delete(ReviewerEntity reviewer) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(reviewer);
		session.getTransaction().commit();
	}

}
