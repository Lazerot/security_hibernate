/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.PolicytypeEntity;

/**
 * @author pmartinez
 *
 */
public interface PolicyTypeDAO {

	public PolicytypeEntity selectbyPolicytypeEntity(int id);

	public List<PolicytypeEntity> selectAll();

	public void insert (PolicytypeEntity policytype);
	
	public void update (PolicytypeEntity policytype);
	
	public void delete (PolicytypeEntity policytype);
}
