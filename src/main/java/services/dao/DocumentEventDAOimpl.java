/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.DocumenteventEntity;

/**
 * @author pmartinez
 *
 */
public class DocumentEventDAOimpl implements DocumentEventDAO{

	@Override
	public DocumenteventEntity selectbyDocumenteventEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		DocumenteventEntity documentEventEntity = (DocumenteventEntity) session.get(DocumenteventEntity.class, id);
		return documentEventEntity;
	}

	@Override
	public List<DocumenteventEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<DocumenteventEntity> criteria = builder.createQuery(DocumenteventEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<DocumenteventEntity> root = criteria.from(DocumenteventEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<DocumenteventEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<DocumenteventEntity> documentEventEntity = q.getResultList();

		return documentEventEntity;
	}

	@Override
	public void insert(DocumenteventEntity documentevent) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(documentevent);
		documentevent.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(DocumenteventEntity documentevent) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(documentevent);
		session.getTransaction().commit();
	}

	@Override
	public void delete(DocumenteventEntity documentevent) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(documentevent);
		session.getTransaction().commit();
	}

}
