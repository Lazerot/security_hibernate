/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.SmsstatisticsEntity;

/**
 * @author pmartinez
 *
 */
public class SmsStatisticsDAOimpl implements SmsStatisticsDAO{

	@Override
	public SmsstatisticsEntity selectbySmsstatisticsEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		SmsstatisticsEntity smsStatisticsEntity = (SmsstatisticsEntity) session.get(SmsstatisticsEntity.class, id);
		return smsStatisticsEntity;
	}

	@Override
	public List<SmsstatisticsEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<SmsstatisticsEntity> criteria = builder.createQuery(SmsstatisticsEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<SmsstatisticsEntity> root = criteria.from(SmsstatisticsEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<SmsstatisticsEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<SmsstatisticsEntity> smsStatisticsEntity = q.getResultList();

		return smsStatisticsEntity;
	}

	@Override
	public void insert(SmsstatisticsEntity smsstatistics) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(smsstatistics);
		smsstatistics.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(SmsstatisticsEntity smsstatistics) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(smsstatistics);
		session.getTransaction().commit();
	}

	@Override
	public void delete(SmsstatisticsEntity smsstatistics) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(smsstatistics);
		session.getTransaction().commit();
	}

}
