/**
 * 
 */
package services.dao;

import java.util.Date;
import java.util.List;

import bbdd.tables.OperationEntity;
import bbdd.dto.OperationDTO;

/**
 * @author pmartinez
 *
 */
public interface OperationDAO {
	
	public OperationEntity selectbyOperationEntity(int id);

	public List<OperationEntity> selectAll();
	
	public List<?> selectRestrictionsCriteria(int domainid, Date from, Date to, String state, String nif, String domainusersIN);

	public void insert (OperationEntity operation);
	
	public void update (OperationEntity operation);
	
	public void delete (OperationEntity operation);

	public List<?> fechasDatosOperacion (OperationEntity operation, OperationDTO operationDto);
}
