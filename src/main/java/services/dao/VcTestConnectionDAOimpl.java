/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.VctestconnectionEntity;

/**
 * @author pmartinez
 *
 */
public class VcTestConnectionDAOimpl implements VcTestConnectionDAO{

	@Override
	public VctestconnectionEntity selectbyVctestconnectionEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		VctestconnectionEntity vcTestConnectionEntity = (VctestconnectionEntity) session.get(VctestconnectionEntity.class, id);
		return vcTestConnectionEntity;
	}

	@Override
	public List<VctestconnectionEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<VctestconnectionEntity> criteria = builder.createQuery(VctestconnectionEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<VctestconnectionEntity> root = criteria.from(VctestconnectionEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<VctestconnectionEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<VctestconnectionEntity> vcTestConnectionEntity = q.getResultList();

		return vcTestConnectionEntity;
	}

	@Override
	public void insert(VctestconnectionEntity vctestconnection) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(vctestconnection);
		vctestconnection.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(VctestconnectionEntity vctestconnection) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(vctestconnection);
		session.getTransaction().commit();
	}

	@Override
	public void delete(VctestconnectionEntity vctestconnection) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(vctestconnection);
		session.getTransaction().commit();
	}

}
