/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.ArcjobresultsEntity;

/**
 * @author pmartinez
 *
 */
public class ArcJobResultsDAOimpl implements ArcJobResultsDAO{

	@Override
	public ArcjobresultsEntity selectbyArcjobresultsEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		ArcjobresultsEntity arcJobResultsEntity = (ArcjobresultsEntity) session.get(ArcjobresultsEntity.class, id);
		return arcJobResultsEntity;
	}

	@Override
	public List<ArcjobresultsEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<ArcjobresultsEntity> criteria = builder.createQuery(ArcjobresultsEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<ArcjobresultsEntity> root = criteria.from(ArcjobresultsEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<ArcjobresultsEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<ArcjobresultsEntity> ArcjobresultsEntity = q.getResultList();

		return ArcjobresultsEntity;
	}

	@Override
	public void insert(ArcjobresultsEntity arcjobresults) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(arcjobresults);
		arcjobresults.setId(id);
		session.getTransaction().commit();	
	}

	@Override
	public void update(ArcjobresultsEntity arcjobresults) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(arcjobresults);
		session.getTransaction().commit();	
	}

	@Override
	public void delete(ArcjobresultsEntity arcjobresults) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(arcjobresults);
		session.getTransaction().commit();	
	}

}
