/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.ConfiguracionloteEntity;

/**
 * @author pmartinez
 *
 */
public class ConfiguracionLoteDAOimpl implements ConfiguracionLoteDAO{

	@Override
	public ConfiguracionloteEntity selectbyConfiguracionloteEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		ConfiguracionloteEntity configuracionLoteEntity = (ConfiguracionloteEntity) session.get(ConfiguracionloteEntity.class, id);
		return configuracionLoteEntity;
	}

	@Override
	public List<ConfiguracionloteEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<ConfiguracionloteEntity> criteria = builder.createQuery(ConfiguracionloteEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<ConfiguracionloteEntity> root = criteria.from(ConfiguracionloteEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<ConfiguracionloteEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<ConfiguracionloteEntity> configuracionLoteEntity = q.getResultList();

		return configuracionLoteEntity;
	}

	@Override
	public void insert(ConfiguracionloteEntity configuracionlote) {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(configuracionlote);
		configuracionlote.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(ConfiguracionloteEntity configuracionlote) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(configuracionlote);
		session.getTransaction().commit();
	}

	@Override
	public void delete(ConfiguracionloteEntity configuracionlote) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(configuracionlote);
		session.getTransaction().commit();		
	}

}
