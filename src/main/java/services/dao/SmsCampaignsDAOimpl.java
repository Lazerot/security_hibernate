/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.SmscampaignsEntity;

/**
 * @author pmartinez
 *
 */
public class SmsCampaignsDAOimpl implements SmsCampaignsDAO{

	@Override
	public SmscampaignsEntity selectbySmscampaignsEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		SmscampaignsEntity smsCampaignsEntity = (SmscampaignsEntity) session.get(SmscampaignsEntity.class, id);
		return smsCampaignsEntity;
	}

	@Override
	public List<SmscampaignsEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<SmscampaignsEntity> criteria = builder.createQuery(SmscampaignsEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<SmscampaignsEntity> root = criteria.from(SmscampaignsEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<SmscampaignsEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<SmscampaignsEntity> smsCampaignsEntity = q.getResultList();

		return smsCampaignsEntity;
	}
	
	@Override
	public void insert(SmscampaignsEntity smscampaigns) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(smscampaigns);
		smscampaigns.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(SmscampaignsEntity smscampaigns) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(smscampaigns);
		session.getTransaction().commit();
	}

	@Override
	public void delete(SmscampaignsEntity smscampaigns) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(smscampaigns);
		session.getTransaction().commit();
	}

}
