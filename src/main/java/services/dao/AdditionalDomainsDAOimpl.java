/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.AdditionaldomainsEntity;

/**
 * @author pmartinez
 *
 */
public class AdditionalDomainsDAOimpl implements AdditionalDomainsDAO {

	@Override
	public AdditionaldomainsEntity selectbyAdditionaldomainsEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		AdditionaldomainsEntity additionalDomainsEntity = (AdditionaldomainsEntity) session.get(AdditionaldomainsEntity.class, id);
		
		return additionalDomainsEntity;
	}
	
	@Override
	public List<AdditionaldomainsEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<AdditionaldomainsEntity> criteria = builder.createQuery(AdditionaldomainsEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<AdditionaldomainsEntity> root = criteria.from(AdditionaldomainsEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<AdditionaldomainsEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<AdditionaldomainsEntity> additionalDomainsEntity = q.getResultList();

		return additionalDomainsEntity;
	}

	@Override
	public void insert(AdditionaldomainsEntity addiDomain) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(addiDomain);
		addiDomain.setId(id);
		session.getTransaction().commit();
		
	}

	@Override
	public void update(AdditionaldomainsEntity addiDomain) {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(addiDomain);
		session.getTransaction().commit();
	}

	@Override
	public void delete(AdditionaldomainsEntity addiDomain) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(addiDomain);
		session.getTransaction().commit();
		
	}

	
	
}
