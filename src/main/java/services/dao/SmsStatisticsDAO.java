/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.SmsstatisticsEntity;

/**
 * @author pmartinez
 *
 */
public interface SmsStatisticsDAO {

	public SmsstatisticsEntity selectbySmsstatisticsEntity(int id);

	public List<SmsstatisticsEntity> selectAll();

	public void insert (SmsstatisticsEntity smsstatistics);
	
	public void update (SmsstatisticsEntity smsstatistics);
	
	public void delete (SmsstatisticsEntity smsstatistics);
}
