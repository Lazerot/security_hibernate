/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.NotificationeventEntity;

/**
 * @author pmartinez
 *
 */
public class NotificationEventDAOimpl implements NotificationEventDAO{

	@Override
	public NotificationeventEntity selectbyNotificationeventEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		NotificationeventEntity notificationEventEntity = (NotificationeventEntity) session.get(NotificationeventEntity.class, id);
		return notificationEventEntity;
	}

	@Override
	public List<NotificationeventEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<NotificationeventEntity> criteria = builder.createQuery(NotificationeventEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<NotificationeventEntity> root = criteria.from(NotificationeventEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<NotificationeventEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<NotificationeventEntity> notificationEventEntity = q.getResultList();

		return notificationEventEntity;
	}

	@Override
	public void insert(NotificationeventEntity notificationevent) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(notificationevent);
		notificationevent.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(NotificationeventEntity notificationevent) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(notificationevent);
		session.getTransaction().commit();
	}

	@Override
	public void delete(NotificationeventEntity notificationevent) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(notificationevent);
		session.getTransaction().commit();
	}

}
