/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.ActatemplateoptypeEntity;

/**
 * @author pmartinez
 *
 */
public interface ActaTemplateOpTypeDAO {

public ActatemplateoptypeEntity selectbyActatemplateoptypeEntity(int id);
	
	public List<ActatemplateoptypeEntity> selectAll();
	
	public void insert (ActatemplateoptypeEntity actaOpType);
	
	public void update (ActatemplateoptypeEntity actaOpType);
	
	public void delete (ActatemplateoptypeEntity actaOpType);

}
