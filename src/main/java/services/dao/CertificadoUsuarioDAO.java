/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.CertificadousuarioEntity;

/**
 * @author pmartinez
 *
 */
public interface CertificadoUsuarioDAO {

	public CertificadousuarioEntity selectbyCertificadousuarioEntity(int idcertificado);

	public List<CertificadousuarioEntity> selectAll();

	public void insert (CertificadousuarioEntity certificadousuario);
	
	public void update (CertificadousuarioEntity certificadousuario);
	
	public void delete (CertificadousuarioEntity certificadousuario);

}
