package services.dao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
//Este import es el necesario a partir de las version 5.x....
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;;

public class HibernateUtils {

	//A partir de la version 5.x... hay que usar otros parametros para los session Factory
	 static SessionFactory sessionFactory = buildSessionFactory();
	 static Session session;
	 
	 //Creamos la SessionFactory a traves del archivo de configuracion hibernate.cfg.xml
	 private static SessionFactory buildSessionFactory (){

		 ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                 .configure("hibernate.cfg.xml").build();

         // Create a metadata sources using the specified service registry.
         Metadata metadata = new MetadataSources(serviceRegistry).getMetadataBuilder().build();

         return metadata.getSessionFactoryBuilder().build();
	 
	 }
	 
	 public static SessionFactory getSessionFactory() {
		 return sessionFactory;
	 }
	 
	 //Obtenemos la sesion actual
	 public static Session getSession() {
		 if(null == session) 
		 {
			 session = sessionFactory.openSession();
			 
		 }
		 return session;
	 }
}
