/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.CertificadousuarioEntity;

/**
 * @author pmartinez
 *
 */
public class CertificadoUsuarioDAOimpl implements CertificadoUsuarioDAO {

	@Override
	public CertificadousuarioEntity selectbyCertificadousuarioEntity(int idcertificado) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		CertificadousuarioEntity certificadoUsuarioEntity = (CertificadousuarioEntity) session.get(CertificadousuarioEntity.class, idcertificado);
		return certificadoUsuarioEntity;
	}

	@Override
	public List<CertificadousuarioEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<CertificadousuarioEntity> criteria = builder.createQuery(CertificadousuarioEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<CertificadousuarioEntity> root = criteria.from(CertificadousuarioEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<CertificadousuarioEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<CertificadousuarioEntity> certificadoUsuarioEntity = q.getResultList();

		return certificadoUsuarioEntity;
	}

	@Override
	public void insert(CertificadousuarioEntity certificadousuario) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int idcertificado = (int) session.save(certificadousuario);
		certificadousuario.setIdcertificado(idcertificado);
		session.getTransaction().commit();
	}

	@Override
	public void update(CertificadousuarioEntity certificadousuario) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(certificadousuario);
		session.getTransaction().commit();
	}

	@Override
	public void delete(CertificadousuarioEntity certificadousuario) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(certificadousuario);
		session.getTransaction().commit();
	}

}
