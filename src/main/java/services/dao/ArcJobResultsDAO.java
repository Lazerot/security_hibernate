/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.ArcjobresultsEntity;

/**
 * @author pmartinez
 *
 */
public interface ArcJobResultsDAO {

	public ArcjobresultsEntity selectbyArcjobresultsEntity(int id);

	public List<ArcjobresultsEntity> selectAll();

	public void insert (ArcjobresultsEntity arcjobresults);
	
	public void update (ArcjobresultsEntity arcjobresults);
	
	public void delete (ArcjobresultsEntity arcjobresults);

}
