/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.SmscampaignsEntity;

/**
 * @author pmartinez
 *
 */
public interface SmsCampaignsDAO {

	public SmscampaignsEntity selectbySmscampaignsEntity(int id);

	public List<SmscampaignsEntity> selectAll();

	public void insert (SmscampaignsEntity smscampaigns);
	
	public void update (SmscampaignsEntity smscampaigns);
	
	public void delete (SmscampaignsEntity smscampaigns);
}
