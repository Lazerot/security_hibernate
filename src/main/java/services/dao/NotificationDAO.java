/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.NotificationEntity;

/**
 * @author pmartinez
 *
 */
public interface NotificationDAO {
	
	public NotificationEntity selectbyArcdocumentEntity(int id);

	public List<NotificationEntity> selectAll();

	public void insert (NotificationEntity notification);
	
	public void update (NotificationEntity notification);
	
	public void delete (NotificationEntity notification);

}
