/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.VcagentreplaceEntity;

/**
 * @author pmartinez
 *
 */
public interface VcagEntrePlaceDAO {

	public VcagentreplaceEntity selectbyVcagentreplaceEntity(int id);

	public List<VcagentreplaceEntity> selectAll();

	public void insert (VcagentreplaceEntity vcagentreplace);
	
	public void update (VcagentreplaceEntity vcagentreplace);
	
	public void delete (VcagentreplaceEntity vcagentreplace);
}
