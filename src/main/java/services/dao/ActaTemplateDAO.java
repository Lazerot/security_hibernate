/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.ActatemplateEntity;

/**
 * @author pmartinez
 *
 */
public interface ActaTemplateDAO {
	
public ActatemplateEntity selectbyActaTemplateEntity(int id);
	
	public List<ActatemplateEntity> selectAll();
	
	public void insert (ActatemplateEntity acta);
	
	public void update (ActatemplateEntity acta);
	
	public void delete (ActatemplateEntity acta);

}
