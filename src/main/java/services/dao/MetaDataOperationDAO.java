/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.MetadataoperationEntity;

/**
 * @author pmartinez
 *
 */
public interface MetaDataOperationDAO {
	
	public MetadataoperationEntity selectbyMetadataoperationEntity(int id);

	public List<MetadataoperationEntity> selectAll();

	public void insert (MetadataoperationEntity metadataoperation);
	
	public void update (MetadataoperationEntity metadataoperation);
	
	public void delete (MetadataoperationEntity metadataoperation);

}
