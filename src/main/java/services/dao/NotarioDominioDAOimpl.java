/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.NotariodominioEntity;

/**
 * @author pmartinez
 *
 */
public class NotarioDominioDAOimpl implements NotarioDominioDAO{

	@Override
	public NotariodominioEntity selectbyNotariodominioEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		NotariodominioEntity notarioDominioEntity = (NotariodominioEntity) session.get(NotariodominioEntity.class, id);
		return notarioDominioEntity;
	}

	@Override
	public List<NotariodominioEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<NotariodominioEntity> criteria = builder.createQuery(NotariodominioEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<NotariodominioEntity> root = criteria.from(NotariodominioEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<NotariodominioEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<NotariodominioEntity> notarioDominioEntity = q.getResultList();

		return notarioDominioEntity;
	}

	@Override
	public void insert(NotariodominioEntity notariodominio) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(notariodominio);
		notariodominio.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(NotariodominioEntity notariodominio) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(notariodominio);
		session.getTransaction().commit();
	}

	@Override
	public void delete(NotariodominioEntity notariodominio) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(notariodominio);
		session.getTransaction().commit();
	}

}
