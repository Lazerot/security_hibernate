/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.CustomeroperationEntity;

/**
 * @author pmartinez
 *
 */
public class CustomerOperationDAOimpl implements CustomerOperationDAO {

	@Override
	public CustomeroperationEntity selectbyCustomeroperationEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		CustomeroperationEntity customerOperationEntity = (CustomeroperationEntity) session.get(CustomeroperationEntity.class, id);
		return customerOperationEntity;
	}

	@Override
	public List<CustomeroperationEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<CustomeroperationEntity> criteria = builder.createQuery(CustomeroperationEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<CustomeroperationEntity> root = criteria.from(CustomeroperationEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<CustomeroperationEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<CustomeroperationEntity> customerOperationEntity = q.getResultList();

		return customerOperationEntity;
	}

	@Override
	public void insert(CustomeroperationEntity customeroperation) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(customeroperation);
		customeroperation.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(CustomeroperationEntity customeroperation) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(customeroperation);
		session.getTransaction().commit();
	}

	@Override
	public void delete(CustomeroperationEntity customeroperation) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(customeroperation);
		session.getTransaction().commit();
	}

}
