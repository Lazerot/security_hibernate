/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.VctestconnectionEntity;

/**
 * @author pmartinez
 *
 */
public interface VcTestConnectionDAO {

	public VctestconnectionEntity selectbyVctestconnectionEntity(int id);

	public List<VctestconnectionEntity> selectAll();

	public void insert (VctestconnectionEntity vctestconnection);
	
	public void update (VctestconnectionEntity vctestconnection);
	
	public void delete (VctestconnectionEntity vctestconnection);
}
