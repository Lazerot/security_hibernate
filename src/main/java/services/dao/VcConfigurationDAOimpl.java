/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.VcconfigurationEntity;

/**
 * @author pmartinez
 *
 */
public class VcConfigurationDAOimpl implements VcConfigurationDAO{

	@Override
	public VcconfigurationEntity selectbyVcconfigurationEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		VcconfigurationEntity vcConfigurationEntity = (VcconfigurationEntity) session.get(VcconfigurationEntity.class, id);
		return vcConfigurationEntity;
	}

	@Override
	public List<VcconfigurationEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<VcconfigurationEntity> criteria = builder.createQuery(VcconfigurationEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<VcconfigurationEntity> root = criteria.from(VcconfigurationEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<VcconfigurationEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<VcconfigurationEntity> vcConfigurationEntity = q.getResultList();

		return vcConfigurationEntity;
	}

	@Override
	public void insert(VcconfigurationEntity vcconfiguration) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(vcconfiguration);
		vcconfiguration.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(VcconfigurationEntity vcconfiguration) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(vcconfiguration);
		session.getTransaction().commit();
	}

	@Override
	public void delete(VcconfigurationEntity vcconfiguration) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(vcconfiguration);
		session.getTransaction().commit();
	}

}
