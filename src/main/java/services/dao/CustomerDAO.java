/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.CustomerEntity;

/**
 * @author pmartinez
 *
 */
public interface CustomerDAO {

	public CustomerEntity selectbyCustomerEntity(int id);

	public List<CustomerEntity> selectAll();

	public void insert (CustomerEntity customer);
	
	public void update (CustomerEntity customer);
	
	public void delete (CustomerEntity customer);
	
	public List<?> datosUsuario (CustomerEntity customer);
	
	public List<?> datosUsuarioCriteria (CustomerEntity customer);

}
