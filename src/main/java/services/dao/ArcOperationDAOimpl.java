/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.ArcoperationEntity;

/**
 * @author pmartinez
 *
 */
public class ArcOperationDAOimpl implements ArcOperationDAO{

	@Override
	public ArcoperationEntity selectbyArcoperationEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		ArcoperationEntity arcOperationEntity = (ArcoperationEntity) session.get(ArcoperationEntity.class, id);
		return arcOperationEntity;
		
	}

	@Override
	public List<ArcoperationEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<ArcoperationEntity> criteria = builder.createQuery(ArcoperationEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<ArcoperationEntity> root = criteria.from(ArcoperationEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<ArcoperationEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<ArcoperationEntity> arcOperationEntity = q.getResultList();

		return arcOperationEntity;
	}

	@Override
	public void insert(ArcoperationEntity arcoperation) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(arcoperation);
		arcoperation.setId(id);
		session.getTransaction().commit();		
	}

	@Override
	public void update(ArcoperationEntity arcoperation) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(arcoperation);
		session.getTransaction().commit();
	}

	@Override
	public void delete(ArcoperationEntity arcoperation) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(arcoperation);
		session.getTransaction().commit();
	}

}
