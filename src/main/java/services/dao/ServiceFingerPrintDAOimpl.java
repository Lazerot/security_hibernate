/**
 * 
 */
package services.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.digitalpersona.uareu.Fmd;
import com.galeon.docsmanager.manager.FileManager;
import com.galeon.docsmanager.manager.SignManager;
import com.galeon.docsmanager.utils.TSADataForm;
import com.galeon.docsmanager.utils.TSUConsumer;
import com.galeon.esign.events.EventManager;
import com.galeon.esign.utils.Globals;
import com.galeon.esign.utils.XmlUtils;
import com.galeon.opentok.ArchiveInfo;

import bbdd.tables.FingerPrintEntity;
import bbdd.tables.OperationEntity;
import exceptions.Forbidden_exception;
import exceptions.Internal_server_error_exception;
import services.digitalFinger.DigitalFingerController;
import services.digitalFinger.FingerPrintInfoXml;
import services.digitalFinger.FingerprintInfo;
import utils.Constantes;
import utils.Xml;
import bbdd.dto.FingerPrintDTO;

/**
 * @author pj
 *
 */
@RestController
@RequestMapping("/FingerPrint")

public class ServiceFingerPrintDAOimpl implements FingerPrintDAO{
	
	private static Logger logger = Logger.getLogger(ServiceFingerPrintDAOimpl.class.getName());
	private String loggerHeader = "";
	

	@GetMapping("/fingerPrint/selectbyFingerEntity/{customeruuid}")
	@Override
	//Devuelve el objeto de la bbdd de FingerPrintEntity asociado al customeruuid
	public FingerPrintEntity selectbyFingerEntity(@PathVariable String userUuid) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		FingerPrintEntity fingerEntity = (FingerPrintEntity) session.get(FingerPrintEntity.class, userUuid);
		return fingerEntity;
	}
	
	@Override
	@PostMapping("/enrollment")
//	@PreAuthorize("hasAnyRole('PLUGIN')")
	public void enrollment(@Valid @RequestBody FingerprintInfo fingerPrintInfo) throws Exception
	{
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
//		session.beginTransaction();
		
		loggerHeader = "[Service enrollment()] -> ";		
		logger.info(loggerHeader + "Enrollment identity for userUuid: " + fingerPrintInfo.getUserUuid());
		
		try 
		{
//		Nuevo Objeto de la bbdd para fingerEntity
		FingerPrintEntity fingerPrintEntity = new FingerPrintEntity();
//		Enviamos los datos que son recuperados del objeto fingerPrintInfo
		fingerPrintEntity.setUserUuid(fingerPrintInfo.getUserUuid());
		fingerPrintEntity.setDate(new Date());
		fingerPrintEntity.setFingerPosition
		(DigitalFingerController.getFingerPosition(fingerPrintInfo.getMinutiaeData(), fingerPrintInfo.getUserUuid()));
		
//		1) Recuperar minucias y mandarlas a la bbdd sin firmar
		Fmd minutiaeData = DigitalFingerController.getFmd(fingerPrintInfo.getImageData(), fingerPrintInfo.getUserUuid());
		if (minutiaeData != null) 
		{
			fingerPrintEntity.setMinutiae(minutiaeData.getData());
		}
		
		String fileImageName =  fingerPrintEntity.getUserUuid() + "_" + fingerPrintEntity.getFingerPosition() + "_Image.bin";		
		fingerPrintEntity.setFingerImageFilePath(fileImageName);
		
		//Sacamos el Path
		String path = calculatePath();
		
		//Enviar el path 
		fingerPrintEntity.setFingerImageFilePath(path);
		fingerPrintEntity.setMinutiaeSignedFilePath(path);
		fingerPrintEntity.setFingerImageSignedFilePath(path);
		
		//recuperar los bytes de la imagen (metodo getImageData) => imageRawData
		//Esta imagen esta decodificada de la base 64
		byte[] fileBytes = DigitalFingerController.getImageData(fingerPrintInfo.getImageData(), fingerPrintInfo.getUserUuid());
		
		
		//2) Save en cloud (imagen  de las huellas en bytes) => Sin firmar
		FileManager filemanager = new FileManager();
					filemanager.saveFileNotBase64(fileBytes, path, fileImageName , Constantes.MIME);
					
		//3) Save las minucias FIRMADAS => base 64
		String minutaeFinger64 = DigitalFingerController.getFmdBase64(fingerPrintInfo.getMinutiaeData(), fingerPrintInfo.getUserUuid());

		String fingerprintSignedMinutae = firma(fingerPrintEntity, minutaeFinger64);	
		fileImageName =  fingerPrintEntity.getUserUuid() + "_" + fingerPrintEntity.getFingerPosition() + "_Minutae_Signed.bin";
		filemanager.saveFileNotBase64(fingerprintSignedMinutae.getBytes(), path, fileImageName , Constantes.MIME);

		//4) Save imagen huellas FIRMADAS => base 64		
		String imageFinger64 = DigitalFingerController.getImageDataBase64(fingerPrintInfo.getImageData(), fingerPrintInfo.getUserUuid());
		
		String fingerprintSigned = firma(fingerPrintEntity, imageFinger64);	
		fileImageName =  fingerPrintEntity.getUserUuid() + "_" + fingerPrintEntity.getFingerPosition() + "_Image_Signed.bin";
		filemanager.saveFileNotBase64(fingerprintSigned.getBytes(), path, fileImageName , Constantes.MIME);
	
		}catch(Exception e)
		{
			throw new Internal_server_error_exception ("ERROR al guardar con el usuario => "+ fingerPrintInfo.getUserUuid());
		}
		session.getTransaction().commit();
					
	}

	@PostMapping("/verify")
	@PreAuthorize("hasAnyRole('PLUGIN')")
	@Override
	public Boolean verify(FingerprintInfo fingerPrintInfo, FingerPrintEntity fingerPrint) 
	{
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		loggerHeader = "[Service verify()] -> ";		
		logger.info(loggerHeader + "Verifying identity for userUuid: " + fingerPrintInfo.getUserUuid());
		
		// 1. Get user's fingerprint transmitted
				Fmd minutiaeData = DigitalFingerController.getFmd(fingerPrintInfo.getMinutiaeData(), fingerPrintInfo.getUserUuid());
		
				if (minutiaeData != null) 
				{
				
					int fingerPosition = DigitalFingerController.getFingerPosition(fingerPrintInfo.getMinutiaeData(), fingerPrintInfo.getUserUuid());
					
					// TODO: Change this after connecting to database. minutiaeBytes: minutiae from database
					// 2. Get user's fingerprint from database
				   Fmd fmdDB = DigitalFingerController.getFmdFromBytes(fingerPrint.getMinutiae(), fingerPrint.getUserUuid());
					
					// 3. Compare two fingerprints
					boolean sameFingerPrint = DigitalFingerController.checkPrints(minutiaeData, fmdDB, fingerPrintInfo.getUserUuid());		
				
					if (!sameFingerPrint) 
					{
						return false;
					}
					else 
					{
						return true;
					} 
				}
				return null;
	}
		
	@GetMapping("/fingerPrint/getFingerPrintByUserUuid")
	@Override
	public List<?> getFingerPrintByUserUuid(FingerPrintEntity fingerPrint) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		FingerPrintDTO dto = new FingerPrintDTO();
		
		if(fingerPrint.getUserUuid() !=null && fingerPrint.getFingerPosition() != null) {
		
			String userUuid = fingerPrint.getUserUuid();
			Integer fingerId = fingerPrint.getFingerPosition();
			
			@SuppressWarnings("deprecation")
		List<?> resultWithAliasedBean = session.createSQLQuery
				(" SELECT DISTINCT fp.userUuid, fp.fingerposition, fp.fingerImageFilePath, fp.minutiae, fp.fingerImageSignedFilePath, fp.minutiaeSignedFilePath, fp.active "
				+ "from fingerprint fp "
				+ "INNER JOIN customer c ON c.uuid = fp.userUuid "
				+ "where :userUuid "
				+ "and :fingerid "
				+ "and active = 1 ")
				.setParameter("userUuid", userUuid)
				.setParameter("fingerId", fingerId)
				.setResultTransformer(Transformers.aliasToBean(FingerPrintDTO.class))
				.list();
			
			dto = (FingerPrintDTO) resultWithAliasedBean.get(0);
			
			return resultWithAliasedBean;
		}
		return null;
	}

	@GetMapping("/fingerPrint/selectAll")
	@Override
	public List<FingerPrintEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<FingerPrintEntity> criteria = builder.createQuery(FingerPrintEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<FingerPrintEntity> root = criteria.from(FingerPrintEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<FingerPrintEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<FingerPrintEntity> fingerEntity = q.getResultList();

		return fingerEntity;
	}

	@PostMapping("/fingerPrint/insert")
	@Override
	public void insert(FingerPrintEntity fingerid) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction(); 
		
		String userUuid = (String) session.save(fingerid);
		fingerid.setUserUuid(userUuid);
		session.getTransaction().commit();
	}

	@PostMapping("/fingerPrint/update")
	@Override
	public void update(FingerPrintEntity fingerid) {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(fingerid);
		session.getTransaction().commit();
	}

	@PostMapping("/fingerPrint/delete")
	@Override
	public void delete(FingerPrintEntity fingerid) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		//Poner el active a 0 ya que por defecto está en 1. De esta manera, quedara "eliminado"
		Integer activeBorrado = 0;
		
		int active = (int) session.merge(activeBorrado);
		fingerid.setActive(active);	
		session.getTransaction().commit();
	}

	@Override
	public String firma (FingerPrintEntity fingerPrint, String fingerprintB64) 
	{
		
		//Iniciar Sesssion factory de hibernate
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		//Clase FingerPrintInfoXml
		FingerPrintInfoXml fingerInfo = new FingerPrintInfoXml();
		
		String userUuid = fingerPrint.getUserUuid();
		
		//Query para obtener el domainId asociado al usuario
		@SuppressWarnings("deprecation")
		Integer domainUser = session.createSQLQuery
				(" SELECT  du.domainid, "
				+ "from domainuser du "
				+ "where :userUuid ")
				.setParameter("userUuid", userUuid)
				.getFirstResult();
		
		//Pasar el array de bytes (minucia) y la imagen del dedo dentro del fingerprintEntity
		
		//Rellenar el fingerInfo con los datos del TSADataForm. Pasandole el domainid
		TSADataForm tsaData = TSUConsumer.stamp(domainUser);
		
		if (tsaData != null) {
			fingerInfo.setTsaGentime(tsaData.getGentime());
			fingerInfo.setTsaSignerdata(tsaData.getSignerdata());
			fingerInfo.setTsaSerialnumber(tsaData.getSerialnumber());
			fingerInfo.setTsaPolicy(tsaData.getPolicy());
			fingerInfo.setTsaB64data(tsaData.getB64data());
		}
		
		fingerInfo.setBase64Fingerprint(fingerprintB64);
		
		String xmlEv = Xml.jaxbArchiveInfoToXML(fingerInfo);
		
		SignManager sig = new SignManager(domainUser, "",
				Globals.certificadoPem, Globals.clavePrivadaPem,
				Globals.certificadoSubCaPem);

		String signed = sig.doXadesSign(xmlEv);

		return signed;
	}
	
	public static  String calculatePath(){

    	Calendar cal = Calendar.getInstance();
    	int year = cal.get(Calendar.YEAR);
    	int month = cal.get(Calendar.MONTH ) +1;
    	int day = cal.get(Calendar.DAY_OF_MONTH);
    	String path = "";
    	path = year + "/" + month + "/" + day +"/";

    	return path;
    }
	
}
