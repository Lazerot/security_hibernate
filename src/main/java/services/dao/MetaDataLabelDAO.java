/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.MetadatalabelEntity;

/**
 * @author pmartinez
 *
 */
public interface MetaDataLabelDAO {

	public MetadatalabelEntity selectbyMetadatalabelEntity(int id);

	public List<MetadatalabelEntity> selectAll();

	public void insert (MetadatalabelEntity metadatalabel);
	
	public void update (MetadatalabelEntity metadatalabel);
	
	public void delete (MetadatalabelEntity metadatalabel);

}
