package services.dao;

import java.util.List;

import bbdd.tables.DomainEntity;

/**
 * @author pmartinez
 *
 */
public interface DomainDAO {

	public DomainEntity selectbyDomainEntity(int id);

	public List<DomainEntity> selectAll();

	public void insert (DomainEntity domain);
	
	public void update (DomainEntity domain);
	
	public void delete (DomainEntity domain);

}
