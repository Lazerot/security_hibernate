/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.NotarioEntity;

/**
 * @author pmartinez
 *
 */
public interface NotarioDAO {

	public NotarioEntity selectbyNotarioEntity(int id);

	public List<NotarioEntity> selectAll();

	public void insert (NotarioEntity notario);
	
	public void update (NotarioEntity notario);
	
	public void delete (NotarioEntity notario);
}
