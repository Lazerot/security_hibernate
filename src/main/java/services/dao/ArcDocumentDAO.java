/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.ArcdocumentEntity;

/**
 * @author pmartinez
 *
 */
public interface ArcDocumentDAO {
	
	public ArcdocumentEntity selectbyArcdocumentEntity(int id);

	public List<ArcdocumentEntity> selectAll();

	public void insert (ArcdocumentEntity arcdocument);
	
	public void update (ArcdocumentEntity arcdocument);
	
	public void delete (ArcdocumentEntity arcdocument);

}
