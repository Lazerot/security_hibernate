/**
 * 
 */
package services.dao;

import java.util.List;
import java.util.Set;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.SetJoin;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.transform.ResultTransformer;
import org.hibernate.transform.Transformers;

import bbdd.tables.CustomerEntity;
import bbdd.tables.CustomerEntity_;
import bbdd.tables.CustomeroperationEntity;
import bbdd.tables.CustomeroperationEntity_;
import bbdd.tables.OperationEntity;
import bbdd.dto.CustomerAndOperationDTO;

/**
 * @author pmartinez
 *
 */
public class CustomerDAOimpl implements CustomerDAO{

	@Override
	public CustomerEntity selectbyCustomerEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		CustomerEntity customerEntity = session.get(CustomerEntity.class, id);
		return customerEntity;
	}

	@Override
	public List<CustomerEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<CustomerEntity> criteria = builder.createQuery(CustomerEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<CustomerEntity> root = criteria.from(CustomerEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<CustomerEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<CustomerEntity> customerEntity = q.getResultList();

		return customerEntity;
		
	}

	@Override
	public void insert(CustomerEntity customer) {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(customer);
		customer.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(CustomerEntity customer) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(customer);
		session.getTransaction().commit();
	}

	@Override
	public void delete(CustomerEntity customer) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(customer);
		session.getTransaction().commit();
	}

	@Override
	public List<?> datosUsuario(CustomerEntity customer) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();

		CustomerAndOperationDTO dto = new CustomerAndOperationDTO();

		if(customer.getNif() != null) 
		{

			String nif = customer.getNif();
			
			@SuppressWarnings("deprecation")
			List<?> resultWithAliasedBean = session.createSQLQuery
					(" SELECT DISTINCT c.id as id_c, op.id as id_op, c.name, c.lastname, c.email, c.cellphone, c.nif, op.extra, cop.operationid, op.creationdate "
					+ "from customeroperation cop "
					+ "INNER JOIN customer c ON c.id = cop.customerid "
					+ "INNER JOIN operation op ON "
					+ " op.id = cop.operationid "
					+ " WHERE c.nif = '" + nif + "' ")
					.setResultTransformer(Transformers.aliasToBean(CustomerAndOperationDTO.class))
					.list();

			 dto = (CustomerAndOperationDTO) resultWithAliasedBean.get(0);
			
			 return  resultWithAliasedBean;

		}
		
		else if(customer.getEmail() != null) 
		{
			
			String email = customer.getEmail();
			
			@SuppressWarnings("deprecation")
			List<?> resultWithAliasedBean = session.createSQLQuery
					(" SELECT DISTINCT c.id as id_c, op.id as id_op, c.name, c.lastname, c.email, c.cellphone, c.nif, op.extra, cop.operationid, op.creationdate "
					+ "from customeroperation cop "
					+ "INNER JOIN customer c ON c.id = cop.customerid "
					+ "INNER JOIN operation op ON "
					+ " op.id = cop.operationid "
					+ " WHERE c.email =  '" + email + "' ")
					.setResultTransformer(Transformers.aliasToBean(CustomerAndOperationDTO.class))
					.list();

			 dto = (CustomerAndOperationDTO) resultWithAliasedBean.get(0);
			
			 return  resultWithAliasedBean;
		}
		
		else if(customer.getCellphone() != null) 
		{
			
			String cellphone = customer.getCellphone();
			
			@SuppressWarnings("deprecation")
			List<?> resultWithAliasedBean = session.createSQLQuery
					(" SELECT DISTINCT c.id as id_c, op.id as id_op, c.name, c.lastname, c.email, c.cellphone, c.nif, op.extra, cop.operationid, op.creationdate "
					+ "from customeroperation cop "
					+ "INNER JOIN customer c ON c.id = cop.customerid "
					+ "INNER JOIN operation op ON "
					+ " op.id = cop.operationid "
					+ " WHERE c.cellphone =  '" + cellphone + "' ")
					.setResultTransformer(Transformers.aliasToBean(CustomerAndOperationDTO.class))
					.list();

			 dto = (CustomerAndOperationDTO) resultWithAliasedBean.get(0);
			
			 return  resultWithAliasedBean;
		}
		return null;
		
	}

	@Override
	public List<?> datosUsuarioCriteria(CustomerEntity customer) {
		
		//Metodo que no se utiliza al final
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		String cellphone = customer.getCellphone();
		
		//Crear a builder
		CriteriaBuilder builder = session.getCriteriaBuilder();
		
		//Creamos el criteria (Operation)
	    CriteriaQuery<OperationEntity> OperationCriteria = builder.createQuery(OperationEntity.class);
	    //Creamos el criteria (Comsumer)
	    CriteriaQuery<CustomerEntity> CustomerCritreria = builder.createQuery(CustomerEntity.class);
	    //Creamos el criteria (CustomerOperation=
	    CriteriaQuery<CustomeroperationEntity> customerOperationCriteria = builder.createQuery(CustomeroperationEntity.class);

	   //Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
//	    Root<OperationEntity> OperationTable = OperationCriteria.from(OperationEntity.class);
	    
	    //Root from Customer
	    Root<CustomerEntity> CustomerTableRoot = CustomerCritreria.from(CustomerEntity.class);
	    
	    //Root CustomerOperation
//	    Root<CustomeroperationEntity> CustomerOperation = customerOperationCriteria.from(CustomeroperationEntity.class);
	    
	    //Using the Joins
	    
//	    Join<CustomerEntity, CustomeroperationEntity> customerId = CustomerTableRoot.join(CustomerEntity_.id).join(CustomeroperationEntity_.customerid);
  		
	    //Join with operationid to operationEntity to customerOperationEntity
//  	    Join<OperationEntity, CustomeroperationEntity> copJoin = OperationTable.join("operationid");
//  	   
//  	    //Join with customerid to customerEntity to customerOperationEntity
//  	    Join<CustomeroperationEntity, CustomerEntity> cusJoin = copJoin.join("customerid");
    
  	    
	    /// !!!!!! WHERE BUENO !!!!!!
	    CustomerCritreria.select(CustomerTableRoot).where(builder.equal(CustomerTableRoot.get(CustomerEntity_.cellphone), cellphone));
	   
		return null;

	}
	
}
