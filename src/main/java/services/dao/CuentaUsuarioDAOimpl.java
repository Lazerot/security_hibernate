/**
 * 
 */
package services.dao;

import java.util.List;


import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;


import bbdd.tables.CuentausuarioEntity;

/**
 * @author pmartinez
 *
 */
public class CuentaUsuarioDAOimpl implements CuentaUsuarioDAO {

	@Override
	public CuentausuarioEntity selectbyCuentausuarioEntity(int idusuario) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
	    Session session = factory.getCurrentSession();
	    
	    CuentausuarioEntity cuentaUsuario = (CuentausuarioEntity) session.get(CuentausuarioEntity.class, idusuario);

		return cuentaUsuario;
	}

	@Override
	public List<CuentausuarioEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
	    Session session = factory.getCurrentSession();
	    session.beginTransaction();

	    // Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();

	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<CuentausuarioEntity> criteria = builder.createQuery(CuentausuarioEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<CuentausuarioEntity> root = criteria.from(CuentausuarioEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<CuentausuarioEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<CuentausuarioEntity> cuentaUsuario = q.getResultList();
	   

	    return cuentaUsuario;
	}

	@Override
	public void insert(CuentausuarioEntity cuentausuario) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
	    Session session = factory.getCurrentSession();
	    session.beginTransaction();
	    
	    int idusuario = (int) session.save(cuentausuario);
		cuentausuario.setIdusuario(idusuario);
		session.getTransaction().commit();
		
	}

	@Override
	public void update(CuentausuarioEntity cuentausuario) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
	    Session session = factory.getCurrentSession();
	    session.beginTransaction();
	    session.merge(cuentausuario);
	    session.getTransaction().commit();
		
	}

	@Override
	public void delete(CuentausuarioEntity cuentausuario) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		session.delete(cuentausuario);
		session.getTransaction().commit();
		
		
	}

}
