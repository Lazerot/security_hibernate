/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.NotariodominioEntity;

/**
 * @author pmartinez
 *
 */
public interface NotarioDominioDAO {

	public NotariodominioEntity selectbyNotariodominioEntity(int id);

	public List<NotariodominioEntity> selectAll();

	public void insert (NotariodominioEntity notariodominio);
	
	public void update (NotariodominioEntity notariodominio);
	
	public void delete (NotariodominioEntity notariodominio);
}
