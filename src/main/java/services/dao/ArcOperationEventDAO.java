/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.ArcoperationeventEntity;

/**
 * @author pmartinez
 *
 */
public interface ArcOperationEventDAO {
	
	public ArcoperationeventEntity selectbyArcoperationeventEntity(int id);

	public List<ArcoperationeventEntity> selectAll();

	public void insert (ArcoperationeventEntity arcoperationevent);
	
	public void update (ArcoperationeventEntity arcoperationevent);
	
	public void delete (ArcoperationeventEntity arcoperationevent);


}
