/**
 * 
 */
package services.dao;

import java.util.List;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;

import bbdd.tables.CustomerEntity;
import bbdd.tables.CustomeroperationEntity;
import bbdd.tables.OperationEntity;
import bbdd.dto.OperationDTO;

/**
 * @author pmartinez
 *
 */
public class OperationDAOimpl implements OperationDAO{

	@Override
	public OperationEntity selectbyOperationEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		OperationEntity operationEntity = (OperationEntity) session.get(OperationEntity.class, id);
		return operationEntity;
	}

	@Override
	public List<OperationEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<OperationEntity> criteria = builder.createQuery(OperationEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<OperationEntity> root = criteria.from(OperationEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<OperationEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<OperationEntity> operationEntity = q.getResultList();

		return operationEntity;
	}
	
	@Override
	public List<?> selectRestrictionsCriteria(int domainid, Date from, Date to, String state, String nif, String domainusersIN) {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		//Crear a builder
		CriteriaBuilder builder = session.getCriteriaBuilder();
		
		//Creamos el criteria (Operation)
	    CriteriaQuery<OperationEntity> OperationCriteria = builder.createQuery(OperationEntity.class);
	    //Creamos el criteria (Comsumer)
	    CriteriaQuery<CustomerEntity> CustomerCritreria = builder.createQuery(CustomerEntity.class);

	   //Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<OperationEntity> OperationTable = OperationCriteria.from(OperationEntity.class);
	    
	    //Root from Customer
	    Root<CustomerEntity> CustomerTable = CustomerCritreria.from(CustomerEntity.class);
	    
	  //Clausules where
	    
	    //Where domainid from operation Table
	    OperationCriteria.select(OperationTable).where(builder.equal(OperationTable.get("domainid"), domainid));
	    
	    //Where nif from Customer Table
		CustomerCritreria.select(CustomerTable).where(builder.equal(CustomerTable.get("nif"), nif));
		
		//Where state from operation table
		OperationCriteria.select(OperationTable).where(builder.equal(OperationTable.get("state"), state));
		
		//Date from + to => Between
	   
	  //Using the Joins
	    
		//Join with operationid to operationEntity to customerOperationEntity
	    Join<OperationEntity, CustomeroperationEntity> copJoin = OperationTable.join("operationid");
	   
	    //Join with customerid to customerEntity to customerOperationEntity
	    Join<CustomeroperationEntity, CustomerEntity> cusJoin = copJoin.join("customerid");
	    
	    //q1.select(postRoot).where(cb.equal(postRoot.get("userName"), name));
	    //OperationTable.on(OperationCriteria.equals(OperationTable.get("domainid")));
	    
		return null;
	}

	@Override
	public void insert(OperationEntity operation) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(operation);
		operation.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(OperationEntity operation) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(operation);
		session.getTransaction().commit();
	}

	@Override
	public void delete(OperationEntity operation) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(operation);
		session.getTransaction().commit();
	}

	@Override
	public List<?> fechasDatosOperacion(OperationEntity operation, OperationDTO operationDto) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		OperationDTO dto = new OperationDTO();
		
		if(operation.getCreationdate()  != null ) 
		{
		
			Date fechaCreacion = operation.getCreationdate();
			Date fechaHasta = operationDto.getCreationdatehasta();
				
		@SuppressWarnings("deprecation")
		List<?> resultWithAliasedBean = session.createSQLQuery
				(" SELECT  op.id, op.operationid, op.extra, op.state, op.creationdate, op.signaturedate "
				+ "FROM operation op " 
				+ "where op.creationdate between :fechaCreacion "
				+ "and :fechaHasta ")
				.setParameter("fechaCreacion", fechaCreacion)
				.setParameter("fechaHasta", fechaHasta)
				.setResultTransformer(Transformers.aliasToBean(OperationDTO.class))
				.list();

		 dto = (OperationDTO) resultWithAliasedBean.get(0);
		
		 return  resultWithAliasedBean;
		}
		
		return null;
	}

	}

