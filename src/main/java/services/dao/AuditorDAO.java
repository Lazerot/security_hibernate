/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.AuditorEntity;

/**
 * @author pmartinez
 *
 */
public interface AuditorDAO {
	
	public AuditorEntity selectbyAuditorEntity(int id);

	public List<AuditorEntity> selectAll();

	public void insert (AuditorEntity auditor);
	
	public void update (AuditorEntity auditor);
	
	public void delete (AuditorEntity auditor);


}
