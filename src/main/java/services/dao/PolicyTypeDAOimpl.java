/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.PolicytypeEntity;

/**
 * @author pmartinez
 *
 */
public class PolicyTypeDAOimpl implements PolicyTypeDAO{

	@Override
	public PolicytypeEntity selectbyPolicytypeEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		PolicytypeEntity policyType = (PolicytypeEntity) session.get(PolicytypeEntity.class, id);
		return policyType;
	}

	@Override
	public List<PolicytypeEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<PolicytypeEntity> criteria = builder.createQuery(PolicytypeEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<PolicytypeEntity> root = criteria.from(PolicytypeEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<PolicytypeEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<PolicytypeEntity> policyType = q.getResultList();

		return policyType;
	}

	@Override
	public void insert(PolicytypeEntity policytype) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(policytype);
		policytype.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(PolicytypeEntity policytype) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(policytype);
		session.getTransaction().commit();
	}

	@Override
	public void delete(PolicytypeEntity policytype) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(policytype);
		session.getTransaction().commit();
	}

}
