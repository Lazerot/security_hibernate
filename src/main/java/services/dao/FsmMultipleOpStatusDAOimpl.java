/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.FsmmultipleopstatusEntity;

/**
 * @author pmartinez
 *
 */
public class FsmMultipleOpStatusDAOimpl implements FsmMultipleOpStatusDAO{

	@Override
	public FsmmultipleopstatusEntity selectbyFsmmultipleopstatusEntity(int id) {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		FsmmultipleopstatusEntity fsmMultipleOpStatusEntity = (FsmmultipleopstatusEntity) session.get(FsmmultipleopstatusEntity.class, id);
		return fsmMultipleOpStatusEntity;
	}

	@Override
	public List<FsmmultipleopstatusEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<FsmmultipleopstatusEntity> criteria = builder.createQuery(FsmmultipleopstatusEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<FsmmultipleopstatusEntity> root = criteria.from(FsmmultipleopstatusEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<FsmmultipleopstatusEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<FsmmultipleopstatusEntity> fsmMultipleOpStatusEntity = q.getResultList();

		return fsmMultipleOpStatusEntity;
	}

	@Override
	public void insert(FsmmultipleopstatusEntity fsmmultipleopstatus) {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(fsmmultipleopstatus);
		fsmmultipleopstatus.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(FsmmultipleopstatusEntity fsmmultipleopstatus) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(fsmmultipleopstatus);
		session.getTransaction().commit();
	}

	@Override
	public void delete(FsmmultipleopstatusEntity fsmmultipleopstatus) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(fsmmultipleopstatus);
		session.getTransaction().commit();
	}

}
