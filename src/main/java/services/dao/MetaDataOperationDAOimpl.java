/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.MetadataoperationEntity;

/**
 * @author pmartinez
 *
 */
public class MetaDataOperationDAOimpl implements MetaDataOperationDAO{

	@Override
	public MetadataoperationEntity selectbyMetadataoperationEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		MetadataoperationEntity metaDataOperationEntity = (MetadataoperationEntity) session.get(MetadataoperationEntity.class, id);
		return metaDataOperationEntity;
	}

	@Override
	public List<MetadataoperationEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<MetadataoperationEntity> criteria = builder.createQuery(MetadataoperationEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<MetadataoperationEntity> root = criteria.from(MetadataoperationEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<MetadataoperationEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<MetadataoperationEntity> metaDataOperationEntity = q.getResultList();

		return metaDataOperationEntity;
	}

	@Override
	public void insert(MetadataoperationEntity metadataoperation) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(metadataoperation);
		metadataoperation.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(MetadataoperationEntity metadataoperation) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(metadataoperation);
		session.getTransaction().commit();
	}

	@Override
	public void delete(MetadataoperationEntity metadataoperation) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(metadataoperation);
		session.getTransaction().commit();
	}

}
