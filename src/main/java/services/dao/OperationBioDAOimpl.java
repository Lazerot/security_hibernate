/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.OperationbioEntity;

/**
 * @author pmartinez
 *
 */
public class OperationBioDAOimpl implements OperationBioDAO{

	@Override
	public OperationbioEntity selectbyOperationbioEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		OperationbioEntity operationbioEntity = (OperationbioEntity) session.get(OperationbioEntity.class, id);
		return operationbioEntity;
	}

	@Override
	public List<OperationbioEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<OperationbioEntity> criteria = builder.createQuery(OperationbioEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<OperationbioEntity> root = criteria.from(OperationbioEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<OperationbioEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<OperationbioEntity> operationbioEntity = q.getResultList();

		return operationbioEntity;
	}

	@Override
	public void insert(OperationbioEntity operationbio) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(operationbio);
		operationbio.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(OperationbioEntity operationbio) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(operationbio);
		session.getTransaction().commit();
	}

	@Override
	public void delete(OperationbioEntity operationbio) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(operationbio);
		session.getTransaction().commit();
	}

}
