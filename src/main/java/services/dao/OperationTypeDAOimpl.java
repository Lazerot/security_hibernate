/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.OperationtypeEntity;

/**
 * @author pmartinez
 *
 */
public class OperationTypeDAOimpl implements OperationTypeDAO{

	@Override
	public OperationtypeEntity selectbyOperationtypeEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		OperationtypeEntity operationTypeEntity = (OperationtypeEntity) session.get(OperationtypeEntity.class, id);
		return operationTypeEntity;
	}

	@Override
	public List<OperationtypeEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<OperationtypeEntity> criteria = builder.createQuery(OperationtypeEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<OperationtypeEntity> root = criteria.from(OperationtypeEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<OperationtypeEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<OperationtypeEntity> operationTypeEntity = q.getResultList();

		return operationTypeEntity;
	}

	@Override
	public void insert(OperationtypeEntity operationtype) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(operationtype);
		operationtype.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(OperationtypeEntity operationtype) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(operationtype);
		session.getTransaction().commit();
	}

	@Override
	public void delete(OperationtypeEntity operationtype) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(operationtype);
		session.getTransaction().commit();
	}

}
