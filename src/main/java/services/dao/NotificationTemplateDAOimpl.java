/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.NotificationtemplateEntity;

/**
 * @author pmartinez
 *
 */
public class NotificationTemplateDAOimpl implements NotificationTemplateDAO{

	@Override
	public NotificationtemplateEntity selectbyNotificationtemplateEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		NotificationtemplateEntity notificationTemplateEntity = (NotificationtemplateEntity) session.get(NotificationtemplateEntity.class, id);
		return notificationTemplateEntity;
	}

	@Override
	public List<NotificationtemplateEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<NotificationtemplateEntity> criteria = builder.createQuery(NotificationtemplateEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<NotificationtemplateEntity> root = criteria.from(NotificationtemplateEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<NotificationtemplateEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<NotificationtemplateEntity> notificationTemplateEntity = q.getResultList();

		return notificationTemplateEntity;
	}

	@Override
	public void insert(NotificationtemplateEntity notificationtemplate) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session  = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(notificationtemplate);
		notificationtemplate.setId(id);
		session.beginTransaction().commit();
	}

	@Override
	public void update(NotificationtemplateEntity notificationtemplate) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(notificationtemplate);
		session.getTransaction().commit();
	}

	@Override
	public void delete(NotificationtemplateEntity notificationtemplate) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(notificationtemplate);
		session.getTransaction().commit();
	}

}
