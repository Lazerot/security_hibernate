/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.AdditionaldomainsEntity;

/**
 * @author pmartinez
 *
 */
public interface AdditionalDomainsDAO {

	
public AdditionaldomainsEntity selectbyAdditionaldomainsEntity(int id);
	
	public List<AdditionaldomainsEntity> selectAll();
	
	public void insert (AdditionaldomainsEntity addiDomain);
	
	public void update (AdditionaldomainsEntity addiDomain);
	
	public void delete (AdditionaldomainsEntity addiDomain);
}
