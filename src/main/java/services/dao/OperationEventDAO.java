/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.OperationeventEntity;

/**
 * @author pmartinez
 *
 */
public interface OperationEventDAO {
	
	public OperationeventEntity selectbyOperationeventEntity(int id);

	public List<OperationeventEntity> selectAll();

	public void insert (OperationeventEntity operationevent);
	
	public void update (OperationeventEntity operationevent);
	
	public void delete (OperationeventEntity operationevent);

}
