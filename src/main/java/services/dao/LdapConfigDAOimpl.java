/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.LdapconfigEntity;

/**
 * @author pmartinez
 *
 */
public class LdapConfigDAOimpl implements LdApConfigDAO{

	@Override
	public LdapconfigEntity selectbyArcdocumentEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		LdapconfigEntity ldapconfigEntity = (LdapconfigEntity) session.get(LdapconfigEntity.class, id);
		return ldapconfigEntity;
	}

	@Override
	public List<LdapconfigEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<LdapconfigEntity> criteria = builder.createQuery(LdapconfigEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<LdapconfigEntity> root = criteria.from(LdapconfigEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<LdapconfigEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<LdapconfigEntity> ldapconfigEntity = q.getResultList();

		return ldapconfigEntity;
	}

	@Override
	public void insert(LdapconfigEntity ldapconfig) {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(ldapconfig);
		ldapconfig.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(LdapconfigEntity ldapconfig) {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(ldapconfig);
		session.getTransaction().commit();
	}

	@Override
	public void delete(LdapconfigEntity ldapconfig) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(ldapconfig);
		session.getTransaction().commit();
	}

}
