/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.DocumentEntity;

/**
 * @author pmartinez
 *
 */
public interface DocumentDAO {
	
	public DocumentEntity selectbyDocumentEntity(int id);

	public List<DocumentEntity> selectAll();

	public void insert (DocumentEntity document);
	
	public void update (DocumentEntity document);
	
	public void delete (DocumentEntity document);


}
