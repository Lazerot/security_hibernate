/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.AllowedcasEntity;

/**
 * @author pmartinez
 *
 */
public class AllowedcasDAOimpl implements AllowedcasDAO {
	
	@Override
	public AllowedcasEntity selectbyAllowedcasEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		AllowedcasEntity allowedCasEntity = (AllowedcasEntity) session.get(AllowedcasEntity.class, id);
		
		return allowedCasEntity;
	}


	@Override
	public List<AllowedcasEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<AllowedcasEntity> criteria = builder.createQuery(AllowedcasEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<AllowedcasEntity> root = criteria.from(AllowedcasEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<AllowedcasEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<AllowedcasEntity> allowedCasEntity = q.getResultList();

		return allowedCasEntity;
	}

	@Override
	public void insert(AllowedcasEntity allowedcas) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(allowedcas);
		allowedcas.setId(id);
		session.getTransaction().commit();
		
	}

	@Override
	public void update(AllowedcasEntity allowedcas) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(allowedcas);
		session.getTransaction().commit();
		
	}

	@Override
	public void delete(AllowedcasEntity allowedcas) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(allowedcas);
		session.getTransaction().commit();
		
	}

}
