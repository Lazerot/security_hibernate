/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.GestordocsEntity;

/**
 * @author pmartinez
 *
 */
public interface GestorDocsDAO {

	public GestordocsEntity selectbyGestordocsEntity(int uuid);

	public List<GestordocsEntity> selectAll();

	public void insert (GestordocsEntity gestordocs);
	
	public void update (GestordocsEntity gestordocs);
	
	public void delete (GestordocsEntity gestordocs);

}
