/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.VcvideoconferenceEntity;

/**
 * @author pmartinez
 *
 */
public interface VcVideoConferenceDAO {

	public VcvideoconferenceEntity selectbyVcvideoconferenceEntity(int id);

	public List<VcvideoconferenceEntity> selectAll();

	public void insert (VcvideoconferenceEntity vcvideoconference);
	
	public void update (VcvideoconferenceEntity vcvideoconference);
	
	public void delete (VcvideoconferenceEntity vcvideoconference);
}
