/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.ArcconfigurationEntity;

/**
 * @author pmartinez
 *
 */
public class ArcconfigurationDAOimpl implements ArcconfigurationDAO{

	@Override
	public ArcconfigurationEntity selectbyAllowedcasEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		ArcconfigurationEntity arcConfigurationEntity = (ArcconfigurationEntity) session.get(ArcconfigurationEntity.class, id);
		
		return arcConfigurationEntity;
	}

	@Override
	public List<ArcconfigurationEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<ArcconfigurationEntity> criteria = builder.createQuery(ArcconfigurationEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<ArcconfigurationEntity> root = criteria.from(ArcconfigurationEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<ArcconfigurationEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<ArcconfigurationEntity> arcConfigurationEntity = q.getResultList();

		return arcConfigurationEntity;
	}

	@Override
	public void insert(ArcconfigurationEntity arcconfiguration) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(arcconfiguration);
		arcconfiguration.setId(id);
		session.getTransaction().commit();
		
	}

	@Override
	public void update(ArcconfigurationEntity arcconfiguration) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(arcconfiguration);
		session.getTransaction().commit();
		
	}

	@Override
	public void delete(ArcconfigurationEntity arcconfiguration) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(arcconfiguration);
		session.getTransaction().commit();
	
	}

}
