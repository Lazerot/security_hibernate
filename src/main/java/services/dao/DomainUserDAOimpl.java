/**
 * 
 */
package services.dao;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import bbdd.tables.DomainuserEntity;
import exceptions.Forbidden_exception;
import bbdd.dto.DomainUserDTO;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.TextCodec;


/**
 * @author pmartinez
 *
 */
@RestController
@RequestMapping("/domainUser")
public class DomainUserDAOimpl implements DomainUserDAO{

	private AuthenticationManager authenticationManager;


	@Override
	public DomainuserEntity selectbyDomainuserEntity(int id) 
	{
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		DomainuserEntity domainUserEntity = (DomainuserEntity) session.get(DomainuserEntity.class, id);
		return domainUserEntity;
	}

	@Override
	public List<DomainuserEntity> selectAll() 
	{

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<DomainuserEntity> criteria = builder.createQuery(DomainuserEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<DomainuserEntity> root = criteria.from(DomainuserEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<DomainuserEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<DomainuserEntity> domainUserEntity = q.getResultList();

		return domainUserEntity;
	}

	@Override
	public void insert(DomainuserEntity domainuser) 
	{
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(domainuser);
		domainuser.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(DomainuserEntity domainuser) 
	{

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(domainuser);
		session.getTransaction().commit();
	}

	@Override
	public void delete(DomainuserEntity domainuser) 
	{
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(domainuser);
		session.getTransaction().commit();
	}

	@PostMapping("/login")
	@ResponseBody
	public DomainUserDTO login(@Valid @RequestBody DomainuserEntity domainuser, HttpServletResponse response) throws Exception
	{
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		DomainUserDTO dto = new DomainUserDTO();
		
		if(domainuser.getUseremail() != null) 
		{
//			Creamos la variable String user y le pasamos el userEmail
			String user = domainuser.getUseremail();
			String password = domainuser.getUserpassword();
			
			try {
			//Creamos la query y añadimos el Token generado anteriormente, que ira al dto
			List<?> resultWithAliasedBean = session.createSQLQuery
					("SELECT du.id, du.username, du.useremail, du.userpassword, du.useralias, du.role FROM domainuser du "
					+ "WHERE du.useremail = :email "
					+ "AND du.userpassword = :password "
					+ "AND du.state = 1 ")
					.setParameter("email", user)
					.setParameter("password", password)
					.setResultTransformer(Transformers.aliasToBean(DomainUserDTO.class))
					.list();
					
			 dto = (DomainUserDTO) resultWithAliasedBean.get(0);
			 
			 user = dto.getUseremail();
			 password = dto.getUserpassword();
			
			//Metodo que crea el token
//			String token = jwtTokenProvider.createToken(user,
//					this.users.findByUseremail(user).get().getRoles());
						
			final Instant now = Instant.now();
			
			final String jwt = Jwts.builder()
			        .setSubject(user)
			        .setIssuedAt(Date.from(now))
			        .setExpiration(Date.from(now.plus(1, ChronoUnit.DAYS)))
			        .signWith(SignatureAlgorithm.HS256, TextCodec.BASE64.encode(password))
			        .compact();
			    			
			 dto.setToken(jwt);

			}catch (Exception e) {
				throw new Forbidden_exception("No coincide la autorizacion ");
			}
			 return  dto;	   
				
		}
		
		else if (domainuser.getUsername() != null) 
		{
			
//			Creamos la variable String user y le pasamos el userEmail
			String user = domainuser.getUsername();			
			String password = domainuser.getUserpassword();
			
			//Creamos la query y añadimos el Token generado anteriormente, que ira al dto
			List<?> resultWithAliasedBean = session.createSQLQuery
					("SELECT du.id, du.username, du.useremail, du.userpassword, du.useralias, du.role FROM domainuser du "
					+ "WHERE du.username = :username "
					+ "AND du.userpassword = :password "
					+ "AND du.state = 1 ")
					.setParameter("username", user)
					.setParameter("password", password)
					.setResultTransformer(Transformers.aliasToBean(DomainUserDTO.class))
					.list();
					
			 dto = (DomainUserDTO) resultWithAliasedBean.get(0);
			 
			 user = dto.getUsername();
			 password = dto.getUserpassword();
			
			//Metodo que crea el token
//			String token = jwtTokenProvider.createToken(user,
//					this.users.findByUseremail(user).get().getRoles());
						
			final Instant now = Instant.now();
			
			final String jwt = Jwts.builder()
			        .setSubject(user)
			        .setIssuedAt(Date.from(now))
			        .setExpiration(Date.from(now.plus(1, ChronoUnit.DAYS)))
			        .signWith(SignatureAlgorithm.HS256, TextCodec.BASE64.encode(password))
			        .compact();
			    			
			 dto.setToken(jwt);

			 return  dto;	   
		}
		
		return null;
	}
	
}
