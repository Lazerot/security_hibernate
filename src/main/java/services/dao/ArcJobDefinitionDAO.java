/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.ArcjobdefinitionEntity;

/**
 * @author pmartinez
 *
 */
public interface  ArcJobDefinitionDAO {
	
	public ArcjobdefinitionEntity selectbyArcjobdefinitionEntity(int id);

	public List<ArcjobdefinitionEntity> selectAll();

	public void insert (ArcjobdefinitionEntity arcjobdefinition);
	
	public void update (ArcjobdefinitionEntity arcjobdefinition);
	
	public void delete (ArcjobdefinitionEntity arcjobdefinition);


}
