/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.DocumentcertifiedEntity;

/**
 * @author pmartinez
 *
 */
public interface DocumentCertifiedDAO {

	public DocumentcertifiedEntity selectbyDocumentcertifiedEntity(int id);

	public List<DocumentcertifiedEntity> selectAll();

	public void insert (DocumentcertifiedEntity documentcertified);
	
	public void update (DocumentcertifiedEntity documentcertified);
	
	public void delete (DocumentcertifiedEntity documentcertified);

}
