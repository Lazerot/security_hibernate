/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.ReviewerEntity;

/**
 * @author pmartinez
 *
 */
public interface ReviewerDAO {

	public ReviewerEntity selectbyReviewerEntity(int id);

	public List<ReviewerEntity> selectAll();

	public void insert (ReviewerEntity reviewer);
	
	public void update (ReviewerEntity reviewer);
	
	public void delete (ReviewerEntity reviewer);
}
