/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.RespuestasencuestaEntity;

/**
 * @author pmartinez
 *
 */
public class RespuestasEncuestaDAOimpl implements RespuestasEncuestaDAO{

	@Override
	public RespuestasencuestaEntity selectbyRespuestasencuestaEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		RespuestasencuestaEntity respuestasEncuestaEntity = (RespuestasencuestaEntity) session.get(RespuestasencuestaEntity.class, id);
		return respuestasEncuestaEntity;
	}

	@Override
	public List<RespuestasencuestaEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<RespuestasencuestaEntity> criteria = builder.createQuery(RespuestasencuestaEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<RespuestasencuestaEntity> root = criteria.from(RespuestasencuestaEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<RespuestasencuestaEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<RespuestasencuestaEntity> respuestasEncuestaEntity = q.getResultList();

		return respuestasEncuestaEntity;
	}

	@Override
	public void insert(RespuestasencuestaEntity respuestasencuesta) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(respuestasencuesta);
		respuestasencuesta.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(RespuestasencuestaEntity respuestasencuesta) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(respuestasencuesta);
		session.getTransaction().commit();
	}

	@Override
	public void delete(RespuestasencuestaEntity respuestasencuesta) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(respuestasencuesta);
		session.getTransaction().commit();
	}

}
