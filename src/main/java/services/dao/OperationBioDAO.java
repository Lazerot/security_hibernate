/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.OperationbioEntity;

/**
 * @author pmartinez
 *
 */
public interface OperationBioDAO {

	public OperationbioEntity selectbyOperationbioEntity(int id);

	public List<OperationbioEntity> selectAll();

	public void insert (OperationbioEntity operationbio);
	
	public void update (OperationbioEntity operationbio);
	
	public void delete (OperationbioEntity operationbio);
}
