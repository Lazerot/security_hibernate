/**
 * 
 */
package services.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import bbdd.tables.CuentausuarioEntity;
import bbdd.tables.CustomerEntity;
import bbdd.tables.DomainuserEntity;
import bbdd.tables.OperationEntity;
import bbdd.tables.OperationtypeEntity;
import bbdd.dto.CustomerAndOperationDTO;
import bbdd.dto.DomainUserDTO;
import bbdd.dto.OperationDTO;

/**
 * @author pmartinez
 *
 */
public class Test_Main {


	public static void showAll (CuentaUsuarioDAO cuentaUsuarioDao, OperationDAO operationDao) {
		List<CuentausuarioEntity> cuentaUsuarioList = cuentaUsuarioDao.selectAll();
		
		System.out.println("---- Usuarios----");
		
		for(CuentausuarioEntity cuentaUsuario :cuentaUsuarioList) {
			System.out.println("Id " + cuentaUsuario.getIdusuario());
			System.out.println("Nombre " + cuentaUsuario.getNombre());
			System.out.println("Apellidos " + cuentaUsuario.getApellidos());
		}
	}
	//Metodo Login DomainUser
	public static void showLogin (DomainUserDAO domainUserDao)
	{
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		DomainuserEntity domainuser = new DomainuserEntity();
		domainuser.setUseremail("cristina.arizon@cognicase.com.mx");
		domainuser.setUserpassword("A6xnQhbz4Vx2HuGl4lXwZ5U2I8iziLRFnhP5eNfIRvQ=");
//		domainuser.setUsername("gfgf.alfonso");

//		try 
//		{
//			List<?> domainuserlist = null;
//			domainuserlist = domainUserDao.login(domainuser);
//			
//		for(Object domain : domainuserlist) 
//		{
//			System.out.println("Id : " + ((DomainUserDTO)domain).getId());
//			System.out.println("Username : " +((DomainUserDTO)domain).getUsername());
//			System.out.println("Email : " + ((DomainUserDTO)domain).getUseremail());
//			System.out.println("Alias : " + ((DomainUserDTO)domain).getUseralias());
//		}
//		}catch (Exception e) {
//			System.out.println("El usuario o email no existe");
//		}
	}
	
	//Metodo operationType
	public static void showAllOperationtype(OperationTypeDAO operationTypeDao) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		OperationtypeEntity operationEntity =  operationTypeDao.selectbyOperationtypeEntity(845);
		
		
		System.out.println("Id" + operationEntity.getId());
		System.out.println("Extra" + operationEntity.getExtra());
		System.out.println("Auth" + operationEntity.getAuthmetadata());
		
	}
	
	//Metodo Operation Fechas
	public static void showAllOperationFecha (OperationDAO operationDao) throws ParseException 
	{
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
		String fecha1 = "2016-01-05 15:24:36";
		String fecha2 = "2016-01-05 16:28:54";
		
		Date fechaEntrada = format.parse(fecha1);
		Date fechaHasta = format.parse(fecha2);
		
		//Entidad
		OperationEntity operation = new OperationEntity();
		operation.setCreationdate(fechaEntrada);
		
		//DTO
		OperationDTO operationDTO = new OperationDTO();
		operationDTO.setCreationdatehasta(fechaHasta);
		
		List<?> operationList = null;
		operationList = operationDao.fechasDatosOperacion(operation, operationDTO);
		
		try 
		{
			
			for (Object list : operationList) 
			{
				
				System.out.println("---- Operaciones Asociadas ----");	
			
			System.out.println("Id " + ((OperationDTO) list).getId());
			System.out.println("Operacion ID " + ((OperationDTO) list).getOperationid());
			System.out.println("Fecha de la operacion " + ((OperationDTO) list).getCreationdate());
			System.out.println("Fecha hasta de las operaciones" + ((OperationDTO)list).getCreationdatehasta());
			System.out.println("Extra " + ((OperationDTO) list).getExtra());
				
			}
			
		}catch (Exception e)
		{
			System.out.println("No hay mas datos");
		}
	}
	
	//Metodo Customer
	public static void showAllCustomer (CustomerDAO customerDao) 
	{

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();

		CustomerEntity customer = new CustomerEntity();		
		customer.setNif("36059043B");
//		customer.setCellphone("679222014");
//		customer.setEmail("jlcumplido@gmail.com");
			
		List <?> customerList = null;
		customerList =  customerDao.datosUsuario(customer);		

		try 
		{

			for(Object list : customerList) 
			{
				
				System.out.println("---- Customers----");
				
				System.out.println("Id " +  ((CustomerAndOperationDTO) list).getId_c());
				System.out.println("Nombre " + ((CustomerAndOperationDTO) list).getName());
				System.out.println("Apellidos " + ((CustomerAndOperationDTO) list).getLastname());
				System.out.println("DNI " + ((CustomerAndOperationDTO) list).getNif());
				System.out.println("Email " + ((CustomerAndOperationDTO) list).getEmail());
				System.out.println("Telefono " +((CustomerAndOperationDTO) list).getCellphone());

			System.out.println("---- Operaciones Asociadas ----");	
			
				System.out.println("---- ----");	
			
			System.out.println("Id " + ((CustomerAndOperationDTO) list).getId_op());
			System.out.println("Operacion ID " + ((CustomerAndOperationDTO) list).getOperationid());
			System.out.println("Fecha de la operacion " + ((CustomerAndOperationDTO) list).getCreationdate());
			System.out.println("Extra " + ((CustomerAndOperationDTO) list).getExtra());
			}
		
		}catch (Exception e) 
			{
			System.out.println(" ");
			System.out.println("No hay mas datos");
			}
	}
	
	public static void main(String[] args) throws ParseException {

		CustomerDAOimpl customerDao = new CustomerDAOimpl();
		OperationDAOimpl operationDao = new OperationDAOimpl();
		OperationTypeDAOimpl operationtypeDao = new OperationTypeDAOimpl();
		DomainUserDAOimpl domainuserDao = new DomainUserDAOimpl();
		
		//Listado LOgin
		showLogin(domainuserDao);
		
		//Listado  OperationType Auth and extra
//		showAllOperationtype(operationtypeDao);
		
		//Listado de fechas y operaciones
//		showAllOperationFecha(operationDao);
		
		//Listado de Customers y Operaciones
//		showAllCustomer(customerDao);
		
		//Listado de la tabla	
		//showAll(cuentaUsuarioDao);
		
		java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());
		
		CuentausuarioEntity newUser = new CuentausuarioEntity(999,"Pedro","Lopez", date,"23311888P",
				"Cea Bermudez","Madrid","28003","607295009","lll@gmail.com","222q","ddd",22 ,"");
		
		//insert
//		cuentaUsuarioDao.insert(newUser);
		
//		System.out.println("Se ha insertado la siguiente persona: " + newUser.getIdusuario() +" "+ newUser.getNombre() +" " + newUser.getApellidos());
		
		//Delete
//		cuentaUsuarioDao.delete(newUser);
		
//		System.out.println("Se ha borrado la siguiente persona: " + newUser.getIdusuario() +" "+ newUser.getNombre() +" " + newUser.getApellidos());
		
		//Update
//		cuentaUsuarioDao.update(newUser);
		
//		System.out.println("Se ha actualizado la siguiente persona: " + newUser.getIdusuario() +" "+ newUser.getNombre() +" " + newUser.getApellidos());
		
	}

}
