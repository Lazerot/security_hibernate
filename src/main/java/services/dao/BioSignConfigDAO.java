/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.BiosignconfigEntity;

/**
 * @author pmartinez
 *
 */
public interface BioSignConfigDAO {
	
	public BiosignconfigEntity selectbyBiosignconfigEntity(int id);

	public List<BiosignconfigEntity> selectAll();

	public void insert (BiosignconfigEntity biosignconfig);
	
	public void update (BiosignconfigEntity biosignconfig);
	
	public void delete (BiosignconfigEntity biosignconfig);


}
