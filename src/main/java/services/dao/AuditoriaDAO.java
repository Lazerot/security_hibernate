/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.AuditoriaEntity;

/**
 * @author pmartinez
 *
 */
public interface AuditoriaDAO {
	
	public AuditoriaEntity selectbyAuditoriaEntity(int idauditoria);

	public List<AuditoriaEntity> selectAll();

	public void insert (AuditoriaEntity auditoria);
	
	public void update (AuditoriaEntity auditoria);
	
	public void delete (AuditoriaEntity auditoria);


}
