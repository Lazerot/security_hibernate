/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.UsedcodesEntity;

/**
 * @author pmartinez
 *
 */
public interface UsedCodesDAO {

	public UsedcodesEntity selectbyUsedcodesEntity(int id);

	public List<UsedcodesEntity> selectAll();

	public void insert (UsedcodesEntity usedcodes);
	
	public void update (UsedcodesEntity usedcodes);
	
	public void delete (UsedcodesEntity usedcodes);
}
