/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.NotificationtemplateEntity;

/**
 * @author pmartinez
 *
 */
public interface NotificationTemplateDAO {
	
	public NotificationtemplateEntity selectbyNotificationtemplateEntity(int id);

	public List<NotificationtemplateEntity> selectAll();

	public void insert (NotificationtemplateEntity notificationtemplate);
	
	public void update (NotificationtemplateEntity notificationtemplate);
	
	public void delete (NotificationtemplateEntity notificationtemplate);

}
