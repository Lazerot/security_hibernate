/**
 * 
 */
package services.dao;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import bbdd.dto.DomainUserDTO;
import bbdd.tables.DomainuserEntity;

/**
 * @author pmartinez
 *
 */
public interface DomainUserDAO {
	
	public DomainuserEntity selectbyDomainuserEntity(int id);

	public List<DomainuserEntity> selectAll();

	public void insert (DomainuserEntity domainuser);
	
	public void update (DomainuserEntity domainuser);
	
	public void delete (DomainuserEntity domainuser);

	public DomainUserDTO login (DomainuserEntity domainuser, HttpServletResponse response) throws Exception;
	
}
