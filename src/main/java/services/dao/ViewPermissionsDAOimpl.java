/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.ViewpermissionsEntity;

/**
 * @author pmartinez
 *
 */
public class ViewPermissionsDAOimpl implements ViewPermissionsDAO{

	@Override
	public ViewpermissionsEntity selectbyViewpermissionsEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		ViewpermissionsEntity viewPermissionsEntity = (ViewpermissionsEntity) session.get(ViewpermissionsEntity.class, id);
		return viewPermissionsEntity;
	}

	@Override
	public List<ViewpermissionsEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<ViewpermissionsEntity> criteria = builder.createQuery(ViewpermissionsEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<ViewpermissionsEntity> root = criteria.from(ViewpermissionsEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<ViewpermissionsEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<ViewpermissionsEntity> viewPermissionsEntity = q.getResultList();

		return viewPermissionsEntity;
	}

	@Override
	public void insert(ViewpermissionsEntity viewpermissions) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(viewpermissions);
		viewpermissions.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(ViewpermissionsEntity viewpermissions) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(viewpermissions);
		session.getTransaction().commit();
	}

	@Override
	public void delete(ViewpermissionsEntity viewpermissions) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(viewpermissions);
		session.getTransaction().commit();
	}

}
