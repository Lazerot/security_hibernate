/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.DomainusergroupEntity;

/**
 * @author pmartinez
 *
 */
public interface DomainUserGroupDAO {
	
	public DomainusergroupEntity selectbyDomainusergroupEntity(int id);

	public List<DomainusergroupEntity> selectAll();

	public void insert (DomainusergroupEntity domainusergroup);
	
	public void update (DomainusergroupEntity domainusergroup);
	
	public void delete (DomainusergroupEntity domainusergroup);


}
