/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.RespuestasencuestaEntity;

/**
 * @author pmartinez
 *
 */
public interface RespuestasEncuestaDAO {

	public RespuestasencuestaEntity selectbyRespuestasencuestaEntity(int id);

	public List<RespuestasencuestaEntity> selectAll();

	public void insert (RespuestasencuestaEntity respuestasencuesta);
	
	public void update (RespuestasencuestaEntity respuestasencuesta);
	
	public void delete (RespuestasencuestaEntity respuestasencuesta);
}
