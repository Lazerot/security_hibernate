/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.SmstypesEntity;

/**
 * @author pmartinez
 *
 */
public class SmsTypesDAOimpl implements SmsTypesDAO{

	@Override
	public SmstypesEntity selectbySmstypesEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		SmstypesEntity smsTypesEntity = (SmstypesEntity) session.get(SmstypesEntity.class, id);
		return smsTypesEntity;
	}

	@Override
	public List<SmstypesEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<SmstypesEntity> criteria = builder.createQuery(SmstypesEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<SmstypesEntity> root = criteria.from(SmstypesEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<SmstypesEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<SmstypesEntity> smsTypesEntity = q.getResultList();

		return smsTypesEntity;
	}

	@Override
	public void insert(SmstypesEntity smstypes) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(smstypes);
		smstypes.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(SmstypesEntity smstypes) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(smstypes);
		session.getTransaction().commit();
	}

	@Override
	public void delete(SmstypesEntity smstypes) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(smstypes);
		session.getTransaction().commit();
	}

}
