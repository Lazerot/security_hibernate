/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.VcconnectionEntity;

/**
 * @author pmartinez
 *
 */
public interface VcConnectionDAO {

	public VcconnectionEntity selectbyVcconnectionEntity(int id);

	public List<VcconnectionEntity> selectAll();

	public void insert (VcconnectionEntity vcconnection);
	
	public void update (VcconnectionEntity vcconnection);
	
	public void delete (VcconnectionEntity vcconnection);
}
