/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.DocumenttypeEntity;

/**
 * @author pmartinez
 *
 */
public class DocumentTypeDAOimpl implements DocumentTypeDAO{

	@Override
	public DocumenttypeEntity selectbyDocumenttypeEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		DocumenttypeEntity documentTypeEntity = (DocumenttypeEntity) session.get(DocumenttypeEntity.class, id);
		return documentTypeEntity;
	}

	@Override
	public List<DocumenttypeEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<DocumenttypeEntity> criteria = builder.createQuery(DocumenttypeEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<DocumenttypeEntity> root = criteria.from(DocumenttypeEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<DocumenttypeEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<DocumenttypeEntity> documentTypeEntity = q.getResultList();

		return documentTypeEntity;
	}

	@Override
	public void insert(DocumenttypeEntity documenttype) {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(documenttype);
		documenttype.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(DocumenttypeEntity documenttype) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(documenttype);
		session.getTransaction().commit();
	}

	@Override
	public void delete(DocumenttypeEntity documenttype) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(documenttype);
		session.getTransaction().commit();
	}

}
