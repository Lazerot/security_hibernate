/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.DocumentcertifiedEntity;

/**
 * @author pmartinez
 *
 */
public class DocumentCertifiedDAOimpl implements DocumentCertifiedDAO {

	@Override
	public DocumentcertifiedEntity selectbyDocumentcertifiedEntity(int id) {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		DocumentcertifiedEntity documentCertifiedEntity = (DocumentcertifiedEntity) session.get(DocumentcertifiedEntity.class, id);
		return documentCertifiedEntity;
	}

	@Override
	public List<DocumentcertifiedEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<DocumentcertifiedEntity> criteria = builder.createQuery(DocumentcertifiedEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<DocumentcertifiedEntity> root = criteria.from(DocumentcertifiedEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<DocumentcertifiedEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<DocumentcertifiedEntity> documentCertifiedEntity = q.getResultList();

		return documentCertifiedEntity;
	}

	@Override
	public void insert(DocumentcertifiedEntity documentcertified) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(documentcertified);
		documentcertified.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(DocumentcertifiedEntity documentcertified) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(documentcertified);
		session.getTransaction().commit();
	}

	@Override
	public void delete(DocumentcertifiedEntity documentcertified) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(documentcertified);
		session.getTransaction().commit();
	}

	
}
