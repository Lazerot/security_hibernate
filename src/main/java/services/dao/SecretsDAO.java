/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.SecretsEntity;

/**
 * @author pmartinez
 *
 */
public interface SecretsDAO {

	public SecretsEntity selectbySecretsEntity(String upn);

	public List<SecretsEntity> selectAll();

	public void insert (SecretsEntity secrets);
	
	public void update (SecretsEntity secrets);
	
	public void delete (SecretsEntity secrets);
}
