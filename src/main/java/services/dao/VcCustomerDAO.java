/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.VccustomerEntity;

/**
 * @author pmartinez
 *
 */
public interface VcCustomerDAO {

	public VccustomerEntity selectbyVccustomerEntityy(int id);

	public List<VccustomerEntity> selectAll();

	public void insert (VccustomerEntity vccustomer);
	
	public void update (VccustomerEntity vccustomer);
	
	public void delete (VccustomerEntity vccustomer);
}
