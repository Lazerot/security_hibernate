/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.LdapconfigEntity;

/**
 * @author pmartinez
 *
 */
public interface LdApConfigDAO {
	
	public LdapconfigEntity selectbyArcdocumentEntity(int id);

	public List<LdapconfigEntity> selectAll();

	public void insert (LdapconfigEntity ldapconfig);
	
	public void update (LdapconfigEntity ldapconfig);
	
	public void delete (LdapconfigEntity ldapconfig);


}
