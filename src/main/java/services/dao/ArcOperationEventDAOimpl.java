/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.ArcoperationeventEntity;

/**
 * @author pmartinez
 *
 */
public class ArcOperationEventDAOimpl implements ArcOperationEventDAO{

	@Override
	public ArcoperationeventEntity selectbyArcoperationeventEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		ArcoperationeventEntity arcOperationEventEntity = (ArcoperationeventEntity) session.get(ArcoperationeventEntity.class, id);
		return arcOperationEventEntity;
	}

	@Override
	public List<ArcoperationeventEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<ArcoperationeventEntity> criteria = builder.createQuery(ArcoperationeventEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<ArcoperationeventEntity> root = criteria.from(ArcoperationeventEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<ArcoperationeventEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<ArcoperationeventEntity> arcOperationEventEntity = q.getResultList();

		return arcOperationEventEntity;
	}

	@Override
	public void insert(ArcoperationeventEntity arcoperationevent) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(arcoperationevent);
		arcoperationevent.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(ArcoperationeventEntity arcoperationevent) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(arcoperationevent);
		session.getTransaction().commit();
	}
	

	@Override
	public void delete(ArcoperationeventEntity arcoperationevent) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(arcoperationevent);
		session.getTransaction().commit();
	}

}
