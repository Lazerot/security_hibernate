/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.SecretsEntity;

/**
 * @author pmartinez
 *
 */
public class SecretsDAOimpl implements SecretsDAO{

	@Override
	public SecretsEntity selectbySecretsEntity(String upn) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		SecretsEntity secretsEntity = (SecretsEntity) session.get(SecretsEntity.class, upn);
		return secretsEntity;
	}

	@Override
	public List<SecretsEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<SecretsEntity> criteria = builder.createQuery(SecretsEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<SecretsEntity> root = criteria.from(SecretsEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<SecretsEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<SecretsEntity> secretsEntity = q.getResultList();

		return secretsEntity;
	}

	@Override
	public void insert(SecretsEntity secrets) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		String upn = (String) session.save(secrets);
		secrets.setUpn(upn);
		session.getTransaction().commit();
	}

	@Override
	public void update(SecretsEntity secrets) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(secrets);
		session.getTransaction().commit();
	}

	@Override
	public void delete(SecretsEntity secrets) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(secrets);
		session.getTransaction().commit();
	}
	

}
