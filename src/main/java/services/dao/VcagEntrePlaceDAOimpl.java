/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.VcagentreplaceEntity;

/**
 * @author pmartinez
 *
 */
public class VcagEntrePlaceDAOimpl implements VcagEntrePlaceDAO{

	@Override
	public VcagentreplaceEntity selectbyVcagentreplaceEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		VcagentreplaceEntity vCagEntrePlaceEntity =(VcagentreplaceEntity) session.get(VcagentreplaceEntity.class, id);
		return vCagEntrePlaceEntity;
	}

	@Override
	public List<VcagentreplaceEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<VcagentreplaceEntity> criteria = builder.createQuery(VcagentreplaceEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<VcagentreplaceEntity> root = criteria.from(VcagentreplaceEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<VcagentreplaceEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<VcagentreplaceEntity> vCagEntrePlaceEntity = q.getResultList();

		return vCagEntrePlaceEntity;
	}

	@Override
	public void insert(VcagentreplaceEntity vcagentreplace) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(vcagentreplace);
		vcagentreplace.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(VcagentreplaceEntity vcagentreplace) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(vcagentreplace);
		session.getTransaction().commit();
	}

	@Override
	public void delete(VcagentreplaceEntity vcagentreplace) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(vcagentreplace);
		session.getTransaction().commit();
	}

}
