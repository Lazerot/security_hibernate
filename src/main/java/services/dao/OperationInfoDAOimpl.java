/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.OperationinfoEntity;

/**
 * @author pmartinez
 *
 */
public class OperationInfoDAOimpl implements OperationInfoDAO{

	@Override
	public OperationinfoEntity selectbyOperationinfoEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		OperationinfoEntity operationInfoEntity = (OperationinfoEntity) session.get(OperationinfoEntity.class, id);
		return operationInfoEntity;
	}

	@Override
	public List<OperationinfoEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<OperationinfoEntity> criteria = builder.createQuery(OperationinfoEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<OperationinfoEntity> root = criteria.from(OperationinfoEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<OperationinfoEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<OperationinfoEntity> operationInfoEntity = q.getResultList();

		return operationInfoEntity;
	}

	@Override
	public void insert(OperationinfoEntity operationinfo) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(operationinfo);
		operationinfo.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(OperationinfoEntity operationinfo) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(operationinfo);
		session.getTransaction().commit();
	}

	@Override
	public void delete(OperationinfoEntity operationinfo) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(operationinfo);
		session.getTransaction().commit();
	}

	
}
