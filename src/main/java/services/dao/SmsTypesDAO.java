/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.SmstypesEntity;

/**
 * @author pmartinez
 *
 */
public interface SmsTypesDAO {

	public SmstypesEntity selectbySmstypesEntity(int id);

	public List<SmstypesEntity> selectAll();

	public void insert (SmstypesEntity smstypes);
	
	public void update (SmstypesEntity smstypes);
	
	public void delete (SmstypesEntity smstypes);
}
