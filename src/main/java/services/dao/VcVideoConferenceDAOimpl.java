/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.VcvideoconferenceEntity;

/**
 * @author pmartinez
 *
 */
public class VcVideoConferenceDAOimpl implements VcVideoConferenceDAO{

	@Override
	public VcvideoconferenceEntity selectbyVcvideoconferenceEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		VcvideoconferenceEntity vcVideoConferenceEntity = (VcvideoconferenceEntity) session.get(VcvideoconferenceEntity.class, id);
		return vcVideoConferenceEntity;
	}

	@Override
	public List<VcvideoconferenceEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<VcvideoconferenceEntity> criteria = builder.createQuery(VcvideoconferenceEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<VcvideoconferenceEntity> root = criteria.from(VcvideoconferenceEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<VcvideoconferenceEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<VcvideoconferenceEntity> vcVideoConferenceEntity = q.getResultList();

		return vcVideoConferenceEntity;
	}

	@Override
	public void insert(VcvideoconferenceEntity vcvideoconference) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(vcvideoconference);
		vcvideoconference.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(VcvideoconferenceEntity vcvideoconference) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(vcvideoconference);
		session.getTransaction().commit();
	}

	@Override
	public void delete(VcvideoconferenceEntity vcvideoconference) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(vcvideoconference);
		session.getTransaction().commit();
	}

}
