/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.MetadatalabelEntity;

/**
 * @author pmartinez
 *
 */
public class MetaDataLabelDAOimpl implements MetaDataLabelDAO{

	@Override
	public MetadatalabelEntity selectbyMetadatalabelEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		MetadatalabelEntity metaDataLabelEntity = (MetadatalabelEntity) session.get(MetadatalabelEntity.class, id);
		return metaDataLabelEntity;
	}

	@Override
	public List<MetadatalabelEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<MetadatalabelEntity> criteria = builder.createQuery(MetadatalabelEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<MetadatalabelEntity> root = criteria.from(MetadatalabelEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<MetadatalabelEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<MetadatalabelEntity> metaDataLabelEntity = q.getResultList();

		return metaDataLabelEntity;
	}

	@Override
	public void insert(MetadatalabelEntity metadatalabel) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(metadatalabel);
		metadatalabel.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(MetadatalabelEntity metadatalabel) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(metadatalabel);
		session.getTransaction().commit();
	}

	@Override
	public void delete(MetadatalabelEntity metadatalabel) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(metadatalabel);
		session.getTransaction().commit();
	}

}
