/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.VcshareddocumentEntity;

/**
 * @author pmartinez
 *
 */
public interface VcSharedDocumentDAO {

	public VcshareddocumentEntity selectbyVcshareddocumentEntity(int id);

	public List<VcshareddocumentEntity> selectAll();

	public void insert (VcshareddocumentEntity vcshareddocument);
	
	public void update (VcshareddocumentEntity vcshareddocument);
	
	public void delete (VcshareddocumentEntity vcshareddocument);
}
