/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.AllowedcasEntity;

/**
 * @author pmartinez
 *
 */
public interface AllowedcasDAO {
	
	public AllowedcasEntity selectbyAllowedcasEntity(int id);

	public List<AllowedcasEntity> selectAll();

	public void insert (AllowedcasEntity allowedcas);
	
	public void update (AllowedcasEntity allowedcas);
	
	public void delete (AllowedcasEntity allowedcas);

}
