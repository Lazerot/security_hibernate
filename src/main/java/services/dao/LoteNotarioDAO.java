/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.LotenotarioEntity;

/**
 * @author pmartinez
 *
 */
public interface LoteNotarioDAO {
	
	public LotenotarioEntity selectbyLotenotarioEntity(int id);

	public List<LotenotarioEntity> selectAll();

	public void insert (LotenotarioEntity lotenotario);
	
	public void update (LotenotarioEntity lotenotario);
	
	public void delete (LotenotarioEntity lotenotario);


}
