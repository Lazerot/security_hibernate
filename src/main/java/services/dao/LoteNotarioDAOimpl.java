/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.LotenotarioEntity;

/**
 * @author pmartinez
 *
 */
public class LoteNotarioDAOimpl implements LoteNotarioDAO{

	@Override
	public LotenotarioEntity selectbyLotenotarioEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		LotenotarioEntity loteNotarioEntity = (LotenotarioEntity) session.get(LotenotarioEntity.class, id);
		return loteNotarioEntity;
	}

	@Override
	public List<LotenotarioEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<LotenotarioEntity> criteria = builder.createQuery(LotenotarioEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<LotenotarioEntity> root = criteria.from(LotenotarioEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<LotenotarioEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<LotenotarioEntity> loteNotarioEntity = q.getResultList();

		return loteNotarioEntity;
	}

	@Override
	public void insert(LotenotarioEntity lotenotario) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(lotenotario);
		lotenotario.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(LotenotarioEntity lotenotario) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(lotenotario);
		session.getTransaction().commit();
	}

	@Override
	public void delete(LotenotarioEntity lotenotario) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(lotenotario);
		session.getTransaction().commit();
	}

}
