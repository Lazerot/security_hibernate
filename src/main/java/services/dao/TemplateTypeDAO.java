/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.TemplatetypeEntity;

/**
 * @author pmartinez
 *
 */
public interface TemplateTypeDAO {

	public TemplatetypeEntity selectbyTemplatetypeEntity(int id);

	public List<TemplatetypeEntity> selectAll();

	public void insert (TemplatetypeEntity templatetype);
	
	public void update (TemplatetypeEntity templatetype);
	
	public void delete (TemplatetypeEntity templatetype);
}
