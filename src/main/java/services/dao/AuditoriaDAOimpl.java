/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.AuditoriaEntity;

/**
 * @author pmartinez
 *
 */
public class AuditoriaDAOimpl implements AuditoriaDAO{

	@Override
	public AuditoriaEntity selectbyAuditoriaEntity(int idauditoria) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		AuditoriaEntity auditoriaEntity = (AuditoriaEntity) session.get(AuditoriaEntity.class, idauditoria);
		return auditoriaEntity;
	}

	@Override
	public List<AuditoriaEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<AuditoriaEntity> criteria = builder.createQuery(AuditoriaEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<AuditoriaEntity> root = criteria.from(AuditoriaEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<AuditoriaEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<AuditoriaEntity> auditoriaEntity = q.getResultList();

		return auditoriaEntity;
	}

	@Override
	public void insert(AuditoriaEntity auditoria) {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int idauditoria = (int) session.save(auditoria);
		auditoria.setIdauditoria(idauditoria);
		session.getTransaction().commit();
	}

	@Override
	public void update(AuditoriaEntity auditoria) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(auditoria);
		session.getTransaction().commit();
	}

	@Override
	public void delete(AuditoriaEntity auditoria) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(auditoria);
		session.getTransaction().commit();	
	}

}
