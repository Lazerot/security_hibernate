/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.FsmopstatusEntity;

/**
 * @author pmartinez
 *
 */
public class FsmOpStatusDAOimpl implements FsmOpStatusDAO{

	@Override
	public FsmopstatusEntity selectbyFsmopstatusEntity(int id) {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		FsmopstatusEntity fsmOpStatusEntity = (FsmopstatusEntity) session.get(FsmopstatusEntity.class, id);
		return fsmOpStatusEntity;
	}

	@Override
	public List<FsmopstatusEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<FsmopstatusEntity> criteria = builder.createQuery(FsmopstatusEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<FsmopstatusEntity> root = criteria.from(FsmopstatusEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<FsmopstatusEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<FsmopstatusEntity> fsmOpStatusEntity = q.getResultList();

		return fsmOpStatusEntity;
	}

	@Override
	public void insert(FsmopstatusEntity fsmstatus) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(fsmstatus);
		fsmstatus.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(FsmopstatusEntity fsmstatus) {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(fsmstatus);
		session.getTransaction().commit();;
	}

	@Override
	public void delete(FsmopstatusEntity fsmstatus) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(fsmstatus);
		session.getTransaction().commit();
	}

}
