/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.OperationtypeEntity;

/**
 * @author pmartinez
 *
 */
public interface OperationTypeDAO {

	public OperationtypeEntity selectbyOperationtypeEntity(int id);

	public List<OperationtypeEntity> selectAll();

	public void insert (OperationtypeEntity operationtype);
	
	public void update (OperationtypeEntity operationtype);
	
	public void delete (OperationtypeEntity operationtype);
}
