/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.CustomeroperationEntity;

/**
 * @author pmartinez
 *
 */
public interface CustomerOperationDAO {
	
	public CustomeroperationEntity selectbyCustomeroperationEntity(int id);

	public List<CustomeroperationEntity> selectAll();

	public void insert (CustomeroperationEntity customeroperation);
	
	public void update (CustomeroperationEntity customeroperation);
	
	public void delete (CustomeroperationEntity customeroperation);


}
