/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.VideosEntity;

/**
 * @author pmartinez
 *
 */
public interface VideosDAO {

	public VideosEntity selectbyVideosEntity(int id);

	public List<VideosEntity> selectAll();

	public void insert (VideosEntity videos);
	
	public void update (VideosEntity videos);
	
	public void delete (VideosEntity videos);
}
