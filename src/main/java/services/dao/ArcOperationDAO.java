/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.ArcoperationEntity;

/**
 * @author pmartinez
 *
 */
public interface ArcOperationDAO {
	
	public ArcoperationEntity selectbyArcoperationEntity(int id);

	public List<ArcoperationEntity> selectAll();

	public void insert (ArcoperationEntity arcoperation);
	
	public void update (ArcoperationEntity arcoperation);
	
	public void delete (ArcoperationEntity arcoperation);


}
