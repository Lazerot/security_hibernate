/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.ConfiguracionloteEntity;

/**
 * @author pmartinez
 *
 */
public interface ConfiguracionLoteDAO {
	
	public ConfiguracionloteEntity selectbyConfiguracionloteEntity(int id);

	public List<ConfiguracionloteEntity> selectAll();

	public void insert (ConfiguracionloteEntity configuracionlote);
	
	public void update (ConfiguracionloteEntity configuracionlote);
	
	public void delete (ConfiguracionloteEntity configuracionlote);

}
