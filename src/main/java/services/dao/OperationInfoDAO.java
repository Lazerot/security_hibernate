/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.OperationinfoEntity;

/**
 * @author pmartinez
 *
 */
public interface OperationInfoDAO {
	
	public OperationinfoEntity selectbyOperationinfoEntity(int id);

	public List<OperationinfoEntity> selectAll();

	public void insert (OperationinfoEntity operationinfo);
	
	public void update (OperationinfoEntity operationinfo);
	
	public void delete (OperationinfoEntity operationinfo);

}
