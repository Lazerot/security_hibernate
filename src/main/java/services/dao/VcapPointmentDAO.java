/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.VcappointmentEntity;

/**
 * @author pmartinez
 *
 */
public interface VcapPointmentDAO {

	public VcappointmentEntity selectbyVcappointmentEntity(int id);

	public List<VcappointmentEntity> selectAll();

	public void insert (VcappointmentEntity vcappointment);
	
	public void update (VcappointmentEntity vcappointment);
	
	public void delete (VcappointmentEntity vcappointment);
}
