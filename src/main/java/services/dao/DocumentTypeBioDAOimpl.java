/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.DocumenttypebioEntity;

/**
 * @author pmartinez
 *
 */
public class DocumentTypeBioDAOimpl implements DocumentTypeBioDAO{

	@Override
	public DocumenttypebioEntity selectbyDocumenttypebioEntity(int id) {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		DocumenttypebioEntity documentTypeBioEntity = (DocumenttypebioEntity) session.get(DocumenttypebioEntity.class, id);
		return documentTypeBioEntity;
	}

	@Override
	public List<DocumenttypebioEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<DocumenttypebioEntity> criteria = builder.createQuery(DocumenttypebioEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<DocumenttypebioEntity> root = criteria.from(DocumenttypebioEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<DocumenttypebioEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<DocumenttypebioEntity> documentTypeBioEntity = q.getResultList();

		return documentTypeBioEntity;
	}

	@Override
	public void insert(DocumenttypebioEntity documenttypebio) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(documenttypebio);
		documenttypebio.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(DocumenttypebioEntity documenttypebio) {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(documenttypebio);
		session.getTransaction().commit();
	}

	@Override
	public void delete(DocumenttypebioEntity documenttypebio) {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(documenttypebio);
		session.getTransaction().commit();
	}

}
