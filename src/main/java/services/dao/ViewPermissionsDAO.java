/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.ViewpermissionsEntity;

/**
 * @author pmartinez
 *
 */
public interface ViewPermissionsDAO {

	public ViewpermissionsEntity selectbyViewpermissionsEntity(int id);

	public List<ViewpermissionsEntity> selectAll();

	public void insert (ViewpermissionsEntity viewpermissions);
	
	public void update (ViewpermissionsEntity viewpermissions);
	
	public void delete (ViewpermissionsEntity viewpermissions);
}
