/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.DocumenteventEntity;

/**
 * @author pmartinez
 *
 */
public interface DocumentEventDAO {

	public DocumenteventEntity selectbyDocumenteventEntity(int id);

	public List<DocumenteventEntity> selectAll();

	public void insert (DocumenteventEntity documentevent);
	
	public void update (DocumenteventEntity documentevent);
	
	public void delete (DocumenteventEntity documentevent);

}
