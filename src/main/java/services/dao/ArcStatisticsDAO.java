/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.ArcstatisticsEntity;

/**
 * @author pmartinez
 *
 */
public interface ArcStatisticsDAO {
	
	public ArcstatisticsEntity selectbyArcstatisticsEntity(int id);

	public List<ArcstatisticsEntity> selectAll();

	public void insert (ArcstatisticsEntity arcstatistics);
	
	public void update (ArcstatisticsEntity arcstatistics);
	
	public void delete (ArcstatisticsEntity arcstatistics);


}
