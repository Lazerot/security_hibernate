/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.DocumenttypebioEntity;

/**
 * @author pmartinez
 *
 */
public interface DocumentTypeBioDAO {
	
	public DocumenttypebioEntity selectbyDocumenttypebioEntity(int id);

	public List<DocumenttypebioEntity> selectAll();

	public void insert (DocumenttypebioEntity documenttypebio);
	
	public void update (DocumenttypebioEntity documenttypebio);
	
	public void delete (DocumenttypebioEntity documenttypebio);


}
