/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.NotificationeventEntity;

/**
 * @author pmartinez
 *
 */
public interface NotificationEventDAO {
	
	public NotificationeventEntity selectbyNotificationeventEntity(int id);

	public List<NotificationeventEntity> selectAll();

	public void insert (NotificationeventEntity notificationevent);
	
	public void update (NotificationeventEntity notificationevent);
	
	public void delete (NotificationeventEntity notificationevent);

}
