/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.DocumenttypeEntity;

/**
 * @author pmartinez
 *
 */
public interface DocumentTypeDAO {
	
	public DocumenttypeEntity selectbyDocumenttypeEntity(int id);

	public List<DocumenttypeEntity> selectAll();

	public void insert (DocumenttypeEntity documenttype);
	
	public void update (DocumenttypeEntity documenttype);
	
	public void delete (DocumenttypeEntity documenttype);


}
