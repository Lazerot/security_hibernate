/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.CapturedimageEntity;

/**
 * @author pmartinez
 *
 */
public interface CapturedImageDAO {
	
	public CapturedimageEntity selectbyCapturedimageEntity(int id);

	public List<CapturedimageEntity> selectAll();

	public void insert (CapturedimageEntity capturedimage);
	
	public void update (CapturedimageEntity capturedimage);
	
	public void delete (CapturedimageEntity capturedimage);


}
