/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.ActatemplateoptypeEntity;

/**
 * @author pmartinez
 *
 */
public class ActaTemplateOpTypeDAOimpl implements ActaTemplateOpTypeDAO {

	@Override
	public ActatemplateoptypeEntity selectbyActatemplateoptypeEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		ActatemplateoptypeEntity actaTemplateOpTypeEntity = (ActatemplateoptypeEntity) session.get(ActatemplateoptypeEntity.class, id);
		
		return actaTemplateOpTypeEntity;
	}

	@Override
	public List<ActatemplateoptypeEntity> selectAll() {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<ActatemplateoptypeEntity> criteria = builder.createQuery(ActatemplateoptypeEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<ActatemplateoptypeEntity> root = criteria.from(ActatemplateoptypeEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<ActatemplateoptypeEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<ActatemplateoptypeEntity> actaTemplateOpTypeEntity = q.getResultList();

		return actaTemplateOpTypeEntity;
	}

	@Override
	public void insert(ActatemplateoptypeEntity actaOpType) {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(actaOpType);
		actaOpType.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(ActatemplateoptypeEntity actaOpType) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(actaOpType);
		session.getTransaction().commit();
		
	}

	@Override
	public void delete(ActatemplateoptypeEntity actaOpType) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(actaOpType);
		session.getTransaction().commit();
		
	}

}
