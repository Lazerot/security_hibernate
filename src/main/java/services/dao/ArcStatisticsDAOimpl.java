/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;


import bbdd.tables.ArcstatisticsEntity;

/**
 * @author pmartinez
 *
 */
public class ArcStatisticsDAOimpl implements ArcStatisticsDAO {

	@Override
	public ArcstatisticsEntity selectbyArcstatisticsEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		ArcstatisticsEntity arcStatisticsEntity = (ArcstatisticsEntity) session.get(ArcstatisticsEntity.class, id);
		return arcStatisticsEntity;
	}

	@Override
	public List<ArcstatisticsEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<ArcstatisticsEntity> criteria = builder.createQuery(ArcstatisticsEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<ArcstatisticsEntity> root = criteria.from(ArcstatisticsEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<ArcstatisticsEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<ArcstatisticsEntity> arcStatisticsEntity = q.getResultList();

		return arcStatisticsEntity;
	}

	@Override
	public void insert(ArcstatisticsEntity arcstatistics) {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(arcstatistics);
		arcstatistics.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(ArcstatisticsEntity arcstatistics) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(arcstatistics);
		session.getTransaction().commit();
	}

	@Override
	public void delete(ArcstatisticsEntity arcstatistics) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(arcstatistics);
		session.getTransaction().commit();
	}

}
