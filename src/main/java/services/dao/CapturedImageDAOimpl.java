/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.CapturedimageEntity;

/**
 * @author pmartinez
 *
 */
public class CapturedImageDAOimpl implements CapturedImageDAO{

	@Override
	public CapturedimageEntity selectbyCapturedimageEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		CapturedimageEntity capturedImageEntity = (CapturedimageEntity) session.get(CapturedimageEntity.class, id);
		return capturedImageEntity;
	}

	@Override
	public List<CapturedimageEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<CapturedimageEntity> criteria = builder.createQuery(CapturedimageEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<CapturedimageEntity> root = criteria.from(CapturedimageEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<CapturedimageEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<CapturedimageEntity> capturedImageEntity = q.getResultList();

		return capturedImageEntity;
	}

	@Override
	public void insert(CapturedimageEntity capturedimage) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(capturedimage);
		capturedimage.setId(id);
		session.getTransaction().commit();
	}
	
	@Override
	public void update(CapturedimageEntity capturedimage) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(capturedimage);
		session.getTransaction().commit();	
	}

	@Override
	public void delete(CapturedimageEntity capturedimage) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(capturedimage);
		session.getTransaction().commit();
	}

}
