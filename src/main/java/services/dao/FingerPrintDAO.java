/**
 * 
 */
package services.dao;

import java.util.List;

import bbdd.tables.FingerPrintEntity;
import bbdd.tables.OperationEntity;
import exceptions.Internal_server_error_exception;
import services.digitalFinger.FingerprintInfo;

/**
 * @author pj
 *
 */
public interface FingerPrintDAO {

	public FingerPrintEntity selectbyFingerEntity(String userUuid);
	
	public void enrollment (FingerprintInfo fingerPrintInfo) throws Exception;
	
	public Boolean verify (FingerprintInfo fingerPrintInfo, FingerPrintEntity fingerPrint);
	
	public List<?> getFingerPrintByUserUuid (FingerPrintEntity fingerPrint);
	
	public List<FingerPrintEntity> selectAll();

	public void insert (FingerPrintEntity fingerid);
	
	public void update (FingerPrintEntity fingerid);
	
	public void delete (FingerPrintEntity fingerid);
	
	public String firma (FingerPrintEntity fingerid, String fingerprintB64);
}
