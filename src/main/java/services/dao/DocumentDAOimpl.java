/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.DocumentEntity;

/**
 * @author pmartinez
 *
 */
public class DocumentDAOimpl implements DocumentDAO{

	@Override
	public DocumentEntity selectbyDocumentEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		DocumentEntity documentEntity = (DocumentEntity) session.get(DocumentEntity.class, id);
		return documentEntity;
	}

	@Override
	public List<DocumentEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<DocumentEntity> criteria = builder.createQuery(DocumentEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<DocumentEntity> root = criteria.from(DocumentEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<DocumentEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<DocumentEntity> documentEntity = q.getResultList();

		return documentEntity;
	}

	@Override
	public void insert(DocumentEntity document) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(document);
		document.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(DocumentEntity document) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(document);
		session.getTransaction().commit();
	}

	@Override
	public void delete(DocumentEntity document) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(document);
		session.getTransaction().commit();
	}

}
