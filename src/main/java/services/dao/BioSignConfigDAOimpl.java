/**
 * 
 */
package services.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import bbdd.tables.BiosignconfigEntity;

/**
 * @author pmartinez
 *
 */
public class BioSignConfigDAOimpl implements BioSignConfigDAO {

	@Override
	public BiosignconfigEntity selectbyBiosignconfigEntity(int id) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		
		BiosignconfigEntity bioSignConfigEntity = (BiosignconfigEntity) session.get(BiosignconfigEntity.class, id);
		return bioSignConfigEntity;
	}

	@Override
	public List<BiosignconfigEntity> selectAll() {

		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		// Create a CriteriaBuilder instance by calling the Session.getCriteriaBuilder() method.
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    
	    // Create a query object by creating an instance of the CriteriaQuery interface.
	    CriteriaQuery<BiosignconfigEntity> criteria = builder.createQuery(BiosignconfigEntity.class);
	    
	    // Set the query Root by calling the from() method on the CriteriaQuery object to define a range variable in FROM clause.
	    Root<BiosignconfigEntity> root = criteria.from(BiosignconfigEntity.class);
	    
	    // Specify what the type of the query result will be by calling the select() method of the CriteriaQuery object.	    
	    criteria.select(root);
	    
	    // Prepare the query for execution by creating a org.hibernate.query.Query instance by calling the Session.createQuery() method, 
	    // specifying the type of the query result.
	    Query<BiosignconfigEntity> q = session.createQuery(criteria);
		
	    // Execute the query by calling the getResultList() or getSingleResult() method on the org.hibernate.query.Query object.
	    List<BiosignconfigEntity> bioSignConfigEntity = q.getResultList();

		return bioSignConfigEntity;
	}

	@Override
	public void insert(BiosignconfigEntity biosignconfig) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		int id = (int) session.save(biosignconfig);
		biosignconfig.setId(id);
		session.getTransaction().commit();
	}

	@Override
	public void update(BiosignconfigEntity biosignconfig) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.merge(biosignconfig);
		session.getTransaction().commit();
	}

	@Override
	public void delete(BiosignconfigEntity biosignconfig) {
		
		SessionFactory factory = HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		session.delete(biosignconfig);
		session.getTransaction().commit();	
	}
	

}
