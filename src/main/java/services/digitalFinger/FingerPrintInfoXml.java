/**
 * 
 */
package services.digitalFinger;

import java.util.Date;

/**
 * @author pj
 *
 */
public class FingerPrintInfoXml {

private int fingerCode;
    
    // Minutiae or finger image
    private String base64Fingerprint;
    
    private Date fecha;
    
    private String cliente_id;
    
    private String tsaGentime;
    private String tsaSignerdata;
    private String tsaSerialnumber;
    private String tsaPolicy;
    private String tsaB64data;
    private String remote_ip;
    
    public int getFingerCode() {
        return fingerCode;
    }
    public void setFingerCode(int fingerCode) {
        this.fingerCode = fingerCode;
    }
    public Date getFecha() {
        return fecha;
    }
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    public String geBase64Fingerprint() {
        return base64Fingerprint;
    }
    public void setBase64Fingerprint(String base64Fingerprint) {
        this.base64Fingerprint = base64Fingerprint;
    }
    
    public String getCliente_id() {
        return cliente_id;
    }
    public void setCliente_id(String cliente_id) {
        this.cliente_id = cliente_id;
    }
    
    public String getTsaGentime() {
        return tsaGentime;
    }
    public void setTsaGentime(String tsaGentime) {
        this.tsaGentime = tsaGentime;
    }
    public String getTsaSignerdata() {
        return tsaSignerdata;
    }
    public void setTsaSignerdata(String tsaSignerdata) {
        this.tsaSignerdata = tsaSignerdata;
    }
    public String getTsaSerialnumber() {
        return tsaSerialnumber;
    }
    public void setTsaSerialnumber(String tsaSerialnumber) {
        this.tsaSerialnumber = tsaSerialnumber;
    }
    public String getTsaPolicy() {
        return tsaPolicy;
    }
    public void setTsaPolicy(String tsaPolicy) {
        this.tsaPolicy = tsaPolicy;
    }
    public String getTsaB64data() {
        return tsaB64data;
    }
    public void setTsaB64data(String tsaB64data) {
        this.tsaB64data = tsaB64data;
    }
    public String getRemote_ip() {
        return remote_ip;
    }
    public void setRemote_ip(String remote_ip) {
        this.remote_ip = remote_ip;
    }
}
