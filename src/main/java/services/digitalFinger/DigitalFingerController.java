package services.digitalFinger;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.tomcat.util.codec.binary.Base64;

//import org.apache.commons.codec.binary.Base64;

import com.digitalpersona.uareu.Engine;
import com.digitalpersona.uareu.Fmd;
import com.digitalpersona.uareu.UareUException;
import com.digitalpersona.uareu.UareUGlobal;


public class DigitalFingerController {
	
	
	private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DigitalFingerController.class.getName());
	
	/**
	 * Reads the finger position from image data
	 * @param imageData
	 * @param nif
	 * @return
	 */
	public static int getFingerPosition(DigitalFingerFid imageData, String userUuid) {
		
		String loggerHeader = "[getFingerPosition() - userUuid " + userUuid + "] -> ";
		
		int fingerPosition = -1;
		
		if (imageData != null && imageData.getViews() != null) {
			
			DigitalFingerFiv viewData = (DigitalFingerFiv) imageData.getViews()[0];
			if (viewData != null) {
				fingerPosition = viewData.getFingerPosition();
				logger.debug(loggerHeader + "Finger position " + fingerPosition);
			}
			else {
				logger.error(loggerHeader + "Error while getting finger position from fingerprint");
			}
		}
		
		return fingerPosition;
	}
	
	
	/**
	 * Reads the finger position from minutiae data
	 * @param minutiaeData
	 * @param userUuid
	 * @return
	 */
	public static int getFingerPosition(DigitalFingerFmd minutiaeData, String userUuid) {
		
		String loggerHeader = "[getFingerPosition() - userUuid " + userUuid + "] -> ";
		
		int fingerPosition = -1;
		
		if (minutiaeData != null && minutiaeData.getViews() != null) {
			
			DigitalFingerFmv viewData = (DigitalFingerFmv) minutiaeData.getViews()[0];
			if (viewData != null) {
				fingerPosition = viewData.getFingerPosition();
				logger.debug(loggerHeader + "Finger position " + fingerPosition);
			}
			else {
				logger.error(loggerHeader + "Error while getting finger position from fingerprint");
			}
		}
		
		return fingerPosition;
	}
	
	
	/**
	 * Get image data from the view of the image data
	 * Initially image are an array of bits. They are coded in base64 to be trasmitted
	 * @param imageData
	 * @param nif
	 * @return
	 */
	public static byte[] getImageData(DigitalFingerFid imageData, String userUuid) {
		
		String loggerHeader = "[getImageData() - userUuid " + userUuid + "] -> ";
		byte[] imageRawData = null;
		
		if (imageData != null && imageData.getViews() != null) {
			
			DigitalFingerFiv viewData = (DigitalFingerFiv) imageData.getViews()[0];
			
			imageRawData = Base64.decodeBase64(viewData.getRawImage());
		}
		else {
			logger.error(loggerHeader + "Error while getting image from fingerprint");
		}
		
		return imageRawData;
	}
	
	
	/**
	 * Get image data from the view of the image data
	 * Initially image are an array of bits. They are coded in base64 to be trasmitted
	 * @param imageData
	 * @param nif
	 * @return
	 */
	public static String getImageDataBase64(DigitalFingerFid imageData, String userUuid) {
		
		String loggerHeader = "[getImageDataBase64() - userUuid " + userUuid + "] -> ";
		String imageRawData = null;
		
		if (imageData != null && imageData.getViews() != null) {
			
			DigitalFingerFiv viewData = (DigitalFingerFiv) imageData.getViews()[0];
			
			imageRawData = viewData.getRawImage();
		}
		else {
			logger.error(loggerHeader + "Error while getting image from fingerprint");
		}
		
		return imageRawData;
	}

	/**
	 * Minutiae are created from image
	 * @param imageData: image object from reader
	 * @param userUuid
	 * @return
	 */
	public static Fmd getFmd(DigitalFingerFid imageData, String userUuid) {

		String loggerHeader = "[getFmd() - userUuid " + userUuid + "] -> ";

		Fmd minutiaeData = null;

		if (imageData != null && imageData.getViews() != null) {

			DigitalFingerFiv viewData = (DigitalFingerFiv) imageData.getViews()[0];

			Fmd.Format minutiaeFormat = null;
			if (imageData.getFormat().contains("ANSI")) {
				minutiaeFormat = Fmd.Format.ANSI_378_2004;
			}
			else {
				minutiaeFormat = Fmd.Format.ISO_19794_2_2005;
			}

			logger.debug(loggerHeader + "Fmd format " + minutiaeFormat.name());

			byte[] imageRawData = null;
			imageRawData = Base64.decodeBase64(viewData.getRawImage());

			if (imageRawData != null) {
				logger.debug(loggerHeader + "Fingerprint has been Base64 decoded");
				try {
					Engine engine = UareUGlobal.GetEngine();
					minutiaeData = engine.CreateFmd(imageRawData, 
							viewData.getWidth(), 
							viewData.getHeight(), 
							imageData.getResolution(), 
							viewData.getFingerPosition(), 
							imageData.getCbeffId(), 
							minutiaeFormat);
					if (minutiaeData != null) {
						logger.info(loggerHeader + "Fmd has been created from fingerprint");
					}
					else {
						logger.error(loggerHeader + "Fmd has not been created from fingerprint");
					}

				} catch (UareUException e) {
					logger.error(loggerHeader + "Error while creating Fmd from fingerprint");
					e.printStackTrace();
				}
			}
			else {
				logger.error(loggerHeader + "Error while decoding Base64 from fingerprint");
			}

		}

		return minutiaeData;
	}
	
	/**
	 * Get transmitted minutiae
	 * Minutiae sdk object is constructed from transmitted minutiae
	 * @param minutiaeData
	 * @param userUuid
	 * @return
	 */
	public static String getFmdBase64(DigitalFingerFmd minutiaeData, String userUuid) {

		String loggerHeader = "[getFmd() - userUuid " + userUuid + "] -> ";

//		Fmd fmd = null;
		String minutaeImage = null;
		
		if (minutiaeData != null && minutiaeData.getViews() != null) {
			
			DigitalFingerFmv view = minutiaeData.getViews()[0];
			
			minutaeImage = view.getBytes();

		}

		return minutaeImage;
	}
	
	public static Fmd getFmd(DigitalFingerFmd minutiaeData, String userUuid) {

		String loggerHeader = "[getFmd() - userUuid " + userUuid + "] -> ";

		Fmd fmd = null;
		
		if (minutiaeData != null && minutiaeData.getViews() != null) {
			
			byte[] minutiaeBytes = Base64.decodeBase64(minutiaeData.getBytes());
			
			fmd = getFmdFromBytes(minutiaeBytes, userUuid);

		}

		return fmd;
	}
	
	
	/**
	 * Get minutiae from bytes
	 * @param minutiaeBytes
	 * @param userUuid
	 * @return
	 */
	public static Fmd getFmdFromBytes(byte[] minutiaeBytes, String userUuid) {

		String loggerHeader = "[getFmdFromBytes() - userUuid " + userUuid + "] -> ";

		Fmd fmd = null;
		
		try {
			fmd = UareUGlobal.GetImporter().ImportFmd(minutiaeBytes,Fmd.Format.ANSI_378_2004,Fmd.Format.ANSI_378_2004);
		} catch (UareUException e) {
			logger.info(loggerHeader + "Error while importing minutiae");
			e.printStackTrace();
		}
		
		if (fmd != null) {
			logger.info(loggerHeader + "Fmd has been created from fingerprint");
		}
		else {
			logger.error(loggerHeader + "Fmd has not been created from fingerprint");
		}

		return fmd;
	}

	/**
	 * Compare if the two user's fingerprints (from the same finger) are equal
	 * @param minutiaeData: Minutiae sended now
	 * @param minutiaeDB: Minutiae in database
	 * @param userUuid
	 * @return
	 */
	public static boolean checkPrints(Fmd minutiaeData, Fmd minutiaeDB, String userUuid) {
		
		String loggerHeader = "[checkPrints() - userUuid " + userUuid + "] -> ";
		
		boolean sameFingerPrint = false;
		
		if (minutiaeData != null && minutiaeDB != null) {

			Engine engine = UareUGlobal.GetEngine();
			
			int falsematch_rate;
			try {
				falsematch_rate = engine.Compare(minutiaeData, 0, minutiaeDB, 0);
				
				int target_falsematch_rate = Engine.PROBABILITY_ONE / 100000;
				if(falsematch_rate < target_falsematch_rate){
					logger.info(loggerHeader + "Fingerprints matched");
		            sameFingerPrint = true;
		            
		            String str = String.format("dissimilarity score: 0x%x", falsematch_rate);
		            logger.debug(loggerHeader + str);
		            
		            str = String.format("false match rate: %e", (double)(falsematch_rate / Engine.PROBABILITY_ONE));
		            logger.debug(loggerHeader + str);
		        }
		        else{
		            logger.error(loggerHeader + "Fingerprints did not match. False match rate: " + falsematch_rate + " < " + target_falsematch_rate);
		        }
				
			} catch (UareUException e) {
				logger.error(loggerHeader + "Error while comparing fingerprints");
				e.printStackTrace();
			}
			
		}
		
		return sameFingerPrint;
		
	}
	

}
