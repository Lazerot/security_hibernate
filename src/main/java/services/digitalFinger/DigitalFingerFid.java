package services.digitalFinger;


public class DigitalFingerFid {
	
	private String bytes;

	private int aquisitionLevel;
	
	private int bpp;
	
	private  int captureDeviceId;
	
	private int cbeffId;
	
	private int compression;
	
	private int fingerCount;
	
	private String format;
	
	private int imageResolution;
	
	private int resolution;
	
	private int scaleUnits;
	
	private int scanResolution;
	
	private DigitalFingerFiv[] views;
	
	
	
	public String getBytes() {
		return bytes;
	}

	public void setBytes(String bytes) {
		this.bytes = bytes;
	}

	public int getAquisitionLevel() {
		return aquisitionLevel;
	}

	public void setAquisitionLevel(int aquisitionLevel) {
		this.aquisitionLevel = aquisitionLevel;
	}

	public int getBpp() {
		return bpp;
	}

	public void setBpp(int bpp) {
		this.bpp = bpp;
	}

	public int getCaptureDeviceId() {
		return captureDeviceId;
	}

	public void setCaptureDeviceId(int captureDeviceId) {
		this.captureDeviceId = captureDeviceId;
	}

	public int getCbeffId() {
		return cbeffId;
	}

	public void setCbeffId(int cbeffId) {
		this.cbeffId = cbeffId;
	}

	public int getCompression() {
		return compression;
	}

	public void setCompression(int compression) {
		this.compression = compression;
	}

	public int getFingerCount() {
		return fingerCount;
	}

	public void setFingerCount(int fingerCount) {
		this.fingerCount = fingerCount;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public int getImageResolution() {
		return imageResolution;
	}

	public void setImageResolution(int imageResolution) {
		this.imageResolution = imageResolution;
	}

	public int getResolution() {
		return resolution;
	}

	public void setResolution(int resolution) {
		this.resolution = resolution;
	}

	public int getScaleUnits() {
		return scaleUnits;
	}

	public void setScaleUnits(int scaleUnits) {
		this.scaleUnits = scaleUnits;
	}

	public int getScanResolution() {
		return scanResolution;
	}

	public void setScanResolution(int scanResolution) {
		this.scanResolution = scanResolution;
	}

	public DigitalFingerFiv[] getViews() {
		return views;
	}

	public void setViews(DigitalFingerFiv[] views) {
		this.views = views;
	}

	
	
	
	
	

}
