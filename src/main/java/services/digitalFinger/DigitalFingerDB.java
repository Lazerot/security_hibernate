package services.digitalFinger;

import java.util.Date;

public class DigitalFingerDB {

	private String userUuid;
	
	private Date date;
	
	private int fingerPosition;
	
	private byte[] minutiae;
	
	private String fingerImageFilePath;
	
	private String minutiaeSignedFilePath;
	
	private String fingerImageSignedFilePath;

	


	public String getUserUuid() {
		return userUuid;
	}

	public void setUserUuid(String userUuid) {
		this.userUuid = userUuid;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getFingerPosition() {
		return fingerPosition;
	}

	public void setFingerPosition(int fingerPosition) {
		this.fingerPosition = fingerPosition;
	}

	public byte[] getMinutiae() {
		return minutiae;
	}

	public void setMinutiae(byte[] minutiae) {
		this.minutiae = minutiae;
	}

	public String getFingerImageFilePath() {
		return fingerImageFilePath;
	}

	public void setFingerImageFilePath(String fingerImageFilePath) {
		this.fingerImageFilePath = fingerImageFilePath;
	}
	
	public String getMinutiaeSignedFilePath() {
		return minutiaeSignedFilePath;
	}

	public void setMinutiaeSignedFilePath(String minutiaeSignedFilePath) {
		this.minutiaeSignedFilePath = minutiaeSignedFilePath;
	}

	public String getFingerImageSignedFilePath() {
		return fingerImageSignedFilePath;
	}

	public void setFingerImageSignedFilePath(String fingerImageSignedFilePath) {
		this.fingerImageSignedFilePath = fingerImageSignedFilePath;
	}

	
	
	
}
