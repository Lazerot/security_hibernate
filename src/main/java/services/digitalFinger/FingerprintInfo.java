package services.digitalFinger;


public class FingerprintInfo {

	private String userUuid;
	
	private DigitalFingerFid imageData;
	
	private DigitalFingerFmd minutiaeData;
	
	
	public String getUserUuid() {
		return userUuid;
	}

	public void setUserUuid(String userUuid) {
		this.userUuid = userUuid;
	}

	public DigitalFingerFid getImageData() {
		return imageData;
	}

	public void setImageData(DigitalFingerFid imageData) {
		this.imageData = imageData;
	}

	public DigitalFingerFmd getMinutiaeData() {
		return minutiaeData;
	}

	public void setMinutiaeData(DigitalFingerFmd minutiaeData) {
		this.minutiaeData = minutiaeData;
	}

		

		
}
