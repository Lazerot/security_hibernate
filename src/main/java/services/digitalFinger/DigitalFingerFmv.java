package services.digitalFinger;

public class DigitalFingerFmv {

	private String bytes;
	
	private int fingerPosition;
	
	private int minutiaeCount;
	
	private int quality;
	
	private int viewNumber;
	
	

	

	public String getBytes() {
		return bytes;
	}

	public void setBytes(String bytes) {
		this.bytes = bytes;
	}

	public int getFingerPosition() {
		return fingerPosition;
	}

	public void setFingerPosition(int fingerPosition) {
		this.fingerPosition = fingerPosition;
	}

	public int getMinutiaeCount() {
		return minutiaeCount;
	}

	public void setMinutiaeCount(int minutiaeCount) {
		this.minutiaeCount = minutiaeCount;
	}

	public int getQuality() {
		return quality;
	}

	public void setQuality(int quality) {
		this.quality = quality;
	}

	public int getViewNumber() {
		return viewNumber;
	}

	public void setViewNumber(int viewNumber) {
		this.viewNumber = viewNumber;
	}
	
	
	
	
}
