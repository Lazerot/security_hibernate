package services.digitalFinger;



public class DigitalFingerFmd {
	
	private String bytes;
	
	private int captureEquipmentComp;
	
	private int captureEquipmentIds;
	
	private String format;
	
	private int height;
	
	private int viewCount;
	
	private int width;
	
	private DigitalFingerFmv[] views;
	
	
	public String getBytes() {
		return bytes;
	}

	public void setBytes(String bytes) {
		this.bytes = bytes;
	}

	public int getCaptureEquipmentComp() {
		return captureEquipmentComp;
	}

	public void setCaptureEquipmentComp(int captureEquipmentComp) {
		this.captureEquipmentComp = captureEquipmentComp;
	}

	public int getCaptureEquipmentIds() {
		return captureEquipmentIds;
	}

	public void setCaptureEquipmentIds(int captureEquipmentIds) {
		this.captureEquipmentIds = captureEquipmentIds;
	}

	
	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getViewCount() {
		return viewCount;
	}

	public void setViewCount(int viewCount) {
		this.viewCount = viewCount;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public DigitalFingerFmv[] getViews() {
		return views;
	}

	public void setViews(DigitalFingerFmv[] views) {
		this.views = views;
	}


	
	

}
