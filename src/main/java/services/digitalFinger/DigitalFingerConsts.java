package services.digitalFinger;

public class DigitalFingerConsts {

	public static final int RIGHT_HAND_THUMB = 0;
	
	public static final int RIGHT_HAND_INDEX = 1;
	
	public static final int RIGHT_HAND_MIDDLE = 2;
	
	public static final int RIGHT_HAND_RING = 3;
	
	public static final int RIGHT_HAND_LITTLE = 4;
	
	public static final int LEFT_HAND_LITTLE = 9;
	
	public static final int LEFT_HAND_RING = 8;
	
	public static final int LEFT_HAND_MIDDLE = 7;
	
	public static final int LEFT_HAND_INDEX = 6;
	
	public static final int LEFT_HAND_THUMB = 5;
	
	
	
}
