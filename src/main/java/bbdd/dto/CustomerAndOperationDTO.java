/**
 * 
 */
package bbdd.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author pmartinez
 *
 */
public class CustomerAndOperationDTO implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer customerid;
	
	private Integer operationid;
	
	//Campos CustomerEntity
	private Integer id_c;
	
	private String name;
	
	private String lastname;
	
	private String nif;
	
	private String email;
	
	private String cellphone;
	
	//Campos operationEntity
	private Integer id_op;
	
	private String extra;

	private Date creationdate;
	
	//Getters and Setters
	public Integer getCustomerid() {
		return customerid;
	}

	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}

	public Integer getOperationid() {
		return operationid;
	}

	public void setOperationid(Integer operationid) {
		this.operationid = operationid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCellphone() {
		return cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public Integer getId_c() {
		return id_c;
	}

	public void setId_c(Integer id_c) {
		this.id_c = id_c;
	}

	public Integer getId_op() {
		return id_op;
	}

	public void setId_op(Integer id_op) {
		this.id_op = id_op;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}
	
	
}
