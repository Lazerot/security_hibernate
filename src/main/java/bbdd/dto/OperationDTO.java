/**
 * 
 */
package bbdd.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author pmartinez
 *
 */
public class OperationDTO implements Serializable{

	private static final long serialVersionUID = 1L;

	private Date creationdate;
	
	//Fecha hasta de creationDate
	private Date creationdatehasta;
	
	private Integer id;
	
	private String operationid;
	
	private String extra;
	
	private Integer state;
	
	private Date signaturedate;

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public Date getCreationdatehasta() {
		return creationdatehasta;
	}

	public void setCreationdatehasta(Date creationdatehasta) {
		this.creationdatehasta = creationdatehasta;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOperationid() {
		return operationid;
	}

	public void setOperationid(String operationid) {
		this.operationid = operationid;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Date getSignaturedate() {
		return signaturedate;
	}

	public void setSignaturedate(Date signaturedate) {
		this.signaturedate = signaturedate;
	}
	
	
}
