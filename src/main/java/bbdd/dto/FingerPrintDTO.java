/**
 * 
 */
package bbdd.dto;

import java.io.Serializable;

/**
 * @author pj
 *
 */
public class FingerPrintDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;

	//Table FingerPrint
	private Integer customeruuid;
	
	private String fingerImageFilePath;
	
	private Integer fingerPosition;
	
	private Byte minutiae;
	
	private String fingerImageSignedFilePath;
	
	private String minutiaeSignedFilePath;
	
	private Integer active;

	//Getters and Setters
	public Integer getCustomeruuid() {
		return customeruuid;
	}

	public void setCustomeruuid(Integer customeruuid) {
		this.customeruuid = customeruuid;
	}

	public String getFingerImageFilePath() {
		return fingerImageFilePath;
	}

	public void setFingerImageFilePath(String fingerImageFilePath) {
		this.fingerImageFilePath = fingerImageFilePath;
	}

	public Integer getFingerPosition() {
		return fingerPosition;
	}

	public void setFingerPosition(Integer fingerPosition) {
		this.fingerPosition = fingerPosition;
	}

	public Byte getMinutiae() {
		return minutiae;
	}

	public void setMinutiae(Byte minutiae) {
		this.minutiae = minutiae;
	}

	public String getFingerImageSignedFilePath() {
		return fingerImageSignedFilePath;
	}

	public void setFingerImageSignedFilePath(String fingerImageSignedFilePath) {
		this.fingerImageSignedFilePath = fingerImageSignedFilePath;
	}

	public String getMinutiaeSignedFilePath() {
		return minutiaeSignedFilePath;
	}

	public void setMinutiaeSignedFilePath(String minutiaeSignedFilePath) {
		this.minutiaeSignedFilePath = minutiaeSignedFilePath;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}
	
	

}
