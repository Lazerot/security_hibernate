/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="videos")
public class VideosEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="operationid")
	private Integer operationid;
	
	@Column(name="operationuuid")
	private String operationuuid;
	
	@Column(name="metadata")
	private String metadata;
	
	@Column(name="videodata")
	private String videodata;
	
	@Column(name="images")
	private String images;
	
	@Column(name="creationdate")
	private Date creationdate;
	
	@Column(name="signature")
	private String signature;
	
	@Column(name="archiveinfo")
	private String archiveinfo;
	
	@Column(name="sessionid")
	private String sessionid;
	
	@Column(name="token")
	private String token;
	
	@Column(name="videostate")
	private String videostate;
	
	@Column(name="archiveid")
	private String archiveid;
	
	@Column(name="chat")
	private String chat;
	
	@Column(name="domainuserid")
	private Integer domainuserid;
	
	@Column(name="signedchat")
	private String signedchat;
	
	@Column(name="username")
	private String username;
	
	public VideosEntity() {
		
	}

	public VideosEntity(Integer id, Integer operationid, String operationuuid, String metadata, String videodata,
			String images, Date creationdate, String signature, String archiveinfo, String sessionid, String token,
			String videostate, String archiveid, String chat, Integer domainuserid, String signedchat,
			String username) {
		this.id = id;
		this.operationid = operationid;
		this.operationuuid = operationuuid;
		this.metadata = metadata;
		this.videodata = videodata;
		this.images = images;
		this.creationdate = creationdate;
		this.signature = signature;
		this.archiveinfo = archiveinfo;
		this.sessionid = sessionid;
		this.token = token;
		this.videostate = videostate;
		this.archiveid = archiveid;
		this.chat = chat;
		this.domainuserid = domainuserid;
		this.signedchat = signedchat;
		this.username = username;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOperationid() {
		return operationid;
	}

	public void setOperationid(Integer operationid) {
		this.operationid = operationid;
	}

	public String getOperationuuid() {
		return operationuuid;
	}

	public void setOperationuuid(String operationuuid) {
		this.operationuuid = operationuuid;
	}

	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public String getVideodata() {
		return videodata;
	}

	public void setVideodata(String videodata) {
		this.videodata = videodata;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getArchiveinfo() {
		return archiveinfo;
	}

	public void setArchiveinfo(String archiveinfo) {
		this.archiveinfo = archiveinfo;
	}

	public String getSessionid() {
		return sessionid;
	}

	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getVideostate() {
		return videostate;
	}

	public void setVideostate(String videostate) {
		this.videostate = videostate;
	}

	public String getArchiveid() {
		return archiveid;
	}

	public void setArchiveid(String archiveid) {
		this.archiveid = archiveid;
	}

	public String getChat() {
		return chat;
	}

	public void setChat(String chat) {
		this.chat = chat;
	}

	public Integer getDomainuserid() {
		return domainuserid;
	}

	public void setDomainuserid(Integer domainuserid) {
		this.domainuserid = domainuserid;
	}

	public String getSignedchat() {
		return signedchat;
	}

	public void setSignedchat(String signedchat) {
		this.signedchat = signedchat;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
