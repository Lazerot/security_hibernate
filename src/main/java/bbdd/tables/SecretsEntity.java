/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="secrets")
public class SecretsEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="upn")
	private String upn;
	
	@Column(name="secret")
	private String secret;
	
	@Column(name="extra")
	private String extra;
	
	public SecretsEntity() {
		
	}

	public SecretsEntity(String upn, String secret, String extra) {
		this.upn = upn;
		this.secret = secret;
		this.extra = extra;
	}

	public String getUpn() {
		return upn;
	}

	public void setUpn(String upn) {
		this.upn = upn;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}
	
}
