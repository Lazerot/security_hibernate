/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="fsmopstatus")
public class FsmopstatusEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="operationid")
	private Integer operationid;
	
	@Column(name="operationuuid")
	private String operationuuid;
	
	@Column(name="domainid")
	private Integer domainid;
	
	@Column(name="fsmopmultipleid")
	private Integer fsmopmultipleid;
	
	@Column(name="statusdate")
	private Date statusdate;
	
	@Column(name="extra")
	private String extra;
	
	@Column(name="status")
	private String status;
	
	@Column(name="fsmbase64")
	private String fsmbase64;
	
	public FsmopstatusEntity () {
		
	}

	public FsmopstatusEntity(Integer id, Integer operationid, String operationuuid, Integer domainid,
			Integer fsmopmultipleid, Date statusdate, String extra, String status, String fsmbase64) {
		this.id = id;
		this.operationid = operationid;
		this.operationuuid = operationuuid;
		this.domainid = domainid;
		this.fsmopmultipleid = fsmopmultipleid;
		this.statusdate = statusdate;
		this.extra = extra;
		this.status = status;
		this.fsmbase64 = fsmbase64;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOperationid() {
		return operationid;
	}

	public void setOperationid(Integer operationid) {
		this.operationid = operationid;
	}

	public String getOperationuuid() {
		return operationuuid;
	}

	public void setOperationuuid(String operationuuid) {
		this.operationuuid = operationuuid;
	}

	public Integer getDomainid() {
		return domainid;
	}

	public void setDomainid(Integer domainid) {
		this.domainid = domainid;
	}

	public Integer getFsmopmultipleid() {
		return fsmopmultipleid;
	}

	public void setFsmopmultipleid(Integer fsmopmultipleid) {
		this.fsmopmultipleid = fsmopmultipleid;
	}

	public Date getStatusdate() {
		return statusdate;
	}

	public void setStatusdate(Date statusdate) {
		this.statusdate = statusdate;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFsmbase64() {
		return fsmbase64;
	}

	public void setFsmbase64(String fsmbase64) {
		this.fsmbase64 = fsmbase64;
	}
	
	
	
}
