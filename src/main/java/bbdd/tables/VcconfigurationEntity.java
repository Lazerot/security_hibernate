/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="vcconfiguration")
public class VcconfigurationEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="domainid")
	private Integer domainid;
	
	@Column(name="configurationdata")
	private String configurationdata;
	
	@Column(name="notifications")
	private String notifications;
	
	public VcconfigurationEntity() {
		
	}

	public VcconfigurationEntity(Integer id, Integer domainid, String configurationdata, String notifications) {
		this.id = id;
		this.domainid = domainid;
		this.configurationdata = configurationdata;
		this.notifications = notifications;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDomainid() {
		return domainid;
	}

	public void setDomainid(Integer domainid) {
		this.domainid = domainid;
	}

	public String getConfigurationdata() {
		return configurationdata;
	}

	public void setConfigurationdata(String configurationdata) {
		this.configurationdata = configurationdata;
	}

	public String getNotifications() {
		return notifications;
	}

	public void setNotifications(String notifications) {
		this.notifications = notifications;
	}
	
	
}
