/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="certificadousuario")
public class CertificadousuarioEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="idcertificado")
	private int idcertificado;
	
	@Column(name="claveprivadacifrada")
	private String claveprivadacifrada;
	
	@Column(name="claveprivadacifradaaux")
	private String claveprivadacifradaaux;
	
	@Column(name="certificado")
	private String certificado;
	
	@Column(name="certificadoaux")
	private String certificadoaux;
	
	@Column(name="fechacreacion")
	private Date fechacreacion;
	
	@Column(name="estado")
	private int estado;
	
	@Column(name="algoritmo")
	private String algoritmo;
	
	@Column(name="estadocertificado")
	private int estadocertificado;
	
	@Column(name="idcuentausuario")
	private int idcuentausuario;
	
	@Column(name="pincifrado")
	private String pincifrado;
	
	@Column(name="datosadicionales")
	private String datosadicionales;
	
	@Column(name="fechavalidez")
	private Date fechavalidez;
	
	
	@Column(name="fechacaducidad")
	private Date fechacaducidad;
	
	public CertificadousuarioEntity() {
		
	}

	public CertificadousuarioEntity(int idcertificado, String claveprivadacifrada, String claveprivadacifradaaux,
			String certificado, String certificadoaux, Date fechacreacion, int estado, String algoritmo,
			int estadocertificado, int idcuentausuario, String pincifrado, String datosadicionales, Date fechavalidez,
			Date fechacaducidad) {
		this.idcertificado = idcertificado;
		this.claveprivadacifrada = claveprivadacifrada;
		this.claveprivadacifradaaux = claveprivadacifradaaux;
		this.certificado = certificado;
		this.certificadoaux = certificadoaux;
		this.fechacreacion = fechacreacion;
		this.estado = estado;
		this.algoritmo = algoritmo;
		this.estadocertificado = estadocertificado;
		this.idcuentausuario = idcuentausuario;
		this.pincifrado = pincifrado;
		this.datosadicionales = datosadicionales;
		this.fechavalidez = fechavalidez;
		this.fechacaducidad = fechacaducidad;
	}

	public int getIdcertificado() {
		return idcertificado;
	}

	public void setIdcertificado(int idcertificado) {
		this.idcertificado = idcertificado;
	}

	public String getClaveprivadacifrada() {
		return claveprivadacifrada;
	}

	public void setClaveprivadacifrada(String claveprivadacifrada) {
		this.claveprivadacifrada = claveprivadacifrada;
	}

	public String getClaveprivadacifradaaux() {
		return claveprivadacifradaaux;
	}

	public void setClaveprivadacifradaaux(String claveprivadacifradaaux) {
		this.claveprivadacifradaaux = claveprivadacifradaaux;
	}

	public String getCertificado() {
		return certificado;
	}

	public void setCertificado(String certificado) {
		this.certificado = certificado;
	}

	public String getCertificadoaux() {
		return certificadoaux;
	}

	public void setCertificadoaux(String certificadoaux) {
		this.certificadoaux = certificadoaux;
	}

	public Date getFechacreacion() {
		return fechacreacion;
	}

	public void setFechacreacion(Date fechacreacion) {
		this.fechacreacion = fechacreacion;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public String getAlgoritmo() {
		return algoritmo;
	}

	public void setAlgoritmo(String algoritmo) {
		this.algoritmo = algoritmo;
	}

	public int getEstadocertificado() {
		return estadocertificado;
	}

	public void setEstadocertificado(int estadocertificado) {
		this.estadocertificado = estadocertificado;
	}

	public int getIdcuentausuario() {
		return idcuentausuario;
	}

	public void setIdcuentausuario(int idcuentausuario) {
		this.idcuentausuario = idcuentausuario;
	}

	public String getPincifrado() {
		return pincifrado;
	}

	public void setPincifrado(String pincifrado) {
		this.pincifrado = pincifrado;
	}

	public String getDatosadicionales() {
		return datosadicionales;
	}

	public void setDatosadicionales(String datosadicionales) {
		this.datosadicionales = datosadicionales;
	}

	public Date getFechavalidez() {
		return fechavalidez;
	}

	public void setFechavalidez(Date fechavalidez) {
		this.fechavalidez = fechavalidez;
	}

	public Date getFechacaducidad() {
		return fechacaducidad;
	}

	public void setFechacaducidad(Date fechacaducidad) {
		this.fechacaducidad = fechacaducidad;
	}
	
}
