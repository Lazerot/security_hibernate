/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="capturedimage")
public class CapturedimageEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="uuid")
	private String uuid;
	
	@Column(name="videosid")
	private int videosid;
	
	@Column(name="operationuuid")
	private String operationuuid;
	
	@Column(name="operationid")
	private int operationid;
	
	@Column(name="capturedate")
	private Date capturedate;
	
	@Column(name="data")
	private String data;
	
	@Column(name="capturedby")
	private String capturedby;
	
	@Column(name="imagename")
	private String imagename;
	
	@Column(name="imagetype")
	private String imagetype;
	
	public CapturedimageEntity() {
		
	}

	public CapturedimageEntity(int id, String uuid, int videosid, String operationuuid, int operationid,
			Date capturedate, String data, String capturedby, String imagename, String imagetype) {
		this.id = id;
		this.uuid = uuid;
		this.videosid = videosid;
		this.operationuuid = operationuuid;
		this.operationid = operationid;
		this.capturedate = capturedate;
		this.data = data;
		this.capturedby = capturedby;
		this.imagename = imagename;
		this.imagetype = imagetype;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public int getVideosid() {
		return videosid;
	}

	public void setVideosid(int videosid) {
		this.videosid = videosid;
	}

	public String getOperationuuid() {
		return operationuuid;
	}

	public void setOperationuuid(String operationuuid) {
		this.operationuuid = operationuuid;
	}

	public int getOperationid() {
		return operationid;
	}

	public void setOperationid(int operationid) {
		this.operationid = operationid;
	}

	public Date getCapturedate() {
		return capturedate;
	}

	public void setCapturedate(Date capturedate) {
		this.capturedate = capturedate;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getCapturedby() {
		return capturedby;
	}

	public void setCapturedby(String capturedby) {
		this.capturedby = capturedby;
	}

	public String getImagename() {
		return imagename;
	}

	public void setImagename(String imagename) {
		this.imagename = imagename;
	}

	public String getImagetype() {
		return imagetype;
	}

	public void setImagetype(String imagetype) {
		this.imagetype = imagetype;
	}
	
	
	
}
