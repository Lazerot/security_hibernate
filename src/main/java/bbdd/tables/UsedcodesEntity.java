/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="usedcodes")
public class UsedcodesEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="upn")
	private String upn;
	
	@Column(name="intervalo")
	private Integer intervalo;
	
	@Column(name="extra")
	private String extra;
	
	public UsedcodesEntity() {
		
	}

	public UsedcodesEntity(Integer id, String upn, Integer intervalo, String extra) {
		this.id = id;
		this.upn = upn;
		this.intervalo = intervalo;
		this.extra = extra;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUpn() {
		return upn;
	}

	public void setUpn(String upn) {
		this.upn = upn;
	}

	public Integer getIntervalo() {
		return intervalo;
	}

	public void setIntervalo(Integer intervalo) {
		this.intervalo = intervalo;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}
	
	
}
