/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="notification")
public class NotificationEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="notificationid")
	private String notificationid;
	
	@Column(name="operationid")
	private Integer operationid;
	
	@Column(name="notificationtype")
	private String notificationtype;
	
	@Column(name="content")
	private String content;
	
	@Column(name="subject")
	private String subject;
	
	@Column(name="info")
	private String info;
	
	@Column(name="creationdate")
	private Date creationdate;
	
	@Column(name="externalsignatureevent")
	private Integer externalsignatureevent;
	
	@Column(name="notificationdate")
	private Date notificationdate;
	
	@Column(name="firstreadip")
	private String firstreadip;
	
	@Column(name="lastreadip")
	private String lastreadip;
	
	@Column(name="firstreaddate")
	private Date firstreaddate;
	
	@Column(name="signedcontent")
	private String signedcontent;
	
	@Column(name="state")
	private Integer state;
	
	@Column(name="lastreaddate")
	private Date lastreaddate;
	
	@Column(name="templateid")
	private Integer templateid;
	
	@Column(name="delivereddate")
	private Date delivereddate;
	
	@Column(name="deliveredtime")
	private Integer deliveredtime;
	
	@Column(name="responsetime")
	private Integer responsetime;
	
	@Column(name="errorsms")
	private String errorsms;
	
	public NotificationEntity() {
		
	}

	public NotificationEntity(Integer id, String notificationid, Integer operationid, String notificationtype,
			String content, String subject, String info, Date creationdate, Integer externalsignatureevent,
			Date notificationdate, String firstreadip, String lastreadip, Date firstreaddate, String signedcontent,
			Integer state, Date lastreaddate, Integer templateid, Date delivereddate, Integer deliveredtime,
			Integer responsetime, String errorsms) {
		this.id = id;
		this.notificationid = notificationid;
		this.operationid = operationid;
		this.notificationtype = notificationtype;
		this.content = content;
		this.subject = subject;
		this.info = info;
		this.creationdate = creationdate;
		this.externalsignatureevent = externalsignatureevent;
		this.notificationdate = notificationdate;
		this.firstreadip = firstreadip;
		this.lastreadip = lastreadip;
		this.firstreaddate = firstreaddate;
		this.signedcontent = signedcontent;
		this.state = state;
		this.lastreaddate = lastreaddate;
		this.templateid = templateid;
		this.delivereddate = delivereddate;
		this.deliveredtime = deliveredtime;
		this.responsetime = responsetime;
		this.errorsms = errorsms;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNotificationid() {
		return notificationid;
	}

	public void setNotificationid(String notificationid) {
		this.notificationid = notificationid;
	}

	public Integer getOperationid() {
		return operationid;
	}

	public void setOperationid(Integer operationid) {
		this.operationid = operationid;
	}

	public String getNotificationtype() {
		return notificationtype;
	}

	public void setNotificationtype(String notificationtype) {
		this.notificationtype = notificationtype;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public Integer getExternalsignatureevent() {
		return externalsignatureevent;
	}

	public void setExternalsignatureevent(Integer externalsignatureevent) {
		this.externalsignatureevent = externalsignatureevent;
	}

	public Date getNotificationdate() {
		return notificationdate;
	}

	public void setNotificationdate(Date notificationdate) {
		this.notificationdate = notificationdate;
	}

	public String getFirstreadip() {
		return firstreadip;
	}

	public void setFirstreadip(String firstreadip) {
		this.firstreadip = firstreadip;
	}

	public String getLastreadip() {
		return lastreadip;
	}

	public void setLastreadip(String lastreadip) {
		this.lastreadip = lastreadip;
	}

	public Date getFirstreaddate() {
		return firstreaddate;
	}

	public void setFirstreaddate(Date firstreaddate) {
		this.firstreaddate = firstreaddate;
	}

	public String getSignedcontent() {
		return signedcontent;
	}

	public void setSignedcontent(String signedcontent) {
		this.signedcontent = signedcontent;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Date getLastreaddate() {
		return lastreaddate;
	}

	public void setLastreaddate(Date lastreaddate) {
		this.lastreaddate = lastreaddate;
	}

	public Integer getTemplateid() {
		return templateid;
	}

	public void setTemplateid(Integer templateid) {
		this.templateid = templateid;
	}

	public Date getDelivereddate() {
		return delivereddate;
	}

	public void setDelivereddate(Date delivereddate) {
		this.delivereddate = delivereddate;
	}

	public Integer getDeliveredtime() {
		return deliveredtime;
	}

	public void setDeliveredtime(Integer deliveredtime) {
		this.deliveredtime = deliveredtime;
	}

	public Integer getResponsetime() {
		return responsetime;
	}

	public void setResponsetime(Integer responsetime) {
		this.responsetime = responsetime;
	}

	public String getErrorsms() {
		return errorsms;
	}

	public void setErrorsms(String errorsms) {
		this.errorsms = errorsms;
	}
	
	
}
