/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="viewpermissions")
public class ViewpermissionsEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="domainid")
	private Integer domainid;
	
	@Column(name="typeid")
	private Integer typeid;
	
	@Column(name="typename")
	private String typename;
	
	@Column(name="visibleelements")
	private String visibleelements;
	
	@Column(name="roleid")
	private Integer roleid;
	
	@Column(name="userid")
	private Integer userid;
	
	public ViewpermissionsEntity() {
		
	}

	public ViewpermissionsEntity(Integer id, String name, Integer domainid, Integer typeid, String typename,
			String visibleelements, Integer roleid, Integer userid) {
		this.id = id;
		this.name = name;
		this.domainid = domainid;
		this.typeid = typeid;
		this.typename = typename;
		this.visibleelements = visibleelements;
		this.roleid = roleid;
		this.userid = userid;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getDomainid() {
		return domainid;
	}

	public void setDomainid(Integer domainid) {
		this.domainid = domainid;
	}

	public Integer getTypeid() {
		return typeid;
	}

	public void setTypeid(Integer typeid) {
		this.typeid = typeid;
	}

	public String getTypename() {
		return typename;
	}

	public void setTypename(String typename) {
		this.typename = typename;
	}

	public String getVisibleelements() {
		return visibleelements;
	}

	public void setVisibleelements(String visibleelements) {
		this.visibleelements = visibleelements;
	}

	public Integer getRoleid() {
		return roleid;
	}

	public void setRoleid(Integer roleid) {
		this.roleid = roleid;
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	
}
