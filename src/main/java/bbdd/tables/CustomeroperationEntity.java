/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="customeroperation")
public class CustomeroperationEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	@GeneratedValue
	@Id
	@Column(name="id")
	private Integer id;
	
	@GeneratedValue
	@Column(name="customerid")
	private Integer customerid;
	
	@GeneratedValue
	@Column(name="operationid")
	private Integer operationid;
	
	@Column(name="extra")
	private String extra;
	
	public CustomeroperationEntity() {
		
	}

	public CustomeroperationEntity(Integer id, Integer customerid, Integer operationid, String extra) {
		this.id = id;
		this.customerid = customerid;
		this.operationid = operationid;
		this.extra = extra;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCustomerid() {
		return customerid;
	}

	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}

	public Integer getOperationid() {
		return operationid;
	}

	public void setOperationid(Integer operationid) {
		this.operationid = operationid;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}
	
}
