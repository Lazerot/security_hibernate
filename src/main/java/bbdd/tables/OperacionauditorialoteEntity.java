/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="operacionauditorialote")
public class OperacionauditorialoteEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="iddominio")
	private String iddominio;
	
	@Column(name="fechaprimeraop")
	private Date fechaprimeraop;
	
	@Column(name="fechaultimaop")
	private Date fechaultimaop;
	
	@Column(name="numerolotes")
	private Integer numerolotes;
	
	@Column(name="estado")
	private Integer estado;
	
	public OperacionauditorialoteEntity() {
		
	}

	public OperacionauditorialoteEntity(Integer id, String iddominio, Date fechaprimeraop, Date fechaultimaop,
			Integer numerolotes, Integer estado) {
		this.id = id;
		this.iddominio = iddominio;
		this.fechaprimeraop = fechaprimeraop;
		this.fechaultimaop = fechaultimaop;
		this.numerolotes = numerolotes;
		this.estado = estado;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIddominio() {
		return iddominio;
	}

	public void setIddominio(String iddominio) {
		this.iddominio = iddominio;
	}

	public Date getFechaprimeraop() {
		return fechaprimeraop;
	}

	public void setFechaprimeraop(Date fechaprimeraop) {
		this.fechaprimeraop = fechaprimeraop;
	}

	public Date getFechaultimaop() {
		return fechaultimaop;
	}

	public void setFechaultimaop(Date fechaultimaop) {
		this.fechaultimaop = fechaultimaop;
	}

	public Integer getNumerolotes() {
		return numerolotes;
	}

	public void setNumerolotes(Integer numerolotes) {
		this.numerolotes = numerolotes;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}
	

}
