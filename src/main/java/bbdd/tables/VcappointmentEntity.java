/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="vcappointment")
public class VcappointmentEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="uuid")
	private String uuid;
	
	@Column(name="creationdate")
	private Date creationdate;
	
	@Column(name="requestedstartdate")
	private Date requestedstartdate;
	
	@Column(name="status")
	private String status;
	
	@Column(name="agentdatein")
	private Date agentdatein;
	
	@Column(name="agentdateout")
	private Date agentdateout;
	
	@Column(name="customerdatein")
	private Date customerdatein;
	
	@Column(name="customerdateout")
	private Date customerdateout;
	
	@Column(name="customeremail")
	private String customeremail;
	
	@Column(name="customerdata")
	private String customerdata;
	
	@Column(name="sessionid")
	private String sessionid;
	
	@Column(name="videoinfo")
	private String videoinfo;
	
	@Column(name="vccustomerid")
	private Integer vccustomerid;
	
	@Column(name="domainid")
	private Integer domainid;
	
	@Column(name="domainuserid")
	private Integer domainuserid;
	
	@Column(name="replacedby")
	private Integer replacedby;
	
	@Column(name="username")
	private String username;
	
	@Column(name="customername")
	private String customername;
	
	public VcappointmentEntity() {
		
	}

	public VcappointmentEntity(Integer id, String uuid, Date creationdate, Date requestedstartdate, String status,
			Date agentdatein, Date agentdateout, Date customerdatein, Date customerdateout, String customeremail,
			String customerdata, String sessionid, String videoinfo, Integer vccustomerid, Integer domainid,
			Integer domainuserid, Integer replacedby, String username, String customername) {
		this.id = id;
		this.uuid = uuid;
		this.creationdate = creationdate;
		this.requestedstartdate = requestedstartdate;
		this.status = status;
		this.agentdatein = agentdatein;
		this.agentdateout = agentdateout;
		this.customerdatein = customerdatein;
		this.customerdateout = customerdateout;
		this.customeremail = customeremail;
		this.customerdata = customerdata;
		this.sessionid = sessionid;
		this.videoinfo = videoinfo;
		this.vccustomerid = vccustomerid;
		this.domainid = domainid;
		this.domainuserid = domainuserid;
		this.replacedby = replacedby;
		this.username = username;
		this.customername = customername;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public Date getRequestedstartdate() {
		return requestedstartdate;
	}

	public void setRequestedstartdate(Date requestedstartdate) {
		this.requestedstartdate = requestedstartdate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getAgentdatein() {
		return agentdatein;
	}

	public void setAgentdatein(Date agentdatein) {
		this.agentdatein = agentdatein;
	}

	public Date getAgentdateout() {
		return agentdateout;
	}

	public void setAgentdateout(Date agentdateout) {
		this.agentdateout = agentdateout;
	}

	public Date getCustomerdatein() {
		return customerdatein;
	}

	public void setCustomerdatein(Date customerdatein) {
		this.customerdatein = customerdatein;
	}

	public Date getCustomerdateout() {
		return customerdateout;
	}

	public void setCustomerdateout(Date customerdateout) {
		this.customerdateout = customerdateout;
	}

	public String getCustomeremail() {
		return customeremail;
	}

	public void setCustomeremail(String customeremail) {
		this.customeremail = customeremail;
	}

	public String getCustomerdata() {
		return customerdata;
	}

	public void setCustomerdata(String customerdata) {
		this.customerdata = customerdata;
	}

	public String getSessionid() {
		return sessionid;
	}

	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}

	public String getVideoinfo() {
		return videoinfo;
	}

	public void setVideoinfo(String videoinfo) {
		this.videoinfo = videoinfo;
	}

	public Integer getVccustomerid() {
		return vccustomerid;
	}

	public void setVccustomerid(Integer vccustomerid) {
		this.vccustomerid = vccustomerid;
	}

	public Integer getDomainid() {
		return domainid;
	}

	public void setDomainid(Integer domainid) {
		this.domainid = domainid;
	}

	public Integer getDomainuserid() {
		return domainuserid;
	}

	public void setDomainuserid(Integer domainuserid) {
		this.domainuserid = domainuserid;
	}

	public Integer getReplacedby() {
		return replacedby;
	}

	public void setReplacedby(Integer replacedby) {
		this.replacedby = replacedby;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCustomername() {
		return customername;
	}

	public void setCustomername(String customername) {
		this.customername = customername;
	}
	
		
}
