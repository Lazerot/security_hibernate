/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="vcconnection")
public class VcconnectionEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="vcvideoconferenceid")
	private Integer vcvideoconferenceid;
	
	@Column(name="sessionid")
	private String sessionid;
	
	@Column(name="projectid")
	private String projectid;
	
	@Column(name="eventtype")
	private String eventtype;
	
	@Column(name="eventdate")
	private Date eventdate;
	
	@Column(name="extra")
	private String extra;
	
	public VcconnectionEntity() {
		
	}

	public VcconnectionEntity(Integer id, Integer vcvideoconferenceid, String sessionid, String projectid,
			String eventtype, Date eventdate, String extra) {
		this.id = id;
		this.vcvideoconferenceid = vcvideoconferenceid;
		this.sessionid = sessionid;
		this.projectid = projectid;
		this.eventtype = eventtype;
		this.eventdate = eventdate;
		this.extra = extra;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getVcvideoconferenceid() {
		return vcvideoconferenceid;
	}

	public void setVcvideoconferenceid(Integer vcvideoconferenceid) {
		this.vcvideoconferenceid = vcvideoconferenceid;
	}

	public String getSessionid() {
		return sessionid;
	}

	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}

	public String getProjectid() {
		return projectid;
	}

	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}

	public String getEventtype() {
		return eventtype;
	}

	public void setEventtype(String eventtype) {
		this.eventtype = eventtype;
	}

	public Date getEventdate() {
		return eventdate;
	}

	public void setEventdate(Date eventdate) {
		this.eventdate = eventdate;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}
	
}
