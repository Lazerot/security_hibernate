/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */

@Entity
@Table(name="additionaldomains")
public class AdditionaldomainsEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="domainid")
	private int domainid;
	
	@Column(name="roleid")
	private int roleid;
	
	@Column(name="userid")
	private int userid;
	
	@Column(name="domains")
	private String domains;
	
	@Column(name="permission")
	private String permission;
	
	public AdditionaldomainsEntity() {
		
	}

	public AdditionaldomainsEntity(int id, int domainid, int roleid, int userid, String domains, String permission) {
		
		this.id = id;
		this.domainid = domainid;
		this.roleid = roleid;
		this.userid = userid;
		this.domains = domains;
		this.permission = permission;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDomainid() {
		return domainid;
	}

	public void setDomainid(int domainid) {
		this.domainid = domainid;
	}

	public int getRoleid() {
		return roleid;
	}

	public void setRoleid(int roleid) {
		this.roleid = roleid;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getDomains() {
		return domains;
	}

	public void setDomains(String domains) {
		this.domains = domains;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}
	
	
	
}
