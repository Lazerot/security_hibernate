/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="domain")
public class DomainEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="domainname")
	private String domainname;
	
	@Column(name="basepath")
	private String basepath;
	
	@Column(name="configurationid")
	private Integer configurationid;
	
	@Column(name="encryptionlevel")
	private Integer encryptionlevel;
	
	@Column(name="sizelimit")
	private Integer sizelimit;
	
	@Column(name="totalsizelimit")
	private Integer totalsizelimit;
	
	@Column(name="storagetype")
	private String storagetype;
	
	@Column(name="stusername")
	private String stusername;
	
	@Column(name="stpassword")
	private String stpassword;
	
	@Column(name="certificate")
	private String certificate;
	
	@Column(name="certificatepk")
	private String certificatepk;
	
	@Column(name="tsaurl")
	private String tsaurl;
	
	@Column(name="tsausername")
	private String tsausername;
	
	@Column(name="tsapassword")
	private String tsapassword;
	
	@Column(name="logos")
	private String logos;
	
	@Column(name="cssfile")
	private String cssfile;
	
	@Column(name="emailservertype")
	private String emailservertype;
	
	@Column(name="emailserver")
	private String emailserver;
	
	@Column(name="emailusername")
	private String emailusername;
	
	@Column(name="emailpassword")
	private String emailpassword;
	
	@Column(name="emailkey")
	private String emailKey;
	
	@Column(name="emailsender")
	private String emailsender;
	
	@Column(name="emailreplyto")
	private String emailreplyto;
	
	@Column(name="esignurl")
	private String esignurl;
	
	@Column(name="serverdomain")
	private String serverdomain;
	
	@Column(name="extrafields")
	private String extrafields;
	
	@Column(name="smsservertype")
	private String smsservertype;
	
	@Column(name="smsserver")
	private String smsserver;
	
	@Column(name="smsusername")
	private String smsusername;
	
	@Column(name="smspassword")
	private String smspassword;
	
	@Column(name="smskey")
	private String smskey;
	
	@Column(name="smssender")
	private String smssender;
	
	@Column(name="smsreplyto")
	private String smsreplyto;
	
	@Column(name="advancedoptions")
	private String advancedoptions;
	
	@Column(name="certificateroot")
	private String certificateroot;
	
	@Column(name="modulesinfo")
	private String modulesinfo;
	
	@Column(name="sthost")
	private String sthost;
	
	@Column(name="statelabel")
	private String statelabel;
	
	@Column(name="biosignurl")
	private String biosignurl;
	
	@Column(name="bsignurl")
	private String bsignurl;
	
	public DomainEntity() {
		
	}

	public DomainEntity(Integer id, String domainname, String basepath, Integer configurationid,
			Integer encryptionlevel, Integer sizelimit, Integer totalsizelimit, String storagetype, String stusername,
			String stpassword, String certificate, String certificatepk, String tsaurl, String tsausername,
			String tsapassword, String logos, String cssfile, String emailservertype, String emailserver,
			String emailusername, String emailpassword, String emailKey, String emailsender, String emailreplyto,
			String esignurl, String serverdomain, String extrafields, String smsservertype, String smsserver,
			String smsusername, String smspassword, String smskey, String smssender, String smsreplyto,
			String advancedoptions, String certificateroot, String modulesinfo, String sthost, String statelabel,
			String biosignurl, String bsignurl) {
		this.id = id;
		this.domainname = domainname;
		this.basepath = basepath;
		this.configurationid = configurationid;
		this.encryptionlevel = encryptionlevel;
		this.sizelimit = sizelimit;
		this.totalsizelimit = totalsizelimit;
		this.storagetype = storagetype;
		this.stusername = stusername;
		this.stpassword = stpassword;
		this.certificate = certificate;
		this.certificatepk = certificatepk;
		this.tsaurl = tsaurl;
		this.tsausername = tsausername;
		this.tsapassword = tsapassword;
		this.logos = logos;
		this.cssfile = cssfile;
		this.emailservertype = emailservertype;
		this.emailserver = emailserver;
		this.emailusername = emailusername;
		this.emailpassword = emailpassword;
		this.emailKey = emailKey;
		this.emailsender = emailsender;
		this.emailreplyto = emailreplyto;
		this.esignurl = esignurl;
		this.serverdomain = serverdomain;
		this.extrafields = extrafields;
		this.smsservertype = smsservertype;
		this.smsserver = smsserver;
		this.smsusername = smsusername;
		this.smspassword = smspassword;
		this.smskey = smskey;
		this.smssender = smssender;
		this.smsreplyto = smsreplyto;
		this.advancedoptions = advancedoptions;
		this.certificateroot = certificateroot;
		this.modulesinfo = modulesinfo;
		this.sthost = sthost;
		this.statelabel = statelabel;
		this.biosignurl = biosignurl;
		this.bsignurl = bsignurl;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDomainname() {
		return domainname;
	}

	public void setDomainname(String domainname) {
		this.domainname = domainname;
	}

	public String getBasepath() {
		return basepath;
	}

	public void setBasepath(String basepath) {
		this.basepath = basepath;
	}

	public Integer getConfigurationid() {
		return configurationid;
	}

	public void setConfigurationid(Integer configurationid) {
		this.configurationid = configurationid;
	}

	public Integer getEncryptionlevel() {
		return encryptionlevel;
	}

	public void setEncryptionlevel(Integer encryptionlevel) {
		this.encryptionlevel = encryptionlevel;
	}

	public Integer getSizelimit() {
		return sizelimit;
	}

	public void setSizelimit(Integer sizelimit) {
		this.sizelimit = sizelimit;
	}

	public Integer getTotalsizelimit() {
		return totalsizelimit;
	}

	public void setTotalsizelimit(Integer totalsizelimit) {
		this.totalsizelimit = totalsizelimit;
	}

	public String getStoragetype() {
		return storagetype;
	}

	public void setStoragetype(String storagetype) {
		this.storagetype = storagetype;
	}

	public String getStusername() {
		return stusername;
	}

	public void setStusername(String stusername) {
		this.stusername = stusername;
	}

	public String getStpassword() {
		return stpassword;
	}

	public void setStpassword(String stpassword) {
		this.stpassword = stpassword;
	}

	public String getCertificate() {
		return certificate;
	}

	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}

	public String getCertificatepk() {
		return certificatepk;
	}

	public void setCertificatepk(String certificatepk) {
		this.certificatepk = certificatepk;
	}

	public String getTsaurl() {
		return tsaurl;
	}

	public void setTsaurl(String tsaurl) {
		this.tsaurl = tsaurl;
	}

	public String getTsausername() {
		return tsausername;
	}

	public void setTsausername(String tsausername) {
		this.tsausername = tsausername;
	}

	public String getTsapassword() {
		return tsapassword;
	}

	public void setTsapassword(String tsapassword) {
		this.tsapassword = tsapassword;
	}

	public String getLogos() {
		return logos;
	}

	public void setLogos(String logos) {
		this.logos = logos;
	}

	public String getCssfile() {
		return cssfile;
	}

	public void setCssfile(String cssfile) {
		this.cssfile = cssfile;
	}

	public String getEmailservertype() {
		return emailservertype;
	}

	public void setEmailservertype(String emailservertype) {
		this.emailservertype = emailservertype;
	}

	public String getEmailserver() {
		return emailserver;
	}

	public void setEmailserver(String emailserver) {
		this.emailserver = emailserver;
	}

	public String getEmailusername() {
		return emailusername;
	}

	public void setEmailusername(String emailusername) {
		this.emailusername = emailusername;
	}

	public String getEmailpassword() {
		return emailpassword;
	}

	public void setEmailpassword(String emailpassword) {
		this.emailpassword = emailpassword;
	}

	public String getEmailKey() {
		return emailKey;
	}

	public void setEmailKey(String emailKey) {
		this.emailKey = emailKey;
	}

	public String getEmailsender() {
		return emailsender;
	}

	public void setEmailsender(String emailsender) {
		this.emailsender = emailsender;
	}

	public String getEmailreplyto() {
		return emailreplyto;
	}

	public void setEmailreplyto(String emailreplyto) {
		this.emailreplyto = emailreplyto;
	}

	public String getEsignurl() {
		return esignurl;
	}

	public void setEsignurl(String esignurl) {
		this.esignurl = esignurl;
	}

	public String getServerdomain() {
		return serverdomain;
	}

	public void setServerdomain(String serverdomain) {
		this.serverdomain = serverdomain;
	}

	public String getExtrafields() {
		return extrafields;
	}

	public void setExtrafields(String extrafields) {
		this.extrafields = extrafields;
	}

	public String getSmsservertype() {
		return smsservertype;
	}

	public void setSmsservertype(String smsservertype) {
		this.smsservertype = smsservertype;
	}

	public String getSmsserver() {
		return smsserver;
	}

	public void setSmsserver(String smsserver) {
		this.smsserver = smsserver;
	}

	public String getSmsusername() {
		return smsusername;
	}

	public void setSmsusername(String smsusername) {
		this.smsusername = smsusername;
	}

	public String getSmspassword() {
		return smspassword;
	}

	public void setSmspassword(String smspassword) {
		this.smspassword = smspassword;
	}

	public String getSmskey() {
		return smskey;
	}

	public void setSmskey(String smskey) {
		this.smskey = smskey;
	}

	public String getSmssender() {
		return smssender;
	}

	public void setSmssender(String smssender) {
		this.smssender = smssender;
	}

	public String getSmsreplyto() {
		return smsreplyto;
	}

	public void setSmsreplyto(String smsreplyto) {
		this.smsreplyto = smsreplyto;
	}

	public String getAdvancedoptions() {
		return advancedoptions;
	}

	public void setAdvancedoptions(String advancedoptions) {
		this.advancedoptions = advancedoptions;
	}

	public String getCertificateroot() {
		return certificateroot;
	}

	public void setCertificateroot(String certificateroot) {
		this.certificateroot = certificateroot;
	}

	public String getModulesinfo() {
		return modulesinfo;
	}

	public void setModulesinfo(String modulesinfo) {
		this.modulesinfo = modulesinfo;
	}

	public String getSthost() {
		return sthost;
	}

	public void setSthost(String sthost) {
		this.sthost = sthost;
	}

	public String getStatelabel() {
		return statelabel;
	}

	public void setStatelabel(String statelabel) {
		this.statelabel = statelabel;
	}

	public String getBiosignurl() {
		return biosignurl;
	}

	public void setBiosignurl(String biosignurl) {
		this.biosignurl = biosignurl;
	}

	public String getBsignurl() {
		return bsignurl;
	}

	public void setBsignurl(String bsignurl) {
		this.bsignurl = bsignurl;
	}	
	
}
