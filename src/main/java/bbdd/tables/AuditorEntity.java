/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="auditor")
public class AuditorEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="eventdate")
	private Date eventdate;
	
	@Column(name="eventtype")
	private String eventtype;
	
	@Column(name="domain")
	private int domain;
	
	@Column(name="domainuser")
	private int domainuser;
	
	@Column(name="customer")
	private int customer;
	
	@Column(name="extra")
	private String extra;
	
	public AuditorEntity() {
		
	}

	public AuditorEntity(int id, Date eventdate, String eventtype, int domain, int domainuser, int customer,
			String extra) {
		this.id = id;
		this.eventdate = eventdate;
		this.eventtype = eventtype;
		this.domain = domain;
		this.domainuser = domainuser;
		this.customer = customer;
		this.extra = extra;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getEventdate() {
		return eventdate;
	}

	public void setEventdate(Date eventdate) {
		this.eventdate = eventdate;
	}

	public String getEventtype() {
		return eventtype;
	}

	public void setEventtype(String eventtype) {
		this.eventtype = eventtype;
	}

	public int getDomain() {
		return domain;
	}

	public void setDomain(int domain) {
		this.domain = domain;
	}

	public int getDomainuser() {
		return domainuser;
	}

	public void setDomainuser(int domainuser) {
		this.domainuser = domainuser;
	}

	public int getCustomer() {
		return customer;
	}

	public void setCustomer(int customer) {
		this.customer = customer;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}
	
}

