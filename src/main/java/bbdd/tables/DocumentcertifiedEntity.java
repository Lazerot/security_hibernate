/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="documentcertified")
public class DocumentcertifiedEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="creationdate")
	private Date creationdate;
	
	@Column(name="documentpath")
	private String documentpath;
	
	@Column(name="documentsize")
	private Integer documentsize;
	
	@Column(name="documenttype")
	private String documenttype;
	
	@Column(name="invoicedate")
	private Date invoicedate;
	
	@Column(name="operationdate")
	private Date operationdate;
	
	@Column(name="receptionnumber")
	private String receptionnumber;
	
	@Column(name="corporatename")
	private String corporatename;
	
	@Column(name="cif")
	private String cif;
	
	@Column(name="amount")
	private Double amount;
	
	@Column(name="extra")
	private String extra;
	
	@Column(name="uuid")
	private String uuid;
	
	@Column(name="domainuserid")
	private Integer domainuserid;
	
	@Column(name="category")
	private String category;
	
	@Column(name="favorite")
	private Integer favorite;
	
	@Column(name="fullname")
	private String fullname;
	
	@Column(name="hash")
	private String hash;
	
	@Column(name="customerid")
	private Integer customerid;
	
	public DocumentcertifiedEntity () {
		
	}

	public DocumentcertifiedEntity(Integer id, String name, Date creationdate, String documentpath,
			Integer documentsize, String documenttype, Date invoicedate, Date operationdate, String receptionnumber,
			String corporatename, String cif, Double amount, String extra, String uuid, Integer domainuserid,
			String category, Integer favorite, String fullname, String hash, Integer customerid) {
		this.id = id;
		this.name = name;
		this.creationdate = creationdate;
		this.documentpath = documentpath;
		this.documentsize = documentsize;
		this.documenttype = documenttype;
		this.invoicedate = invoicedate;
		this.operationdate = operationdate;
		this.receptionnumber = receptionnumber;
		this.corporatename = corporatename;
		this.cif = cif;
		this.amount = amount;
		this.extra = extra;
		this.uuid = uuid;
		this.domainuserid = domainuserid;
		this.category = category;
		this.favorite = favorite;
		this.fullname = fullname;
		this.hash = hash;
		this.customerid = customerid;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public String getDocumentpath() {
		return documentpath;
	}

	public void setDocumentpath(String documentpath) {
		this.documentpath = documentpath;
	}

	public Integer getDocumentsize() {
		return documentsize;
	}

	public void setDocumentsize(Integer documentsize) {
		this.documentsize = documentsize;
	}

	public String getDocumenttype() {
		return documenttype;
	}

	public void setDocumenttype(String documenttype) {
		this.documenttype = documenttype;
	}

	public Date getInvoicedate() {
		return invoicedate;
	}

	public void setInvoicedate(Date invoicedate) {
		this.invoicedate = invoicedate;
	}

	public Date getOperationdate() {
		return operationdate;
	}

	public void setOperationdate(Date operationdate) {
		this.operationdate = operationdate;
	}

	public String getReceptionnumber() {
		return receptionnumber;
	}

	public void setReceptionnumber(String receptionnumber) {
		this.receptionnumber = receptionnumber;
	}

	public String getCorporatename() {
		return corporatename;
	}

	public void setCorporatename(String corporatename) {
		this.corporatename = corporatename;
	}

	public String getCif() {
		return cif;
	}

	public void setCif(String cif) {
		this.cif = cif;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Integer getDomainuserid() {
		return domainuserid;
	}

	public void setDomainuserid(Integer domainuserid) {
		this.domainuserid = domainuserid;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Integer getFavorite() {
		return favorite;
	}

	public void setFavorite(Integer favorite) {
		this.favorite = favorite;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public Integer getCustomerid() {
		return customerid;
	}

	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}
	
	
}
