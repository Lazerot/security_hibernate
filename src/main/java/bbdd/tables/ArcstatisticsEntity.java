/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="arcstatistics")
public class ArcstatisticsEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="statsname")
	private String statesname;
	
	@Column(name="domainid")
	private int domainid;
	
	@Column(name="statstype")
	private int statstype;
	
	@Column(name="timeelement")
	private int timeelement;
	
	@Column(name="fromdate")
	private Date fromdate;
	
	@Column(name="todate")
	private Date todate;
	
	@Column(name="statsinfo")
	private String statsinfo;
	
	public ArcstatisticsEntity() {
		
	}

	public ArcstatisticsEntity(int id, String statesname, int domainid, int statstype, int timeelement, Date fromdate,
			Date todate, String statsinfo) {
		this.id = id;
		this.statesname = statesname;
		this.domainid = domainid;
		this.statstype = statstype;
		this.timeelement = timeelement;
		this.fromdate = fromdate;
		this.todate = todate;
		this.statsinfo = statsinfo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatesname() {
		return statesname;
	}

	public void setStatesname(String statesname) {
		this.statesname = statesname;
	}

	public int getDomainid() {
		return domainid;
	}

	public void setDomainid(int domainid) {
		this.domainid = domainid;
	}

	public int getStatstype() {
		return statstype;
	}

	public void setStatstype(int statstype) {
		this.statstype = statstype;
	}

	public int getTimeelement() {
		return timeelement;
	}

	public void setTimeelement(int timeelement) {
		this.timeelement = timeelement;
	}

	public Date getFromdate() {
		return fromdate;
	}

	public void setFromdate(Date fromdate) {
		this.fromdate = fromdate;
	}

	public Date getTodate() {
		return todate;
	}

	public void setTodate(Date todate) {
		this.todate = todate;
	}

	public String getStatsinfo() {
		return statsinfo;
	}

	public void setStatsinfo(String statsinfo) {
		this.statsinfo = statsinfo;
	}
	
	

}
