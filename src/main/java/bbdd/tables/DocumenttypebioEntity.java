/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="documenttypebio")
public class DocumenttypebioEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="idbiosignconfig")
	private Integer idbiosignconfig;
	
	@Column(name="name")
	private String name;
	
	@Column(name="signtype")
	private String signtype;
	
	@Column(name="reference")
	private String reference;
	
	@Column(name="signernumber")
	private Integer signernumber;
	
	@Column(name="metadata")
	private String metadata;
	
	@Column(name="pathname")
	private String pathname;
	
	public DocumenttypebioEntity() {
		
	}

	public DocumenttypebioEntity(Integer id, Integer idbiosignconfig, String name, String signtype, String reference,
			Integer signernumber, String metadata, String pathname) {
		this.id = id;
		this.idbiosignconfig = idbiosignconfig;
		this.name = name;
		this.signtype = signtype;
		this.reference = reference;
		this.signernumber = signernumber;
		this.metadata = metadata;
		this.pathname = pathname;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdbiosignconfig() {
		return idbiosignconfig;
	}

	public void setIdbiosignconfig(Integer idbiosignconfig) {
		this.idbiosignconfig = idbiosignconfig;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSigntype() {
		return signtype;
	}

	public void setSigntype(String signtype) {
		this.signtype = signtype;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Integer getSignernumber() {
		return signernumber;
	}

	public void setSignernumber(Integer signernumber) {
		this.signernumber = signernumber;
	}

	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public String getPathname() {
		return pathname;
	}

	public void setPathname(String pathname) {
		this.pathname = pathname;
	}
	

}
