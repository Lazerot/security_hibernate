/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="documentevent")
public class DocumenteventEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="documentid")
	private Integer documentid;
	
	@Column(name="info")
	private String info;
	
	@Column(name="xml")
	private String xml;
	
	@Column(name="eventtype")
	private String eventtype;
	
	@Column(name="eventsequence")
	private Integer eventsequence;
	
	@Column(name="signed")
	private Integer signed;
	
	public DocumenteventEntity() {
		
	}

	public DocumenteventEntity(Integer id, Integer documentid, String info, String xml, String eventtype,
			Integer eventsequence, Integer signed) {
		this.id = id;
		this.documentid = documentid;
		this.info = info;
		this.xml = xml;
		this.eventtype = eventtype;
		this.eventsequence = eventsequence;
		this.signed = signed;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDocumentid() {
		return documentid;
	}

	public void setDocumentid(Integer documentid) {
		this.documentid = documentid;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public String getEventtype() {
		return eventtype;
	}

	public void setEventtype(String eventtype) {
		this.eventtype = eventtype;
	}

	public Integer getEventsequence() {
		return eventsequence;
	}

	public void setEventsequence(Integer eventsequence) {
		this.eventsequence = eventsequence;
	}

	public Integer getSigned() {
		return signed;
	}

	public void setSigned(Integer signed) {
		this.signed = signed;
	}
	

}
