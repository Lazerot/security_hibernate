/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name ="actatemplate")
public class ActatemplateEntity implements Serializable {

	private static final long serialVersionUID = 1L;
		
	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="uuid")
	private String uuid;
	
	@Column(name="name")
	private String name;
	
	@Column(name="domainid")
	private int domainid;
	
	@Column(name="path")
	private String path;
	
	@Column(name="extra")
	private String extra;
	
	@Column(name="type")
	private String type;
	
	public ActatemplateEntity() {
	}
	
	public ActatemplateEntity(int id, String uuid, String name, int domainid, String path, String extra, String type) {
	
		this.id = id;
		this.uuid = uuid;
		this.name = name;
		this.domainid = domainid;
		this.path = path;
		this.extra = extra;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDomainid() {
		return domainid;
	}

	public void setDomainid(int domainid) {
		this.domainid = domainid;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
	
	

