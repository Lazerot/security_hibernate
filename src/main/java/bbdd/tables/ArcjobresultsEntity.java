/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="arcjobresults")
public class ArcjobresultsEntity implements Serializable {
 
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="uuid")
	private String uuid;
	
	@Column(name="jobid")
	private String jobid;
	
	@Column(name="croninfo")
	private String croninfo;
	
	@Column(name="starttime")
	private Date sarttime;
	
	@Column(name="currenttime")
	private Date currenttime;
	
	@Column(name="ispartialresult")
	private int ispartialresult;
	
	@Column(name="state")
	private int state;
	
	@Column(name="results")
	private String results;
	
	@Column(name="statistics")
	private String statistics;
	
	public ArcjobresultsEntity () {
		
	}

	public ArcjobresultsEntity(int id, String uuid, String jobid, String croninfo, Date sarttime, Date currenttime,
			int ispartialresult, int state, String results, String statistics) {
		this.id = id;
		this.uuid = uuid;
		this.jobid = jobid;
		this.croninfo = croninfo;
		this.sarttime = sarttime;
		this.currenttime = currenttime;
		this.ispartialresult = ispartialresult;
		this.state = state;
		this.results = results;
		this.statistics = statistics;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getJobid() {
		return jobid;
	}

	public void setJobid(String jobid) {
		this.jobid = jobid;
	}

	public String getCroninfo() {
		return croninfo;
	}

	public void setCroninfo(String croninfo) {
		this.croninfo = croninfo;
	}

	public Date getSarttime() {
		return sarttime;
	}

	public void setSarttime(Date sarttime) {
		this.sarttime = sarttime;
	}

	public Date getCurrenttime() {
		return currenttime;
	}

	public void setCurrenttime(Date currenttime) {
		this.currenttime = currenttime;
	}

	public int getIspartialresult() {
		return ispartialresult;
	}

	public void setIspartialresult(int ispartialresult) {
		this.ispartialresult = ispartialresult;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getResults() {
		return results;
	}

	public void setResults(String results) {
		this.results = results;
	}

	public String getStatistics() {
		return statistics;
	}

	public void setStatistics(String statistics) {
		this.statistics = statistics;
	}
	
	
}
