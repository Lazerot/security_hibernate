/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="documenttype")
public class DocumenttypeEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="domainid")
	private Integer domainid;
	
	@Column(name="documentname")
	private String documentname;
	
	@Column(name="reference")
	private String reference;
	
	@Column(name="signernumber")
	private Integer signernumber;
	
	@Column(name="metadata")
	private String metadata;
	
	@Column(name="extra")
	private String extra;
	
	@Column(name="pathname")
	private String pathname;
	
	public DocumenttypeEntity () {
		
	}

	public DocumenttypeEntity(Integer id, Integer domainid, String documentname, String reference, Integer signernumber,
			String metadata, String extra, String pathname) {
		this.id = id;
		this.domainid = domainid;
		this.documentname = documentname;
		this.reference = reference;
		this.signernumber = signernumber;
		this.metadata = metadata;
		this.extra = extra;
		this.pathname = pathname;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDomainid() {
		return domainid;
	}

	public void setDomainid(Integer domainid) {
		this.domainid = domainid;
	}

	public String getDocumentname() {
		return documentname;
	}

	public void setDocumentname(String documentname) {
		this.documentname = documentname;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Integer getSignernumber() {
		return signernumber;
	}

	public void setSignernumber(Integer signernumber) {
		this.signernumber = signernumber;
	}

	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public String getPathname() {
		return pathname;
	}

	public void setPathname(String pathname) {
		this.pathname = pathname;
	}
	
	
	
	
}
