/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="auditoria")
public class AuditoriaEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="idauditoria")
	private int idauditoria;
	
	@Column(name="operacion")
	private String operacion;
	
	@Column(name="suboperacion")
	private String suboperacion;
	
	@Column(name="fechaoperacion")
	private Date fechaoperacion;
	
	@Column(name="detalles")
	private String detalles;
	
	public AuditoriaEntity() {
		
	}

	public AuditoriaEntity(int idauditoria, String operacion, String suboperacion, Date fechaoperacion,
			String detalles) {
		this.idauditoria = idauditoria;
		this.operacion = operacion;
		this.suboperacion = suboperacion;
		this.fechaoperacion = fechaoperacion;
		this.detalles = detalles;
	}

	public int getIdauditoria() {
		return idauditoria;
	}

	public void setIdauditoria(int idauditoria) {
		this.idauditoria = idauditoria;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getSuboperacion() {
		return suboperacion;
	}

	public void setSuboperacion(String suboperacion) {
		this.suboperacion = suboperacion;
	}

	public Date getFechaoperacion() {
		return fechaoperacion;
	}

	public void setFechaoperacion(Date fechaoperacion) {
		this.fechaoperacion = fechaoperacion;
	}

	public String getDetalles() {
		return detalles;
	}

	public void setDetalles(String detalles) {
		this.detalles = detalles;
	}
	
	
}
