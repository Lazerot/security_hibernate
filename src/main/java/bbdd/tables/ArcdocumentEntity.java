/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="arcdocument")
public class ArcdocumentEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="documentuuid")
	private String documentuuid;
	
	@Column(name="operationid")
	private int operationid;
	
	@Column(name="documentname")
	private String documentname;
	
	@Column(name="documentpath")
	private String documentpath;
	
	@Column(name="state")
	private int state;
	
	@Column(name="extra")
	private String extra;
	
	@Column(name="documentsize")
	private int documentsize;
	
	@Column(name="documenttype")
	private String documenttype;
	
	@Column(name="archiveddate")
	private Date archiveddate;
	
	@Column(name="sentexternaldate")
	private Date sentexternaldate;
	
	public ArcdocumentEntity() {
		
	}

	public ArcdocumentEntity(int id, String documentuuid, int operationid, String documentname, String documentpath,
			int state, String extra, int documentsize, String documenttype, Date archiveddate, Date sentexternaldate) {
		this.id = id;
		this.documentuuid = documentuuid;
		this.operationid = operationid;
		this.documentname = documentname;
		this.documentpath = documentpath;
		this.state = state;
		this.extra = extra;
		this.documentsize = documentsize;
		this.documenttype = documenttype;
		this.archiveddate = archiveddate;
		this.sentexternaldate = sentexternaldate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDocumentuuid() {
		return documentuuid;
	}

	public void setDocumentuuid(String documentuuid) {
		this.documentuuid = documentuuid;
	}

	public int getOperationid() {
		return operationid;
	}

	public void setOperationid(int operationid) {
		this.operationid = operationid;
	}

	public String getDocumentname() {
		return documentname;
	}

	public void setDocumentname(String documentname) {
		this.documentname = documentname;
	}

	public String getDocumentpath() {
		return documentpath;
	}

	public void setDocumentpath(String documentpath) {
		this.documentpath = documentpath;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public int getDocumentsize() {
		return documentsize;
	}

	public void setDocumentsize(int documentsize) {
		this.documentsize = documentsize;
	}

	public String getDocumenttype() {
		return documenttype;
	}

	public void setDocumenttype(String documenttype) {
		this.documenttype = documenttype;
	}

	public Date getArchiveddate() {
		return archiveddate;
	}

	public void setArchiveddate(Date archiveddate) {
		this.archiveddate = archiveddate;
	}

	public Date getSentexternaldate() {
		return sentexternaldate;
	}

	public void setSentexternaldate(Date sentexternaldate) {
		this.sentexternaldate = sentexternaldate;
	}
	
	
	
}
