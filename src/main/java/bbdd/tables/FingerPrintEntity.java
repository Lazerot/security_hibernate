/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pj
 *
 */
@Entity
@Table(name="fingerprint")
public class FingerPrintEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="userUuid")
	private String userUuid;
	
	@Column(name="fingerImageFilePath")
	private String fingerImageFilePath;
	
	@Column(name="fingerPosition")
	private Integer fingerPosition;
	
	@Column(name="minutiae")
	private byte[] minutiae;
	
	@Column(name="fingerImageSignedFilePath")
	private String fingerImageSignedFilePath;
	
	@Column(name="minutiaeSignedFilePath")
	private String minutiaeSignedFilePath;
	
	@Column(name="active")
	private Integer active;
	
	@Column(name="date")
	private Date date;
	
	public FingerPrintEntity() {
		
	}

	public FingerPrintEntity(String userUuid, String fingerImageFilePath, Integer fingerPosition, byte[] minutiae,
			String fingerImageSignedFilePath, String minutiaeSignedFilePath, Integer active, Date date) {
		super();
		this.userUuid = userUuid;
		this.fingerImageFilePath = fingerImageFilePath;
		this.fingerPosition = fingerPosition;
		this.minutiae = minutiae;
		this.fingerImageSignedFilePath = fingerImageSignedFilePath;
		this.minutiaeSignedFilePath = minutiaeSignedFilePath;
		this.active = active;
		this.date = date;
	}

	public String getUserUuid() {
		return userUuid;
	}

	public void setUserUuid(String userUuid) {
		this.userUuid = userUuid;
	}

	public String getFingerImageFilePath() {
		return fingerImageFilePath;
	}

	public void setFingerImageFilePath(String fingerImageFilePath) {
		this.fingerImageFilePath = fingerImageFilePath;
	}

	public Integer getFingerPosition() {
		return fingerPosition;
	}

	public void setFingerPosition(Integer fingerPosition) {
		this.fingerPosition = fingerPosition;
	}

	public byte[] getMinutiae() {
		return minutiae;
	}

	public void setMinutiae(byte[] minutiae) {
		this.minutiae = minutiae;
	}

	public String getFingerImageSignedFilePath() {
		return fingerImageSignedFilePath;
	}

	public void setFingerImageSignedFilePath(String fingerImageSignedFilePath) {
		this.fingerImageSignedFilePath = fingerImageSignedFilePath;
	}

	public String getMinutiaeSignedFilePath() {
		return minutiaeSignedFilePath;
	}

	public void setMinutiaeSignedFilePath(String minutiaeSignedFilePath) {
		this.minutiaeSignedFilePath = minutiaeSignedFilePath;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
}
