/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="vcshareddocument")
public class VcshareddocumentEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="vcvideoconferenceid")
	private Integer vcvideoconferenceid;
	
	@Column(name="uuid")
	private String uuid;
	
	@Column(name="documentname")
	private String documentname;
	
	@Column(name="documentpath")
	private String documentpath;
	
	@Column(name="extra")
	private String extra;
	
	@Column(name="state")
	private Integer state;
	
	@Column(name="documentsize")
	private Integer documentsize;
	
	public VcshareddocumentEntity() {
		
	}

	public VcshareddocumentEntity(Integer id, Integer vcvideoconferenceid, String uuid, String documentname,
			String documentpath, String extra, Integer state, Integer documentsize) {
		this.id = id;
		this.vcvideoconferenceid = vcvideoconferenceid;
		this.uuid = uuid;
		this.documentname = documentname;
		this.documentpath = documentpath;
		this.extra = extra;
		this.state = state;
		this.documentsize = documentsize;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getVcvideoconferenceid() {
		return vcvideoconferenceid;
	}

	public void setVcvideoconferenceid(Integer vcvideoconferenceid) {
		this.vcvideoconferenceid = vcvideoconferenceid;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getDocumentname() {
		return documentname;
	}

	public void setDocumentname(String documentname) {
		this.documentname = documentname;
	}

	public String getDocumentpath() {
		return documentpath;
	}

	public void setDocumentpath(String documentpath) {
		this.documentpath = documentpath;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getDocumentsize() {
		return documentsize;
	}

	public void setDocumentsize(Integer documentsize) {
		this.documentsize = documentsize;
	}
	
}
