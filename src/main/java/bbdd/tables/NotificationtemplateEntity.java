/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="notificationtemplate")
public class NotificationtemplateEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="operationtypeid")
	private Integer operationtypeid;
	
	@Column(name="subject")
	private String subject;
	
	@Column(name="body")
	private String body;
	
	@Column(name="notiftype")
	private String notiftype;
	
	@Column(name="extra")
	private String extra;
	
	@Column(name="name")
	private String name;
	
	@Column(name="creationdate")
	private Date creationdate;
	
	@Column(name="modificationdate")
	private Date modificationdate;
	
	public NotificationtemplateEntity() {
		
	}

	public NotificationtemplateEntity(Integer id, Integer operationtypeid, String subject, String body,
			String notiftype, String extra, String name, Date creationdate, Date modificationdate) {
		this.id = id;
		this.operationtypeid = operationtypeid;
		this.subject = subject;
		this.body = body;
		this.notiftype = notiftype;
		this.extra = extra;
		this.name = name;
		this.creationdate = creationdate;
		this.modificationdate = modificationdate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOperationtypeid() {
		return operationtypeid;
	}

	public void setOperationtypeid(Integer operationtypeid) {
		this.operationtypeid = operationtypeid;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getNotiftype() {
		return notiftype;
	}

	public void setNotiftype(String notiftype) {
		this.notiftype = notiftype;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public Date getModificationdate() {
		return modificationdate;
	}

	public void setModificationdate(Date modificationdate) {
		this.modificationdate = modificationdate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
