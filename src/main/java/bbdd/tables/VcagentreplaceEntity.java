/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="vcagentreplace")
public class VcagentreplaceEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="supervisorid")
	private Integer supervisorid;
	
	@Column(name="originalagentid")
	private Integer originalagentid;
	
	@Column(name="replaceagentid")
	private Integer replaceagentid;
	
	@Column(name="status")
	private Integer status;
	
	@Column(name="created")
	private Date created;
	
	@Column(name="datefrom")
	private Date datefrom;
	
	@Column(name="dateto")
	private Date dateto;
	
	@Column(name="reason")
	private String reason;
	
	public VcagentreplaceEntity() {
		
	}

	public VcagentreplaceEntity(Integer id, Integer supervisorid, Integer originalagentid, Integer replaceagentid,
			Integer status, Date created, Date datefrom, Date dateto, String reason) {
		this.id = id;
		this.supervisorid = supervisorid;
		this.originalagentid = originalagentid;
		this.replaceagentid = replaceagentid;
		this.status = status;
		this.created = created;
		this.datefrom = datefrom;
		this.dateto = dateto;
		this.reason = reason;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSupervisorid() {
		return supervisorid;
	}

	public void setSupervisorid(Integer supervisorid) {
		this.supervisorid = supervisorid;
	}

	public Integer getOriginalagentid() {
		return originalagentid;
	}

	public void setOriginalagentid(Integer originalagentid) {
		this.originalagentid = originalagentid;
	}

	public Integer getReplaceagentid() {
		return replaceagentid;
	}

	public void setReplaceagentid(Integer replaceagentid) {
		this.replaceagentid = replaceagentid;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getDatefrom() {
		return datefrom;
	}

	public void setDatefrom(Date datefrom) {
		this.datefrom = datefrom;
	}

	public Date getDateto() {
		return dateto;
	}

	public void setDateto(Date dateto) {
		this.dateto = dateto;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
	

}
