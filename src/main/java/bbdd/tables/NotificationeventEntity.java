/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="notificationevent")
public class NotificationeventEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="notificationid")
	private Integer notificationid;
	
	@Column(name="info")
	private String info;
	
	@Column(name="xml")
	private String xml;
	
	@Column(name="eventtype")
	private String eventtype;
	
	@Column(name="eventsequence")
	private Integer eventsequence;
	
	@Column(name="signed")
	private Integer signed;
	
	public NotificationeventEntity() {
		
	}

	public NotificationeventEntity(Integer id, Integer notificationid, String info, String xml, String eventtype,
			Integer eventsequence, Integer signed) {
		this.id = id;
		this.notificationid = notificationid;
		this.info = info;
		this.xml = xml;
		this.eventtype = eventtype;
		this.eventsequence = eventsequence;
		this.signed = signed;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNotificationid() {
		return notificationid;
	}

	public void setNotificationid(Integer notificationid) {
		this.notificationid = notificationid;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public String getEventtype() {
		return eventtype;
	}

	public void setEventtype(String eventtype) {
		this.eventtype = eventtype;
	}

	public Integer getEventsequence() {
		return eventsequence;
	}

	public void setEventsequence(Integer eventsequence) {
		this.eventsequence = eventsequence;
	}

	public Integer getSigned() {
		return signed;
	}

	public void setSigned(Integer signed) {
		this.signed = signed;
	}
	
}
