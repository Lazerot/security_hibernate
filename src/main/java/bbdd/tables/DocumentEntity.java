/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="document")
public class DocumentEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="documentid")
	private String documentid;
	
	@Column(name="operationid")
	private Integer operationid;
	
	@Column(name="documentname")
	private String documentname;
	
	@Column(name="documentpath")
	private String documentpath;
	
	@Column(name="state")
	private Integer state;
	
	@Column(name="externalsignatureevent")
	private String externalsignatureevent;
	
	@Column(name="extra")
	private String extra;
	
	@Column(name="signaturetype")
	private Integer signaturetype;
	
	@Column(name="documentsize")
	private Integer documentsize;
	
	@Column(name="documenttype")
	private String documenttype;
	
	public DocumentEntity () {
		
	}

	public DocumentEntity(Integer id, String documentid, Integer operationid, String documentname, String documentpath,
			Integer state, String externalsignatureevent, String extra, Integer signaturetype, Integer documentsize,
			String documenttype) {
		this.id = id;
		this.documentid = documentid;
		this.operationid = operationid;
		this.documentname = documentname;
		this.documentpath = documentpath;
		this.state = state;
		this.externalsignatureevent = externalsignatureevent;
		this.extra = extra;
		this.signaturetype = signaturetype;
		this.documentsize = documentsize;
		this.documenttype = documenttype;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDocumentid() {
		return documentid;
	}

	public void setDocumentid(String documentid) {
		this.documentid = documentid;
	}

	public Integer getOperationid() {
		return operationid;
	}

	public void setOperationid(Integer operationid) {
		this.operationid = operationid;
	}

	public String getDocumentname() {
		return documentname;
	}

	public void setDocumentname(String documentname) {
		this.documentname = documentname;
	}

	public String getDocumentpath() {
		return documentpath;
	}

	public void setDocumentpath(String documentpath) {
		this.documentpath = documentpath;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getExternalsignatureevent() {
		return externalsignatureevent;
	}

	public void setExternalsignatureevent(String externalsignatureevent) {
		this.externalsignatureevent = externalsignatureevent;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public Integer getSignaturetype() {
		return signaturetype;
	}

	public void setSignaturetype(Integer signaturetype) {
		this.signaturetype = signaturetype;
	}

	public Integer getDocumentsize() {
		return documentsize;
	}

	public void setDocumentsize(Integer documentsize) {
		this.documentsize = documentsize;
	}

	public String getDocumenttype() {
		return documenttype;
	}

	public void setDocumenttype(String documenttype) {
		this.documenttype = documenttype;
	}
	

}
