/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="operationinfo")
public class OperationinfoEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="operationid")
	private Integer operationid;
	
	@Column(name="operationuuid")
	private String operationuuid;
	
	@Column(name="costumerdata")
	private String costumerdata;
	
	@Column(name="reviewerdata")
	private String reviewerdata;
	
	@Column(name="reviewstatus")
	private String reviewstatus;
	
	@Column(name="pdfdoc")
	private String pdfdoc;
	
	@Column(name="creationdate")
	private Date creationdate;
	
	@Column(name="reviewdate")
	private Date reviewdate;
	
	@Column(name="signatures")
	private String signatures;
	
	@Column(name="reviewerid")
	private Integer reviewerid;
	
	public OperationinfoEntity() {
		
	}

	public OperationinfoEntity(Integer id, Integer operationid, String operationuuid, String costumerdata,
			String reviewerdata, String reviewstatus, String pdfdoc, Date creationdate, Date reviewdate,
			String signatures, Integer reviewerid) {
		this.id = id;
		this.operationid = operationid;
		this.operationuuid = operationuuid;
		this.costumerdata = costumerdata;
		this.reviewerdata = reviewerdata;
		this.reviewstatus = reviewstatus;
		this.pdfdoc = pdfdoc;
		this.creationdate = creationdate;
		this.reviewdate = reviewdate;
		this.signatures = signatures;
		this.reviewerid = reviewerid;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOperationid() {
		return operationid;
	}

	public void setOperationid(Integer operationid) {
		this.operationid = operationid;
	}

	public String getOperationuuid() {
		return operationuuid;
	}

	public void setOperationuuid(String operationuuid) {
		this.operationuuid = operationuuid;
	}

	public String getCostumerdata() {
		return costumerdata;
	}

	public void setCostumerdata(String costumerdata) {
		this.costumerdata = costumerdata;
	}

	public String getReviewerdata() {
		return reviewerdata;
	}

	public void setReviewerdata(String reviewerdata) {
		this.reviewerdata = reviewerdata;
	}

	public String getReviewstatus() {
		return reviewstatus;
	}

	public void setReviewstatus(String reviewstatus) {
		this.reviewstatus = reviewstatus;
	}

	public String getPdfdoc() {
		return pdfdoc;
	}

	public void setPdfdoc(String pdfdoc) {
		this.pdfdoc = pdfdoc;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public Date getReviewdate() {
		return reviewdate;
	}

	public void setReviewdate(Date reviewdate) {
		this.reviewdate = reviewdate;
	}

	public String getSignatures() {
		return signatures;
	}

	public void setSignatures(String signatures) {
		this.signatures = signatures;
	}

	public Integer getReviewerid() {
		return reviewerid;
	}

	public void setReviewerid(Integer reviewerid) {
		this.reviewerid = reviewerid;
	}
	
	
}
