/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="vctestconnection")
public class VctestconnectionEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="vcappointmentid")
	private Integer vcappointmentid;
	
	@Column(name="extra")
	private String extra;
	
	@Column(name="status")
	private Integer status;
	
	public VctestconnectionEntity() {
		
	}

	public VctestconnectionEntity(Integer id, Integer vcappointmentid, String extra, Integer status) {
		this.id = id;
		this.vcappointmentid = vcappointmentid;
		this.extra = extra;
		this.status = status;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getVcappointmentid() {
		return vcappointmentid;
	}

	public void setVcappointmentid(Integer vcappointmentid) {
		this.vcappointmentid = vcappointmentid;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
}
