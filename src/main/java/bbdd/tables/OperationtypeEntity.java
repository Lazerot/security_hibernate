/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="operationtype")
public class OperationtypeEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="domainid")
	private Integer domainid;
	
	@Column(name="name")
	private String name;
	
	@Column(name="signaturetype")
	private String signaturetype;
	
	@Column(name="documents")
	private String documents;
	
	@Column(name="authmetadata")
	private String authmetadata;
	
	@Column(name="limitsmetadata")
	private String limitsmetadata;
	
	@Column(name="conditionsmetadata")
	private String conditionsmetadata;
	
	@Column(name="reminders")
	private String reminders;
	
	@Column(name="extra")
	private String extra;
	
	@Column(name="creationdate")
	private Date creationdate;
	
	@Column(name="modificationdate")
	private Date modificationdate;
	
	@Column(name="mutiplesigndata")
	private String mutiplesigndata;
	
	@Column(name="advancedoptions")
	private String advancedoptions;

	public OperationtypeEntity() {
		
	}

	public OperationtypeEntity(Integer id, Integer domainid, String name, String signaturetype, String documents,
			String authmetadata, String limitsmetadata, String conditionsmetadata, String reminders, String extra,
			Date creationdate, Date modificationdate, String mutiplesigndata, String advancedoptions) {
		this.id = id;
		this.domainid = domainid;
		this.name = name;
		this.signaturetype = signaturetype;
		this.documents = documents;
		this.authmetadata = authmetadata;
		this.limitsmetadata = limitsmetadata;
		this.conditionsmetadata = conditionsmetadata;
		this.reminders = reminders;
		this.extra = extra;
		this.creationdate = creationdate;
		this.modificationdate = modificationdate;
		this.mutiplesigndata = mutiplesigndata;
		this.advancedoptions = advancedoptions;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDomainid() {
		return domainid;
	}

	public void setDomainid(Integer domainid) {
		this.domainid = domainid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSignaturetype() {
		return signaturetype;
	}

	public void setSignaturetype(String signaturetype) {
		this.signaturetype = signaturetype;
	}

	public String getDocuments() {
		return documents;
	}

	public void setDocuments(String documents) {
		this.documents = documents;
	}

	public String getAuthmetadata() {
		return authmetadata;
	}

	public void setAuthmetadata(String authmetadata) {
		this.authmetadata = authmetadata;
	}

	public String getLimitsmetadata() {
		return limitsmetadata;
	}

	public void setLimitsmetadata(String limitsmetadata) {
		this.limitsmetadata = limitsmetadata;
	}

	public String getConditionsmetadata() {
		return conditionsmetadata;
	}

	public void setConditionsmetadata(String conditionsmetadata) {
		this.conditionsmetadata = conditionsmetadata;
	}

	public String getReminders() {
		return reminders;
	}

	public void setReminders(String reminders) {
		this.reminders = reminders;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public Date getModificationdate() {
		return modificationdate;
	}

	public void setModificationdate(Date modificationdate) {
		this.modificationdate = modificationdate;
	}

	public String getMutiplesigndata() {
		return mutiplesigndata;
	}

	public void setMutiplesigndata(String mutiplesigndata) {
		this.mutiplesigndata = mutiplesigndata;
	}

	public String getAdvancedoptions() {
		return advancedoptions;
	}

	public void setAdvancedoptions(String advancedoptions) {
		this.advancedoptions = advancedoptions;
	}
	
}
