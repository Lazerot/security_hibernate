package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="operationbio")
public class OperationbioEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="idoperation")
	private String idoperation;
	
	@Column(name="creationDate")
	private Date creationDate;
	
	@Column(name="creator")
	private String creator;

	@Column(name="metadata")
	private String metadata;
	
	@Column(name="state")
	private Integer state;
	
	@Column(name="idpolicytype")
	private Integer idpolicytype;
	
	public OperationbioEntity () {
		
	}

	public OperationbioEntity(Integer id, String idoperation, Date creationDate, String creator, String metadata,
			Integer state, Integer idpolicytype) {
		this.id = id;
		this.idoperation = idoperation;
		this.creationDate = creationDate;
		this.creator = creator;
		this.metadata = metadata;
		this.state = state;
		this.idpolicytype = idpolicytype;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIdoperation() {
		return idoperation;
	}

	public void setIdoperation(String idoperation) {
		this.idoperation = idoperation;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getIdpolicytype() {
		return idpolicytype;
	}

	public void setIdpolicytype(Integer idpolicytype) {
		this.idpolicytype = idpolicytype;
	}
	
	
}
