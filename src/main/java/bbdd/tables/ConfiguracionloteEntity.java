/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="configuracionlote")
public class ConfiguracionloteEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="esgeneral")
	private int esgeneral;
	
	@Column(name="iddominio")
	private int iddominio;

	@Column(name="expresioncron")
	private String expresioncron;
	
	public ConfiguracionloteEntity () {
		
	}

	public ConfiguracionloteEntity(int id, int esgeneral, int iddominio, String expresioncron) {
		this.id = id;
		this.esgeneral = esgeneral;
		this.iddominio = iddominio;
		this.expresioncron = expresioncron;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEsgeneral() {
		return esgeneral;
	}

	public void setEsgeneral(int esgeneral) {
		this.esgeneral = esgeneral;
	}

	public int getIddominio() {
		return iddominio;
	}

	public void setIddominio(int iddominio) {
		this.iddominio = iddominio;
	}

	public String getExpresioncron() {
		return expresioncron;
	}

	public void setExpresioncron(String expresioncron) {
		this.expresioncron = expresioncron;
	}
	
}
