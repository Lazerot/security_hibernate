/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="reviewer")
public class ReviewerEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="uuid")
	private Integer uuid;
	
	@Column(name="username")
	private String username;

	@Column(name="passwdmd5")
	private String passwdmd5;
	
	@Column(name="publickey")
	private String publickey;
	
	@Column(name="privatekey")
	private String privatekey;
	
	public ReviewerEntity () {
		
	}

	public ReviewerEntity(Integer id, Integer uuid, String username, String passwdmd5, String publickey,
			String privatekey) {
		this.id = id;
		this.uuid = uuid;
		this.username = username;
		this.passwdmd5 = passwdmd5;
		this.publickey = publickey;
		this.privatekey = privatekey;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUuid() {
		return uuid;
	}

	public void setUuid(Integer uuid) {
		this.uuid = uuid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPasswdmd5() {
		return passwdmd5;
	}

	public void setPasswdmd5(String passwdmd5) {
		this.passwdmd5 = passwdmd5;
	}

	public String getPublickey() {
		return publickey;
	}

	public void setPublickey(String publickey) {
		this.publickey = publickey;
	}

	public String getPrivatekey() {
		return privatekey;
	}

	public void setPrivatekey(String privatekey) {
		this.privatekey = privatekey;
	}
	
}
