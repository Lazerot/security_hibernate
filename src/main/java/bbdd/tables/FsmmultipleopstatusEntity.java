/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="fsmmultipleopstatus")
public class FsmmultipleopstatusEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="multipleuuid")
	private String multipleuuid;
	
	@Column(name="domainid")
	private Integer domainid;
	
	@Column(name="statusdate")
	private Date statusdate;
	
	@Column(name="extra")
	private String extra;
	
	@Column(name="status")
	private String status;
	
	@Column(name="fsmbase64")
	private String fsmbase64;
	
	public FsmmultipleopstatusEntity() {
		
	}

	public FsmmultipleopstatusEntity(Integer id, String multipleuuid, Integer domainid, Date statusdate, String extra,
			String status, String fsmbase64) {
		this.id = id;
		this.multipleuuid = multipleuuid;
		this.domainid = domainid;
		this.statusdate = statusdate;
		this.extra = extra;
		this.status = status;
		this.fsmbase64 = fsmbase64;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMultipleuuid() {
		return multipleuuid;
	}

	public void setMultipleuuid(String multipleuuid) {
		this.multipleuuid = multipleuuid;
	}

	public Integer getDomainid() {
		return domainid;
	}

	public void setDomainid(Integer domainid) {
		this.domainid = domainid;
	}

	public Date getStatusdate() {
		return statusdate;
	}

	public void setStatusdate(Date statusdate) {
		this.statusdate = statusdate;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFsmbase64() {
		return fsmbase64;
	}

	public void setFsmbase64(String fsmbase64) {
		this.fsmbase64 = fsmbase64;
	}
	
}
