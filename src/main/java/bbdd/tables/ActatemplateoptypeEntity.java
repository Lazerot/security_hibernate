package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */

@Entity
@Table(name="actatemplateoptype")
public class ActatemplateoptypeEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="actatemplateid")
	private int actatemplateid;
	
	@Column(name="operationtypeid")
	private int operationtypeid;
	
	@Column(name="extra")
	private String extra;
	
	public ActatemplateoptypeEntity() {
		
	}

	public ActatemplateoptypeEntity(int id, int actatemplateid, int operationtypeid, String extra) {
		
		this.id = id;
		this.actatemplateid = actatemplateid;
		this.operationtypeid = operationtypeid;
		this.extra = extra;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getActatemplateid() {
		return actatemplateid;
	}

	public void setActatemplateid(int actatemplateid) {
		this.actatemplateid = actatemplateid;
	}

	public int getOperationtypeid() {
		return operationtypeid;
	}

	public void setOperationtypeid(int operationtypeid) {
		this.operationtypeid = operationtypeid;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}
	
	
	
	
}
