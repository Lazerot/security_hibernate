/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="operation")
public class OperationEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="operationid")
	private String operationid;
	
	@Column(name="domainid")
	private Integer domainid;
	
	@Column(name="state")
	private Integer state;
	
	@Column(name="optype")
	private String optype;
	
	@Column(name="opclass")
	private String opclass;
	
	@Column(name="externalsignatureevent")
	private Integer externalsignatureevent;
	
	@Column(name="extra")
	private String extra;
	
	@Column(name="signaturetype")
	private Integer signaturetype;
	
	@Column(name="creationdate")
	private Date creationdate;
	
	@Column(name="uuid")
	private String uuid;
	
	@Column(name="multipleuuid")
	private String multipleuuid;
	
	@Column(name="domainuserid")
	private Integer domainuserid;
	
	@Column(name="signaturedate")
	private Date signaturedate;
	
	@Column(name="optypeextended")
	private String optypeextended;
	
	@Column(name="customercreatorid")
	private Integer customercreatorid;
	
	@Column(name="cellphone")
	private String cellphone;
	
	@Column(name="rejectdate")
	private Date rejecdate;
	
	
	public OperationEntity() {
		
	}

	public OperationEntity(Integer id, String operationid, Integer domainid, Integer state, String optype,
			String opclass, Integer externalsignatureevent, String extra, Integer signaturetype, Date creationdate,
			String uuid, String multipleuuid, Integer domainuserid, Date signaturedate, String optypeextended,
			Integer customercreatorid, String cellphone, Date rejecdate) {
		this.id = id;
		this.operationid = operationid;
		this.domainid = domainid;
		this.state = state;
		this.optype = optype;
		this.opclass = opclass;
		this.externalsignatureevent = externalsignatureevent;
		this.extra = extra;
		this.signaturetype = signaturetype;
		this.creationdate = creationdate;
		this.uuid = uuid;
		this.multipleuuid = multipleuuid;
		this.domainuserid = domainuserid;
		this.signaturedate = signaturedate;
		this.optypeextended = optypeextended;
		this.customercreatorid = customercreatorid;
		this.cellphone = cellphone;
		this.rejecdate = rejecdate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOperationid() {
		return operationid;
	}

	public void setOperationid(String operationid) {
		this.operationid = operationid;
	}

	public Integer getDomainid() {
		return domainid;
	}

	public void setDomainid(Integer domainid) {
		this.domainid = domainid;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getOptype() {
		return optype;
	}

	public void setOptype(String optype) {
		this.optype = optype;
	}

	public String getOpclass() {
		return opclass;
	}

	public void setOpclass(String opclass) {
		this.opclass = opclass;
	}

	public Integer getExternalsignatureevent() {
		return externalsignatureevent;
	}

	public void setExternalsignatureevent(Integer externalsignatureevent) {
		this.externalsignatureevent = externalsignatureevent;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public Integer getSignaturetype() {
		return signaturetype;
	}

	public void setSignaturetype(Integer signaturetype) {
		this.signaturetype = signaturetype;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getMultipleuuid() {
		return multipleuuid;
	}

	public void setMultipleuuid(String multipleuuid) {
		this.multipleuuid = multipleuuid;
	}

	public Integer getDomainuserid() {
		return domainuserid;
	}

	public void setDomainuserid(Integer domainuserid) {
		this.domainuserid = domainuserid;
	}

	public Date getSignaturedate() {
		return signaturedate;
	}

	public void setSignaturedate(Date signaturedate) {
		this.signaturedate = signaturedate;
	}

	public String getOptypeextended() {
		return optypeextended;
	}

	public void setOptypeextended(String optypeextended) {
		this.optypeextended = optypeextended;
	}

	public Integer getCustomercreatorid() {
		return customercreatorid;
	}

	public void setCustomercreatorid(Integer customercreatorid) {
		this.customercreatorid = customercreatorid;
	}

	public String getCellphone() {
		return cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	public Date getRejecdate() {
		return rejecdate;
	}

	public void setRejecdate(Date rejecdate) {
		this.rejecdate = rejecdate;
	}
	
}
