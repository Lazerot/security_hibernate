/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="metadatalabel")
public class MetadatalabelEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="tagname")
	private String tagname;
	
	@Column(name="metadatalang")
	private String metadatalang;
	
	@Column(name="metadatalabel")
	private String metadatalabel;
	
	@Column(name="metadatatype")
	private Integer metadatatype;
	
	@Column(name="listoptions")
	private String listoptions;
	
	@Column(name="validationregex")
	private String validationregex;
	
	@Column(name="metadatarange")
	private Integer metadatarange;
	
	public MetadatalabelEntity () {
		
	}

	public MetadatalabelEntity(Integer id, String tagname, String metadatalang, String metadatalabel,
			Integer metadatatype, String listoptions, String validationregex, Integer metadatarange) {
		this.id = id;
		this.tagname = tagname;
		this.metadatalang = metadatalang;
		this.metadatalabel = metadatalabel;
		this.metadatatype = metadatatype;
		this.listoptions = listoptions;
		this.validationregex = validationregex;
		this.metadatarange = metadatarange;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTagname() {
		return tagname;
	}

	public void setTagname(String tagname) {
		this.tagname = tagname;
	}

	public String getMetadatalang() {
		return metadatalang;
	}

	public void setMetadatalang(String metadatalang) {
		this.metadatalang = metadatalang;
	}

	public String getMetadatalabel() {
		return metadatalabel;
	}

	public void setMetadatalabel(String metadatalabel) {
		this.metadatalabel = metadatalabel;
	}

	public Integer getMetadatatype() {
		return metadatatype;
	}

	public void setMetadatatype(Integer metadatatype) {
		this.metadatatype = metadatatype;
	}

	public String getListoptions() {
		return listoptions;
	}

	public void setListoptions(String listoptions) {
		this.listoptions = listoptions;
	}

	public String getValidationregex() {
		return validationregex;
	}

	public void setValidationregex(String validationregex) {
		this.validationregex = validationregex;
	}

	public Integer getMetadatarange() {
		return metadatarange;
	}

	public void setMetadatarange(Integer metadatarange) {
		this.metadatarange = metadatarange;
	}
	
}
