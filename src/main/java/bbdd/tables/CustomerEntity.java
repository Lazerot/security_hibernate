/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="customer")
public class CustomerEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	
	@Column(name="nif")
	private String nif;
	
	@Column(name="name")
	private String name;
	
	@Column(name="lastname")
	private String lastname;
	
	@Column(name="cellphone")
	private String cellphone;
	
	@Column(name="email")
	private String email;
	
	@Column(name="address")
	private String address;
	
	@Column(name="city")
	private String city;
	
	@Column(name="cp")
	private String cp;
	
	@Column(name="extra")
	private String extra;
	
	@Column(name="certificateid")
	private String certificateid;
	
	@Column(name="creationdate")
	private Date creationdate;
	
	@Column(name="lastoperationdate")
	private Date lastoperationdate;
	
	@Column(name="lastaccessdate")
	private Date lastaccessdate;
	
	@Column(name="publickey")
	private String publickey;
	
	@Column(name="privatekey")
	private String privateKey;
	
	@Column(name="pass")
	private String pass;
	
	@Column(name="pushtoken")
	private String pushtoken;
	
	@Column(name="uuid")
	private String uuid;
	
	@Column(name="boxaccess")
	private Integer boxaccess;
	
	@Column(name="domainid")
	private Integer domainid;
	
	public CustomerEntity() {
		
	}

	public CustomerEntity(Integer id, String nif, String name, String lastname, String cellphone, String email,
			String address, String city, String cp, String extra, String certificateid, Date creationdate,
			Date lastoperationdate, Date lastaccessdate, String publickey, String privateKey, String pass,
			String pushtoken, String uuid, Integer boxaccess, Integer domainid) {
		this.id = id;
		this.nif = nif;
		this.name = name;
		this.lastname = lastname;
		this.cellphone = cellphone;
		this.email = email;
		this.address = address;
		this.city = city;
		this.cp = cp;
		this.extra = extra;
		this.certificateid = certificateid;
		this.creationdate = creationdate;
		this.lastoperationdate = lastoperationdate;
		this.lastaccessdate = lastaccessdate;
		this.publickey = publickey;
		this.privateKey = privateKey;
		this.pass = pass;
		this.pushtoken = pushtoken;
		this.uuid = uuid;
		this.boxaccess = boxaccess;
		this.domainid = domainid;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getCellphone() {
		return cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public String getCertificateid() {
		return certificateid;
	}

	public void setCertificateid(String certificateid) {
		this.certificateid = certificateid;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public Date getLastoperationdate() {
		return lastoperationdate;
	}

	public void setLastoperationdate(Date lastoperationdate) {
		this.lastoperationdate = lastoperationdate;
	}

	public Date getLastaccessdate() {
		return lastaccessdate;
	}

	public void setLastaccessdate(Date lastaccessdate) {
		this.lastaccessdate = lastaccessdate;
	}

	public String getPublickey() {
		return publickey;
	}

	public void setPublickey(String publickey) {
		this.publickey = publickey;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getPushtoken() {
		return pushtoken;
	}

	public void setPushtoken(String pushtoken) {
		this.pushtoken = pushtoken;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Integer getBoxaccess() {
		return boxaccess;
	}

	public void setBoxaccess(Integer boxaccess) {
		this.boxaccess = boxaccess;
	}

	public Integer getDomainid() {
		return domainid;
	}

	public void setDomainid(Integer domainid) {
		this.domainid = domainid;
	}
	
}
