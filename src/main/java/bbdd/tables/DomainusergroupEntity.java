/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="domainusergroup")
public class DomainusergroupEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="domainid")
	private Integer domainid;
	
	@Column(name="groupname")
	private String groupname;
	
	@Column(name="groupadmin")
	private Integer groupadmin;
	
	@Column(name="parentid")
	private Integer parentid;
	
	public DomainusergroupEntity () {
		
	}

	public DomainusergroupEntity(Integer id, Integer domainid, String groupname, Integer groupadmin, Integer parentid) {
		this.id = id;
		this.domainid = domainid;
		this.groupname = groupname;
		this.groupadmin = groupadmin;
		this.parentid = parentid;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDomainid() {
		return domainid;
	}

	public void setDomainid(Integer domainid) {
		this.domainid = domainid;
	}

	public String getGroupname() {
		return groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	public Integer getGroupadmin() {
		return groupadmin;
	}

	public void setGroupadmin(Integer groupadmin) {
		this.groupadmin = groupadmin;
	}

	public Integer getParentid() {
		return parentid;
	}

	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}
	
}
