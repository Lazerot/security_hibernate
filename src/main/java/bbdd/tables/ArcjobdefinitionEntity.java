/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="arcjobdefinition")
public class ArcjobdefinitionEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="jobname")
	private String jobname;
	
	@Column(name="domainid")
	private int domainid;
	
	@Column(name="scheduleinfo")
	private String scheduleinfo;
	
	@Column(name="command")
	private String command;
	
	@Column(name="comments")
	private String comments;
	
	@Column(name="state")
	private int state;
	
	@Column(name="jobtype")
	private int jobtype;
	
	@Column(name="uuid")
	private String uuid;
	
	@Column(name="cronuser")
	private String cronuser;
	
	@Column(name="parameters")
	private String parameters;
	
	public ArcjobdefinitionEntity () {
		
	}

	public ArcjobdefinitionEntity(int id, String jobname, int domainid, String scheduleinfo, String command,
			String comments, int state, int jobtype, String uuid, String cronuser, String parameters) {
		this.id = id;
		this.jobname = jobname;
		this.domainid = domainid;
		this.scheduleinfo = scheduleinfo;
		this.command = command;
		this.comments = comments;
		this.state = state;
		this.jobtype = jobtype;
		this.uuid = uuid;
		this.cronuser = cronuser;
		this.parameters = parameters;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getJobname() {
		return jobname;
	}

	public void setJobname(String jobname) {
		this.jobname = jobname;
	}

	public int getDomainid() {
		return domainid;
	}

	public void setDomainid(int domainid) {
		this.domainid = domainid;
	}

	public String getScheduleinfo() {
		return scheduleinfo;
	}

	public void setScheduleinfo(String scheduleinfo) {
		this.scheduleinfo = scheduleinfo;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getJobtype() {
		return jobtype;
	}

	public void setJobtype(int jobtype) {
		this.jobtype = jobtype;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getCronuser() {
		return cronuser;
	}

	public void setCronuser(String cronuser) {
		this.cronuser = cronuser;
	}

	public String getParameters() {
		return parameters;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}
	
	
}
