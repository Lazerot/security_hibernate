/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="vcvideoconference")
public class VcvideoconferenceEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="vcappointmentid")
	private Integer vcappointmentid;
	
	@Column(name="vcappoinmentuuid")
	private String vcappoinmentuuid;
	
	@Column(name="creationdate")
	private Date creationdate;
	
	@Column(name="sessionid")
	private String sessionid;
	
	@Column(name="sessiondata")
	private String sessiondata;
	
	@Column(name="videostate")
	private String videostate;
	
	@Column(name="archiveid")
	private String archiveid;
	
	@Column(name="archiveinfo")
	private String archiveinfo;
	
	@Column(name="metadata")
	private String metadata;
	
	@Column(name="signature")
	private String signature;
	
	@Column(name="domainid")
	private Integer domainid;
	
	@Column(name="domainuserid")
	private Integer domainuserid;
	
	@Column(name="username")
	private String username;
	
	@Column(name="customername")
	private String customername;
	
	@Column(name="agentdatein")
	private Date agentdatein;
	
	@Column(name="agentdateout")
	private Date agentdateout;
	
	@Column(name="customerdatein")
	private Date customerdatein;
	
	@Column(name="customerdateout")
	private Date customerdateout;
	
	@Column(name="duration")
	private Integer duration;
	
	public VcvideoconferenceEntity() {
		
	}

	public VcvideoconferenceEntity(Integer id, Integer vcappointmentid, String vcappoinmentuuid, Date creationdate,
			String sessionid, String sessiondata, String videostate, String archiveid, String archiveinfo,
			String metadata, String signature, Integer domainid, Integer domainuserid, String username,
			String customername, Date agentdatein, Date agentdateout, Date customerdatein, Date customerdateout,
			Integer duration) {
		this.id = id;
		this.vcappointmentid = vcappointmentid;
		this.vcappoinmentuuid = vcappoinmentuuid;
		this.creationdate = creationdate;
		this.sessionid = sessionid;
		this.sessiondata = sessiondata;
		this.videostate = videostate;
		this.archiveid = archiveid;
		this.archiveinfo = archiveinfo;
		this.metadata = metadata;
		this.signature = signature;
		this.domainid = domainid;
		this.domainuserid = domainuserid;
		this.username = username;
		this.customername = customername;
		this.agentdatein = agentdatein;
		this.agentdateout = agentdateout;
		this.customerdatein = customerdatein;
		this.customerdateout = customerdateout;
		this.duration = duration;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getVcappointmentid() {
		return vcappointmentid;
	}

	public void setVcappointmentid(Integer vcappointmentid) {
		this.vcappointmentid = vcappointmentid;
	}

	public String getVcappoinmentuuid() {
		return vcappoinmentuuid;
	}

	public void setVcappoinmentuuid(String vcappoinmentuuid) {
		this.vcappoinmentuuid = vcappoinmentuuid;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public String getSessionid() {
		return sessionid;
	}

	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}

	public String getSessiondata() {
		return sessiondata;
	}

	public void setSessiondata(String sessiondata) {
		this.sessiondata = sessiondata;
	}

	public String getVideostate() {
		return videostate;
	}

	public void setVideostate(String videostate) {
		this.videostate = videostate;
	}

	public String getArchiveid() {
		return archiveid;
	}

	public void setArchiveid(String archiveid) {
		this.archiveid = archiveid;
	}

	public String getArchiveinfo() {
		return archiveinfo;
	}

	public void setArchiveinfo(String archiveinfo) {
		this.archiveinfo = archiveinfo;
	}

	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public Integer getDomainid() {
		return domainid;
	}

	public void setDomainid(Integer domainid) {
		this.domainid = domainid;
	}

	public Integer getDomainuserid() {
		return domainuserid;
	}

	public void setDomainuserid(Integer domainuserid) {
		this.domainuserid = domainuserid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCustomername() {
		return customername;
	}

	public void setCustomername(String customername) {
		this.customername = customername;
	}

	public Date getAgentdatein() {
		return agentdatein;
	}

	public void setAgentdatein(Date agentdatein) {
		this.agentdatein = agentdatein;
	}

	public Date getAgentdateout() {
		return agentdateout;
	}

	public void setAgentdateout(Date agentdateout) {
		this.agentdateout = agentdateout;
	}

	public Date getCustomerdatein() {
		return customerdatein;
	}

	public void setCustomerdatein(Date customerdatein) {
		this.customerdatein = customerdatein;
	}

	public Date getCustomerdateout() {
		return customerdateout;
	}

	public void setCustomerdateout(Date customerdateout) {
		this.customerdateout = customerdateout;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	
}
