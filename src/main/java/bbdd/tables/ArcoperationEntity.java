/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="arcoperation")
public class ArcoperationEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="operationid")
	private String operationid;
	
	@Column(name="domainid")
	private int domainid;
	
	@Column(name="state")
	private int state;
	
	@Column(name="optypeextended")
	private String optypeextended;
	
	@Column(name="opclass")
	private int opclass;
	
	@Column(name="extra")
	private String extra;
	
	@Column(name="creationdate")
	private Date creationdate;
	
	@Column(name="archiveddate")
	private Date archiveddate;
	
	@Column(name="sentexternaldate")
	private Date sentexternaldate;
	
	@Column(name="uuid")
	private String uuid;
	
	@Column(name="creatorid")
	private int creatorid;
	
	@Column(name="creatorusername")
	private String creatorusername;
	
	@Column(name="client")
	private int client;
	
	@Column(name="reconcil")
	private int reconcil;
	
	public ArcoperationEntity () {
		
	}

	public ArcoperationEntity(int id, String operationid, int domainid, int state, String optypeextended, int opclass,
			String extra, Date creationdate, Date archiveddate, Date sentexternaldate, String uuid, int creatorid,
			String creatorusername, int client, int reconcil) {
		this.id = id;
		this.operationid = operationid;
		this.domainid = domainid;
		this.state = state;
		this.optypeextended = optypeextended;
		this.opclass = opclass;
		this.extra = extra;
		this.creationdate = creationdate;
		this.archiveddate = archiveddate;
		this.sentexternaldate = sentexternaldate;
		this.uuid = uuid;
		this.creatorid = creatorid;
		this.creatorusername = creatorusername;
		this.client = client;
		this.reconcil = reconcil;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOperationid() {
		return operationid;
	}

	public void setOperationid(String operationid) {
		this.operationid = operationid;
	}

	public int getDomainid() {
		return domainid;
	}

	public void setDomainid(int domainid) {
		this.domainid = domainid;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getOptypeextended() {
		return optypeextended;
	}

	public void setOptypeextended(String optypeextended) {
		this.optypeextended = optypeextended;
	}

	public int getOpclass() {
		return opclass;
	}

	public void setOpclass(int opclass) {
		this.opclass = opclass;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public Date getArchiveddate() {
		return archiveddate;
	}

	public void setArchiveddate(Date archiveddate) {
		this.archiveddate = archiveddate;
	}

	public Date getSentexternaldate() {
		return sentexternaldate;
	}

	public void setSentexternaldate(Date sentexternaldate) {
		this.sentexternaldate = sentexternaldate;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public int getCreatorid() {
		return creatorid;
	}

	public void setCreatorid(int creatorid) {
		this.creatorid = creatorid;
	}

	public String getCreatorusername() {
		return creatorusername;
	}

	public void setCreatorusername(String creatorusername) {
		this.creatorusername = creatorusername;
	}

	public int getClient() {
		return client;
	}

	public void setClient(int client) {
		this.client = client;
	}

	public int getReconcil() {
		return reconcil;
	}

	public void setReconcil(int reconcil) {
		this.reconcil = reconcil;
	}
	
	
	
}
