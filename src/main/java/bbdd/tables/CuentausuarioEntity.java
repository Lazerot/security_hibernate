/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="cuentausuario")
public class CuentausuarioEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="idusuario")
	private int idusuario;

	@Column(name="nombre")
	private String nombre;
	
	@Column(name="apellidos")
	private String apellidos;
	
	@Column(name="fecha_nacimiento")
	private Date fecha_nacimiento;
	
	@Column(name="nif")
	private String nif;
	
	@Column(name="direccion")
	private String direccion;
	
	@Column(name="localidad")
	private String localidad;
	
	@Column(name="codpostal")
	private String codpostal;
	
	@Column(name="movil")
	private String movil;
	
	@Column(name="email")
	private String email;
	
	@Column(name="claveacceso")
	private String claveacceso;
	
	@Column(name="pin", nullable=true)
	private String pin;
	
	@Column(name="esadmin", nullable=true)
	private Integer esadmin;
	
	@Column(name="datosadicionales")
	private String datosadicionales;
	
	public CuentausuarioEntity() {
		
	}

	public CuentausuarioEntity(int idusuario, String nombre, String apellidos, Date fecha_nacimiento, String nif,
			String direccion, String localidad, String codpostal, String movil, String email, String claveacceso,
			String pin, Integer esadmin, String datosadicionales) {
		this.idusuario = idusuario;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.fecha_nacimiento = fecha_nacimiento;
		this.nif = nif;
		this.direccion = direccion;
		this.localidad = localidad;
		this.codpostal = codpostal;
		this.movil = movil;
		this.email = email;
		this.claveacceso = claveacceso;
		this.pin = pin;
		this.esadmin = esadmin;
		this.datosadicionales = datosadicionales;
	}

	public int getIdusuario() {
		return idusuario;
	}

	public void setIdusuario(int idusuario) {
		this.idusuario = idusuario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Date getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(Date fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getCodpostal() {
		return codpostal;
	}

	public void setCodpostal(String codpostal) {
		this.codpostal = codpostal;
	}

	public String getMovil() {
		return movil;
	}

	public void setMovil(String movil) {
		this.movil = movil;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getClaveacceso() {
		return claveacceso;
	}

	public void setClaveacceso(String claveacceso) {
		this.claveacceso = claveacceso;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public Integer getEsadmin() {
		return esadmin;
	}

	public void setEsadmin(Integer esadmin) {
		this.esadmin = esadmin;
	}

	public String getDatosadicionales() {
		return datosadicionales;
	}

	public void setDatosadicionales(String datosadicionales) {
		this.datosadicionales = datosadicionales;
	}
	
}
