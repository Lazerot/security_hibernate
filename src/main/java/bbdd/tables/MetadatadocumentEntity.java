/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="metadatadocument")
public class MetadatadocumentEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="documentid")
	private Integer documentid;
	
	@Column(name="tagname")
	private String tagame;
	
	@Column(name="value")
	private String value;
	
	@Column(name="domainid")
	private Integer domainid;
	
	@Column(name="operationid")
	private Integer operationid;
	
	public MetadatadocumentEntity() {
		
	}

	public MetadatadocumentEntity(Integer id, Integer documentid, String tagame, String value, Integer domainid,
			Integer operationid) {
		this.id = id;
		this.documentid = documentid;
		this.tagame = tagame;
		this.value = value;
		this.domainid = domainid;
		this.operationid = operationid;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDocumentid() {
		return documentid;
	}

	public void setDocumentid(Integer documentid) {
		this.documentid = documentid;
	}

	public String getTagame() {
		return tagame;
	}

	public void setTagame(String tagame) {
		this.tagame = tagame;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getDomainid() {
		return domainid;
	}

	public void setDomainid(Integer domainid) {
		this.domainid = domainid;
	}

	public Integer getOperationid() {
		return operationid;
	}

	public void setOperationid(Integer operationid) {
		this.operationid = operationid;
	}
	

}
