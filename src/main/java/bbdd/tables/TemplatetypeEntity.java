/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 */
@Entity
@Table(name="templatetype")
public class TemplatetypeEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="templateid")
	private String templateid;
	
	@Column(name="name")
	private String name;
	
	@Column(name="messagetype")
	private Integer messagetype;
	
	@Column(name="extra")
	private String extra;
	
	@Column(name="domainid")
	private Integer domainid;
	
	@Column(name="category1")
	private String category1;
	
	@Column(name="category2")
	private String cateogry2;
	
	@Column(name="state")
	private Integer state;
	
	@Column(name="campaignid")
	private Integer campaignid;
	
	@Column(name="typeid")
	private Integer typeid;
	
	public TemplatetypeEntity() {
		
	}

	public TemplatetypeEntity(Integer id, String templateid, String name, Integer messagetype, String extra,
			Integer domainid, String category1, String cateogry2, Integer state, Integer campaignid, Integer typeid) {
		this.id = id;
		this.templateid = templateid;
		this.name = name;
		this.messagetype = messagetype;
		this.extra = extra;
		this.domainid = domainid;
		this.category1 = category1;
		this.cateogry2 = cateogry2;
		this.state = state;
		this.campaignid = campaignid;
		this.typeid = typeid;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTemplateid() {
		return templateid;
	}

	public void setTemplateid(String templateid) {
		this.templateid = templateid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getMessagetype() {
		return messagetype;
	}

	public void setMessagetype(Integer messagetype) {
		this.messagetype = messagetype;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public Integer getDomainid() {
		return domainid;
	}

	public void setDomainid(Integer domainid) {
		this.domainid = domainid;
	}

	public String getCategory1() {
		return category1;
	}

	public void setCategory1(String category1) {
		this.category1 = category1;
	}

	public String getCateogry2() {
		return cateogry2;
	}

	public void setCateogry2(String cateogry2) {
		this.cateogry2 = cateogry2;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getCampaignid() {
		return campaignid;
	}

	public void setCampaignid(Integer campaignid) {
		this.campaignid = campaignid;
	}

	public Integer getTypeid() {
		return typeid;
	}

	public void setTypeid(Integer typeid) {
		this.typeid = typeid;
	}
	
}
