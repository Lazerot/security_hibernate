/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="notariodominio")
public class NotariodominioEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="idnotario")
	private Integer idnotario;
	
	@Column(name="iddominio")
	private Integer iddominio;
	
	@Column(name="extra")
	private String extra;

	public NotariodominioEntity () {
		
	}

	public NotariodominioEntity(Integer id, Integer idnotario, Integer iddominio, String extra) {
		this.id = id;
		this.idnotario = idnotario;
		this.iddominio = iddominio;
		this.extra = extra;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdnotario() {
		return idnotario;
	}

	public void setIdnotario(Integer idnotario) {
		this.idnotario = idnotario;
	}

	public Integer getIddominio() {
		return iddominio;
	}

	public void setIddominio(Integer iddominio) {
		this.iddominio = iddominio;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}
	
}

