/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="gestordocs")
public class GestordocsEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="uuid")
	private Integer uuid;
	
	@Column(name="docname")
	private String docname;
	
	@Column(name="docpath")
	private String docpath;
	
	@Column(name="docmetadata")
	private String docmetadata;
	
	@Column(name="docmetadata2")
	private String docmetadata2;
	
	@Column(name="idoperation")
	private String idoperation;
	
	@Column(name="docstate")
	private Integer docstate;
	
	@Column(name="docsignedpath")
	private String docsignedpath;
	
	public GestordocsEntity() {
		
	}

	public GestordocsEntity(Integer uuid, String docname, String docpath, String docmetadata, String docmetadata2,
			String idoperation, Integer docstate, String docsignedpath) {
		this.uuid = uuid;
		this.docname = docname;
		this.docpath = docpath;
		this.docmetadata = docmetadata;
		this.docmetadata2 = docmetadata2;
		this.idoperation = idoperation;
		this.docstate = docstate;
		this.docsignedpath = docsignedpath;
	}

	public Integer getUuid() {
		return uuid;
	}

	public void setUuid(Integer uuid) {
		this.uuid = uuid;
	}

	public String getDocname() {
		return docname;
	}

	public void setDocname(String docname) {
		this.docname = docname;
	}

	public String getDocpath() {
		return docpath;
	}

	public void setDocpath(String docpath) {
		this.docpath = docpath;
	}

	public String getDocmetadata() {
		return docmetadata;
	}

	public void setDocmetadata(String docmetadata) {
		this.docmetadata = docmetadata;
	}

	public String getDocmetadata2() {
		return docmetadata2;
	}

	public void setDocmetadata2(String docmetadata2) {
		this.docmetadata2 = docmetadata2;
	}

	public String getIdoperation() {
		return idoperation;
	}

	public void setIdoperation(String idoperation) {
		this.idoperation = idoperation;
	}

	public Integer getDocstate() {
		return docstate;
	}

	public void setDocstate(Integer docstate) {
		this.docstate = docstate;
	}

	public String getDocsignedpath() {
		return docsignedpath;
	}

	public void setDocsignedpath(String docsignedpath) {
		this.docsignedpath = docsignedpath;
	}
	
}
