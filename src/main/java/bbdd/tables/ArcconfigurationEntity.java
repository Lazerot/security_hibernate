/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */

@Entity
@Table(name="arcconfiguration")
public class ArcconfigurationEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="domainid")
	private int domainid;
	
	@Column(name="storagetype")
	private String storagetype;
	
	@Column(name="stusername")
	private String stusername;
	
	@Column(name="stpassword")
	private String stpassword;
	
	@Column(name="basepath")
	private String basepath;
	
	@Column(name="extra")
	private String extra;
	
	@Column(name="jobinputlocations")
	private String jobinputlocations;
	
	public ArcconfigurationEntity() {
		
	}

	public ArcconfigurationEntity(int id, int domainid, String storagetype, String stusername, String stpassword,
			String basepath, String extra, String jobinputlocations) {
		this.id = id;
		this.domainid = domainid;
		this.storagetype = storagetype;
		this.stusername = stusername;
		this.stpassword = stpassword;
		this.basepath = basepath;
		this.extra = extra;
		this.jobinputlocations = jobinputlocations;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDomainid() {
		return domainid;
	}

	public void setDomainid(int domainid) {
		this.domainid = domainid;
	}

	public String getStoragetype() {
		return storagetype;
	}

	public void setStoragetype(String storagetype) {
		this.storagetype = storagetype;
	}

	public String getStusername() {
		return stusername;
	}

	public void setStusername(String stusername) {
		this.stusername = stusername;
	}

	public String getStpassword() {
		return stpassword;
	}

	public void setStpassword(String stpassword) {
		this.stpassword = stpassword;
	}

	public String getBasepath() {
		return basepath;
	}

	public void setBasepath(String basepath) {
		this.basepath = basepath;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public String getJobinputlocations() {
		return jobinputlocations;
	}

	public void setJobinputlocations(String jobinputlocations) {
		this.jobinputlocations = jobinputlocations;
	}
	
	

}
