/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="biosignconfig")
public class BiosignconfigEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="empresa")
	private String empresa;
	
	@Column(name="signType")
	private String signType;
	
	@Column(name="encriptionKey")
	private String encriptionKey;
	
	@Column(name="minPoints")
	private int minPoints;
	
	@Column(name="extradata")
	private String extradata;
	
	public BiosignconfigEntity() {
		
	}

	public BiosignconfigEntity(int id, String empresa, String signType, String encriptionKey, int minPoints,
			String extradata) {
		this.id = id;
		this.empresa = empresa;
		this.signType = signType;
		this.encriptionKey = encriptionKey;
		this.minPoints = minPoints;
		this.extradata = extradata;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getSignType() {
		return signType;
	}

	public void setSignType(String signType) {
		this.signType = signType;
	}

	public String getEncriptionKey() {
		return encriptionKey;
	}

	public void setEncriptionKey(String encriptionKey) {
		this.encriptionKey = encriptionKey;
	}

	public int getMinPoints() {
		return minPoints;
	}

	public void setMinPoints(int minPoints) {
		this.minPoints = minPoints;
	}

	public String getExtradata() {
		return extradata;
	}

	public void setExtradata(String extradata) {
		this.extradata = extradata;
	}
	
	
	

}
