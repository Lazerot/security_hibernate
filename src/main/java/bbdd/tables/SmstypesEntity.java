/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="smstypes")
public class SmstypesEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="extra")
	private String extra;
	
	@Column(name="domainid")
	private Integer domainid;
	
	@Column(name="state")
	private Integer state;
	
	public SmstypesEntity() {
		
	}

	public SmstypesEntity(Integer id, String name, String extra, Integer domainid, Integer state) {
		this.id = id;
		this.name = name;
		this.extra = extra;
		this.domainid = domainid;
		this.state = state;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public Integer getDomainid() {
		return domainid;
	}

	public void setDomainid(Integer domainid) {
		this.domainid = domainid;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}
	
}
