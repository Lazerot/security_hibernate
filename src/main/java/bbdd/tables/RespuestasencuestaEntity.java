/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="respuestasencuesta")
public class RespuestasencuestaEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="operationtypeid")
	private Integer operationtypeid;
	
	@Column(name="operationid")
	private Integer operationid;
	
	@Column(name="nifcliente")
	private String nifcliente;
	
	@Column(name="questionCode")
	private String questionCode;
	
	@Column(name="questionText")
	private String quesionText;
	
	@Column(name="optionCode")
	private String optionCode;
	
	@Column(name="optionText")
	private String optionText;
	
	@Column(name="fechaRespuesta")
	private Date fechaRespesta;
	
	public RespuestasencuestaEntity() {
		
	}

	public RespuestasencuestaEntity(Integer id, Integer operationtypeid, Integer operationid, String nifcliente,
			String questionCode, String quesionText, String optionCode, String optionText, Date fechaRespesta) {
		super();
		this.id = id;
		this.operationtypeid = operationtypeid;
		this.operationid = operationid;
		this.nifcliente = nifcliente;
		this.questionCode = questionCode;
		this.quesionText = quesionText;
		this.optionCode = optionCode;
		this.optionText = optionText;
		this.fechaRespesta = fechaRespesta;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOperationtypeid() {
		return operationtypeid;
	}

	public void setOperationtypeid(Integer operationtypeid) {
		this.operationtypeid = operationtypeid;
	}

	public Integer getOperationid() {
		return operationid;
	}

	public void setOperationid(Integer operationid) {
		this.operationid = operationid;
	}

	public String getNifcliente() {
		return nifcliente;
	}

	public void setNifcliente(String nifcliente) {
		this.nifcliente = nifcliente;
	}

	public String getQuestionCode() {
		return questionCode;
	}

	public void setQuestionCode(String questionCode) {
		this.questionCode = questionCode;
	}

	public String getQuesionText() {
		return quesionText;
	}

	public void setQuesionText(String quesionText) {
		this.quesionText = quesionText;
	}

	public String getOptionCode() {
		return optionCode;
	}

	public void setOptionCode(String optionCode) {
		this.optionCode = optionCode;
	}

	public String getOptionText() {
		return optionText;
	}

	public void setOptionText(String optionText) {
		this.optionText = optionText;
	}

	public Date getFechaRespesta() {
		return fechaRespesta;
	}

	public void setFechaRespesta(Date fechaRespesta) {
		this.fechaRespesta = fechaRespesta;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
