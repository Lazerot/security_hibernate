/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="policytype")
public class PolicytypeEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="idbiosignconfig")
	private Integer idbiosignconfig;
	
	@Column(name="policyname")
	private String policyname;
	
	@Column(name="policymetadata")
	private String policymetadata;
	
	@Column(name="idcompany")
	private Integer idcompany;
	
	@Column(name="policyfamily")
	private String policyfamiliy;
	
	public PolicytypeEntity() {
		
	}

	public PolicytypeEntity(Integer id, Integer idbiosignconfig, String policyname, String policymetadata, Integer idcompany,
			String policyfamiliy) {
		this.id = id;
		this.idbiosignconfig = idbiosignconfig;
		this.policyname = policyname;
		this.policymetadata = policymetadata;
		this.idcompany = idcompany;
		this.policyfamiliy = policyfamiliy;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdbiosignconfig() {
		return idbiosignconfig;
	}

	public void setIdbiosignconfig(Integer idbiosignconfig) {
		this.idbiosignconfig = idbiosignconfig;
	}

	public String getPolicyname() {
		return policyname;
	}

	public void setPolicyname(String policyname) {
		this.policyname = policyname;
	}

	public String getPolicymetadata() {
		return policymetadata;
	}

	public void setPolicymetadata(String policymetadata) {
		this.policymetadata = policymetadata;
	}

	public Integer getIdcompany() {
		return idcompany;
	}

	public void setIdcompany(Integer idcompany) {
		this.idcompany = idcompany;
	}

	public String getPolicyfamiliy() {
		return policyfamiliy;
	}

	public void setPolicyfamiliy(String policyfamiliy) {
		this.policyfamiliy = policyfamiliy;
	}
	
}
