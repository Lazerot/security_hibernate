/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="arcoperationevent")
public class ArcoperationeventEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="operationid")
	private int operationid;
	
	@Column(name="info")
	private String info;
	
	@Column(name="xml")
	private String xml;
	
	@Column(name="eventtype")
	private String eventtype;
	
	@Column(name="eventsequence")
	private int eventsequence;
	
	public ArcoperationeventEntity() {
		
	}

	public ArcoperationeventEntity(int id, int operationid, String info, String xml, String eventtype,
			int eventsequence) {
		this.id = id;
		this.operationid = operationid;
		this.info = info;
		this.xml = xml;
		this.eventtype = eventtype;
		this.eventsequence = eventsequence;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOperationid() {
		return operationid;
	}

	public void setOperationid(int operationid) {
		this.operationid = operationid;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public String getEventtype() {
		return eventtype;
	}

	public void setEventtype(String eventtype) {
		this.eventtype = eventtype;
	}

	public int getEventsequence() {
		return eventsequence;
	}

	public void setEventsequence(int eventsequence) {
		this.eventsequence = eventsequence;
	}
	
	
}
