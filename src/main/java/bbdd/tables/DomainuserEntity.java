/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import static java.util.stream.Collectors.toList;


/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="domainuser")
public class DomainuserEntity implements Serializable, UserDetails {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="domainid")
	private Integer domainid;
	
	@Column(name="username")
	private String username;
	
	@Column(name="useremail")
	private String useremail;
	
	@Column(name="userpassword")
	private String userpassword;
	
	@Column(name="useralias")
	private String useralias;
	
	@Column(name="role")
	private Integer role;
	
	@Column(name="state")
	private Integer state;
	
	@Column(name="creationdate")
	private Date creationdate;
	
	@Column(name="icon")
	private String icon;
	
	@Column(name="cellphone")
	private String cellphone;
	
	@Column(name="groupid")
	private Integer groupid;
	
	@Column(name="publickey")
	private String publickey;
	
	@Column(name="privatekey")
	private String privatekey;
	
	@Column(name="passwordsalt")
	private String passwordsalt;
	
	@Column(name="userfirstname")
	private String userfirstname;
	
	@Column(name="userlastname1")
	private String userlastname1;
	
	@Column(name="userlastname2")
	private String userlastname2;
	
	@Column(name="userdocument")
	private String userdocument;
	
	@Column(name="userphone")
	private String userphone;
	
	@Column(name="extra")
	private String extra;
	
	@Column(name="ldapuser")
	private Integer ldapuser;
	
	@Column(name="ldapdn")
	private String ldapdn;
	
	@Column(name="islogged")
	private Integer islogged;
	
	@Column(name="lastlogindate")
	private Date lastlogindate;
	
	@Column(name="loginattemps")
	private Integer loginattemps;
	
	@ElementCollection(fetch = FetchType.EAGER)
//    @Builder.Default
    private List<String> roles = new ArrayList<>();
	
	public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.roles.stream().map(SimpleGrantedAuthority::new).collect(toList());
    }
	
	public DomainuserEntity() {
		
	}

	public DomainuserEntity(Integer id, Integer domainid, String username, String useremail, String userpassword,
			String useralias, Integer role, Integer state, Date creationdate, String icon, String cellphone,
			Integer groupid, String publickey, String privatekey, String passwordsalt, String userfirstname,
			String userlastname1, String userlastname2, String userdocument, String userphone, String extra,
			Integer ldapuser, String ldapdn, Integer islogged, Date lastlogindate, Integer loginattemps) {
		this.id = id;
		this.domainid = domainid;
		this.username = username;
		this.useremail = useremail;
		this.userpassword = userpassword;
		this.useralias = useralias;
		this.role = role;
		this.state = state;
		this.creationdate = creationdate;
		this.icon = icon;
		this.cellphone = cellphone;
		this.groupid = groupid;
		this.publickey = publickey;
		this.privatekey = privatekey;
		this.passwordsalt = passwordsalt;
		this.userfirstname = userfirstname;
		this.userlastname1 = userlastname1;
		this.userlastname2 = userlastname2;
		this.userdocument = userdocument;
		this.userphone = userphone;
		this.extra = extra;
		this.ldapuser = ldapuser;
		this.ldapdn = ldapdn;
		this.islogged = islogged;
		this.lastlogindate = lastlogindate;
		this.loginattemps = loginattemps;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDomainid() {
		return domainid;
	}

	public void setDomainid(Integer domainid) {
		this.domainid = domainid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUseremail() {
		return useremail;
	}

	public void setUseremail(String useremail) {
		this.useremail = useremail;
	}

	public String getUserpassword() {
		return userpassword;
	}

	public void setUserpassword(String userpassword) {
		this.userpassword = userpassword;
	}

	public String getUseralias() {
		return useralias;
	}

	public void setUseralias(String useralias) {
		this.useralias = useralias;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getCellphone() {
		return cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	public Integer getGroupid() {
		return groupid;
	}

	public void setGroupid(Integer groupid) {
		this.groupid = groupid;
	}

	public String getPublickey() {
		return publickey;
	}

	public void setPublickey(String publickey) {
		this.publickey = publickey;
	}

	public String getPrivatekey() {
		return privatekey;
	}

	public void setPrivatekey(String privatekey) {
		this.privatekey = privatekey;
	}

	public String getPasswordsalt() {
		return passwordsalt;
	}

	public void setPasswordsalt(String passwordsalt) {
		this.passwordsalt = passwordsalt;
	}

	public String getUserfirstname() {
		return userfirstname;
	}

	public void setUserfirstname(String userfirstname) {
		this.userfirstname = userfirstname;
	}

	public String getUserlastname1() {
		return userlastname1;
	}

	public void setUserlastname1(String userlastname1) {
		this.userlastname1 = userlastname1;
	}

	public String getUserlastname2() {
		return userlastname2;
	}

	public void setUserlastname2(String userlastname2) {
		this.userlastname2 = userlastname2;
	}

	public String getUserdocument() {
		return userdocument;
	}

	public void setUserdocument(String userdocument) {
		this.userdocument = userdocument;
	}

	public String getUserphone() {
		return userphone;
	}

	public void setUserphone(String userphone) {
		this.userphone = userphone;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public Integer getLdapuser() {
		return ldapuser;
	}

	public void setLdapuser(Integer ldapuser) {
		this.ldapuser = ldapuser;
	}

	public String getLdapdn() {
		return ldapdn;
	}

	public void setLdapdn(String ldapdn) {
		this.ldapdn = ldapdn;
	}

	public Integer getIslogged() {
		return islogged;
	}

	public void setIslogged(Integer islogged) {
		this.islogged = islogged;
	}

	public Date getLastlogindate() {
		return lastlogindate;
	}

	public void setLastlogindate(Date lastlogindate) {
		this.lastlogindate = lastlogindate;
	}

	public Integer getLoginattemps() {
		return loginattemps;
	}

	public void setLoginattemps(Integer loginattemps) {
		this.loginattemps = loginattemps;
	}

	public Object orElseThrow(Object object) {
		// TODO Auto-generated method stub
		return null;
	}

//Prueba
	/**
	 * @return the roles
	 */
	public List<String> getRoles() {
		return roles;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	
}
