/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="ldapconfig")
public class LdapconfigEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="domainid")
	private Integer domainid;
	
	@Column(name="idapname")
	private String idapname;
	
	@Column(name="idapdata")
	private String idapdata;
	
	@Column(name="idapuuid")
	private String idapuuid;
	
	@Column(name="status")
	private Integer status;
	
	public LdapconfigEntity() {
		
	}

	public LdapconfigEntity(Integer id, Integer domainid, String idapname, String idapdata, String idapuuid,
			Integer status) {
		this.id = id;
		this.domainid = domainid;
		this.idapname = idapname;
		this.idapdata = idapdata;
		this.idapuuid = idapuuid;
		this.status = status;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDomainid() {
		return domainid;
	}

	public void setDomainid(Integer domainid) {
		this.domainid = domainid;
	}

	public String getIdapname() {
		return idapname;
	}

	public void setIdapname(String idapname) {
		this.idapname = idapname;
	}

	public String getIdapdata() {
		return idapdata;
	}

	public void setIdapdata(String idapdata) {
		this.idapdata = idapdata;
	}

	public String getIdapuuid() {
		return idapuuid;
	}

	public void setIdapuuid(String idapuuid) {
		this.idapuuid = idapuuid;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
