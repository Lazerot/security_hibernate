/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="notario")
public class NotarioEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="notariouuid")
	private String notariouuid;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="apellidos")
	private String apellidos;
	
	@Column(name="identificador")
	private String identificador;
	
	@Column(name="clave")
	private String clave;
	
	@Column(name="email")
	private String email;
	
	@Column(name="estado")
	private Integer estado;
	
	@Column(name="fechaalta")
	private Date fechaalta;
	
	@Column(name="extra")
	private String extra;
	
	public NotarioEntity () {
		
	}

	public NotarioEntity(Integer id, String notariouuid, String nombre, String apellidos, String identificador,
			String clave, String email, Integer estado, Date fechaalta, String extra) {
		this.id = id;
		this.notariouuid = notariouuid;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.identificador = identificador;
		this.clave = clave;
		this.email = email;
		this.estado = estado;
		this.fechaalta = fechaalta;
		this.extra = extra;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNotariouuid() {
		return notariouuid;
	}

	public void setNotariouuid(String notariouuid) {
		this.notariouuid = notariouuid;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Date getFechaalta() {
		return fechaalta;
	}

	public void setFechaalta(Date fechaalta) {
		this.fechaalta = fechaalta;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
