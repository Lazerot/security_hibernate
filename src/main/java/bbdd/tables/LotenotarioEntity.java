/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="lotenotario")
public class LotenotarioEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="identificador")
	private String identificador;
	
	@Column(name="iddominio")
	private Integer iddominio;
	
	@Column(name="fechacreacion")
	private Date fechacreacion;
	
	@Column(name="fechadesde")
	private Date fechadesde;
	
	@Column(name="fechahasta")
	private Date fechahasta;
	
	@Column(name="numerooperaciones")
	private Integer numerooperaciones;
	
	@Column(name="archivolote")
	private String archivolote;
	
	@Column(name="estado")
	private Integer estado;
	
	@Column(name="idnotario")
	private Integer idnotario;
	
	@Column(name="fechaoperacion")
	private Date fechaoperacion;
	
	@Column(name="fechafirma")
	private Date fechafirma;
	
	@Column(name="extra")
	private String extra;
	
	@Column(name="hasxml")
	private String hasxml;
	
	@Column(name="signaturexml")
	private String signaturexml;
	
	public LotenotarioEntity() {
		
	}

	public LotenotarioEntity(Integer id, String identificador, Integer iddominio, Date fechacreacion, Date fechadesde,
			Date fechahasta, Integer numerooperaciones, String archivolote, Integer estado, Integer idnotario,
			Date fechaoperacion, Date fechafirma, String extra, String hasxml, String signaturexml) {
		this.id = id;
		this.identificador = identificador;
		this.iddominio = iddominio;
		this.fechacreacion = fechacreacion;
		this.fechadesde = fechadesde;
		this.fechahasta = fechahasta;
		this.numerooperaciones = numerooperaciones;
		this.archivolote = archivolote;
		this.estado = estado;
		this.idnotario = idnotario;
		this.fechaoperacion = fechaoperacion;
		this.fechafirma = fechafirma;
		this.extra = extra;
		this.hasxml = hasxml;
		this.signaturexml = signaturexml;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public Integer getIddominio() {
		return iddominio;
	}

	public void setIddominio(Integer iddominio) {
		this.iddominio = iddominio;
	}

	public Date getFechacreacion() {
		return fechacreacion;
	}

	public void setFechacreacion(Date fechacreacion) {
		this.fechacreacion = fechacreacion;
	}

	public Date getFechadesde() {
		return fechadesde;
	}

	public void setFechadesde(Date fechadesde) {
		this.fechadesde = fechadesde;
	}

	public Date getFechahasta() {
		return fechahasta;
	}

	public void setFechahasta(Date fechahasta) {
		this.fechahasta = fechahasta;
	}

	public Integer getNumerooperaciones() {
		return numerooperaciones;
	}

	public void setNumerooperaciones(Integer numerooperaciones) {
		this.numerooperaciones = numerooperaciones;
	}

	public String getArchivolote() {
		return archivolote;
	}

	public void setArchivolote(String archivolote) {
		this.archivolote = archivolote;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Integer getIdnotario() {
		return idnotario;
	}

	public void setIdnotario(Integer idnotario) {
		this.idnotario = idnotario;
	}

	public Date getFechaoperacion() {
		return fechaoperacion;
	}

	public void setFechaoperacion(Date fechaoperacion) {
		this.fechaoperacion = fechaoperacion;
	}

	public Date getFechafirma() {
		return fechafirma;
	}

	public void setFechafirma(Date fechafirma) {
		this.fechafirma = fechafirma;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public String getHasxml() {
		return hasxml;
	}

	public void setHasxml(String hasxml) {
		this.hasxml = hasxml;
	}

	public String getSignaturexml() {
		return signaturexml;
	}

	public void setSignaturexml(String signaturexml) {
		this.signaturexml = signaturexml;
	}
	
}
