/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="smsstatistics")
public class SmsstatisticsEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="statsname")
	private String statsname;
	
	@Column(name="domainid")
	private Integer domainid;
	
	@Column(name="statstype")
	private Integer statstype;
	
	@Column(name="timeelement")
	private Integer timeelement;
	
	@Column(name="fromdate")
	private Date fromdate;
	
	@Column(name="todate")
	private String todate;
	
	@Column(name="statsinfo")
	private String statsinfo;
	
	public SmsstatisticsEntity() {
		
	}

	public SmsstatisticsEntity(Integer id, String statsname, Integer domainid, Integer statstype, Integer timeelement,
			Date fromdate, String todate, String statsinfo) {
		this.id = id;
		this.statsname = statsname;
		this.domainid = domainid;
		this.statstype = statstype;
		this.timeelement = timeelement;
		this.fromdate = fromdate;
		this.todate = todate;
		this.statsinfo = statsinfo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStatsname() {
		return statsname;
	}

	public void setStatsname(String statsname) {
		this.statsname = statsname;
	}

	public Integer getDomainid() {
		return domainid;
	}

	public void setDomainid(Integer domainid) {
		this.domainid = domainid;
	}

	public Integer getStatstype() {
		return statstype;
	}

	public void setStatstype(Integer statstype) {
		this.statstype = statstype;
	}

	public Integer getTimeelement() {
		return timeelement;
	}

	public void setTimeelement(Integer timeelement) {
		this.timeelement = timeelement;
	}

	public Date getFromdate() {
		return fromdate;
	}

	public void setFromdate(Date fromdate) {
		this.fromdate = fromdate;
	}

	public String getTodate() {
		return todate;
	}

	public void setTodate(String todate) {
		this.todate = todate;
	}

	public String getStatsinfo() {
		return statsinfo;
	}

	public void setStatsinfo(String statsinfo) {
		this.statsinfo = statsinfo;
	}
	
	

}
