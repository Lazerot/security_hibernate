/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="vccustomer")
public class VccustomerEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="uuid")
	private String uuid;
	
	@Column(name="customercompany")
	private String customercompany;
	
	@Column(name="customertype")
	private String customertype;
	
	@Column(name="customercode")
	private String customercode;
	
	@Column(name="customerfirstname")
	private String customerfirstname;
	
	@Column(name="customerlastname")
	private String customerlastname;
	
	@Column(name="customerphone")
	private String customerphone;
	
	@Column(name="customeralias")
	private String customeralias;
	
	@Column(name="customeremail")
	private String customeremail;
	
	@Column(name="customerchannel")
	private String customerchannel;
	
	@Column(name="customermultichannelcompany")
	private String customermultichannelcompany;
	
	@Column(name="customermultichannelcenter")
	private String customermultichannelcenter;
	
	@Column(name="customermultichannelproduct")
	private String customermultichannelproduct;
	
	@Column(name="customermultichannelcontract")
	private String customermultichannelcontract;
	
	@Column(name="creationdate")
	private Date creationdate;
	
	@Column(name="videocallagentid")
	private String videocallagentid;
	
	@Column(name="state")
	private Integer state;
	
	public VccustomerEntity() {
		
	}

	public VccustomerEntity(Integer id, String uuid, String customercompany, String customertype, String customercode,
			String customerfirstname, String customerlastname, String customerphone, String customeralias,
			String customeremail, String customerchannel, String customermultichannelcompany,
			String customermultichannelcenter, String customermultichannelproduct, String customermultichannelcontract,
			Date creationdate, String videocallagentid, Integer state) {
		this.id = id;
		this.uuid = uuid;
		this.customercompany = customercompany;
		this.customertype = customertype;
		this.customercode = customercode;
		this.customerfirstname = customerfirstname;
		this.customerlastname = customerlastname;
		this.customerphone = customerphone;
		this.customeralias = customeralias;
		this.customeremail = customeremail;
		this.customerchannel = customerchannel;
		this.customermultichannelcompany = customermultichannelcompany;
		this.customermultichannelcenter = customermultichannelcenter;
		this.customermultichannelproduct = customermultichannelproduct;
		this.customermultichannelcontract = customermultichannelcontract;
		this.creationdate = creationdate;
		this.videocallagentid = videocallagentid;
		this.state = state;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getCustomercompany() {
		return customercompany;
	}

	public void setCustomercompany(String customercompany) {
		this.customercompany = customercompany;
	}

	public String getCustomertype() {
		return customertype;
	}

	public void setCustomertype(String customertype) {
		this.customertype = customertype;
	}

	public String getCustomercode() {
		return customercode;
	}

	public void setCustomercode(String customercode) {
		this.customercode = customercode;
	}

	public String getCustomerfirstname() {
		return customerfirstname;
	}

	public void setCustomerfirstname(String customerfirstname) {
		this.customerfirstname = customerfirstname;
	}

	public String getCustomerlastname() {
		return customerlastname;
	}

	public void setCustomerlastname(String customerlastname) {
		this.customerlastname = customerlastname;
	}

	public String getCustomerphone() {
		return customerphone;
	}

	public void setCustomerphone(String customerphone) {
		this.customerphone = customerphone;
	}

	public String getCustomeralias() {
		return customeralias;
	}

	public void setCustomeralias(String customeralias) {
		this.customeralias = customeralias;
	}

	public String getCustomeremail() {
		return customeremail;
	}

	public void setCustomeremail(String customeremail) {
		this.customeremail = customeremail;
	}

	public String getCustomerchannel() {
		return customerchannel;
	}

	public void setCustomerchannel(String customerchannel) {
		this.customerchannel = customerchannel;
	}

	public String getCustomermultichannelcompany() {
		return customermultichannelcompany;
	}

	public void setCustomermultichannelcompany(String customermultichannelcompany) {
		this.customermultichannelcompany = customermultichannelcompany;
	}

	public String getCustomermultichannelcenter() {
		return customermultichannelcenter;
	}

	public void setCustomermultichannelcenter(String customermultichannelcenter) {
		this.customermultichannelcenter = customermultichannelcenter;
	}

	public String getCustomermultichannelproduct() {
		return customermultichannelproduct;
	}

	public void setCustomermultichannelproduct(String customermultichannelproduct) {
		this.customermultichannelproduct = customermultichannelproduct;
	}

	public String getCustomermultichannelcontract() {
		return customermultichannelcontract;
	}

	public void setCustomermultichannelcontract(String customermultichannelcontract) {
		this.customermultichannelcontract = customermultichannelcontract;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public String getVideocallagentid() {
		return videocallagentid;
	}

	public void setVideocallagentid(String videocallagentid) {
		this.videocallagentid = videocallagentid;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}
	
	
}
