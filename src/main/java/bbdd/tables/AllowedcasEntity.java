/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */

@Entity
@Table(name="allowedcas")
public class AllowedcasEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="uuid")
	private String uuid;
	
	@Column(name="name")
	private String name;

	@Column(name="cn")
	private String cn;
	
	@Column(name="info")
	private String info;
	
	@Column(name="mapping")
	private String mapping;
	
	@Column(name="domainid")
	private int domainid;
	
	public AllowedcasEntity() {
		
	}

	public AllowedcasEntity(int id, String uuid, String name, String cn, String info, String mapping, int domainid) {
		this.id = id;
		this.uuid = uuid;
		this.name = name;
		this.cn = cn;
		this.info = info;
		this.mapping = mapping;
		this.domainid = domainid;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getMapping() {
		return mapping;
	}

	public void setMapping(String mapping) {
		this.mapping = mapping;
	}

	public int getDomainid() {
		return domainid;
	}

	public void setDomainid(int domainid) {
		this.domainid = domainid;
	}
	
	
	
}
