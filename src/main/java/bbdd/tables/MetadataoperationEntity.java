/**
 * 
 */
package bbdd.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pmartinez
 *
 */
@Entity
@Table(name="metadataoperation")
public class MetadataoperationEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="operationid")
	private Integer operationid;
	
	@Column(name="tagname")
	private String tagname;
	
	@Column(name="metadatavalue")
	private String metadatavalue;
	
	@Column(name="domainid")
	private Integer domainid;
	
	public MetadataoperationEntity() {
		
	}

	public MetadataoperationEntity(Integer id, Integer operationid, String tagname, String metadatavalue,
			Integer domainid) {
		this.id = id;
		this.operationid = operationid;
		this.tagname = tagname;
		this.metadatavalue = metadatavalue;
		this.domainid = domainid;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOperationid() {
		return operationid;
	}

	public void setOperationid(Integer operationid) {
		this.operationid = operationid;
	}

	public String getTagname() {
		return tagname;
	}

	public void setTagname(String tagname) {
		this.tagname = tagname;
	}

	public String getMetadatavalue() {
		return metadatavalue;
	}

	public void setMetadatavalue(String metadatavalue) {
		this.metadatavalue = metadatavalue;
	}

	public Integer getDomainid() {
		return domainid;
	}

	public void setDomainid(Integer domainid) {
		this.domainid = domainid;
	}
	
}
 