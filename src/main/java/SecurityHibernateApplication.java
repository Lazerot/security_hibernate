

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })

@ComponentScan(basePackages = {
		"pruebas",
        "bbdd.dto",  // MVC @Configuration
        "bbdd.tables", // Security @Configuration
        "services.dao", // Database @Configuration -> does Entity Scan and Repository scan
        "utils",
})
//@EnableJpaRepositories(basePackages = "security_hibernate")
//@EntityScan(basePackages = "dao.tables")
public class SecurityHibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurityHibernateApplication.class, args);
	}

}
