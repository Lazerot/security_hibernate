package pruebas;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

//Controller
@RestController
public class Hello_World {

	//GET
	//URI - /hello-world
	//Method  - "Hello world"
	
	@GetMapping (path = "/hello-world")
	public String helloWorld() {
		return "Hello World";
	}
	
}

