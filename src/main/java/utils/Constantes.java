/**
 * 
 */
package utils;

/**
 * @author pj
 *
 */
public class Constantes {
	
	 //Rol de plugin
	 public static final int ROLE_PLUGIN = 0;

	 public static final int ROLE_SUPER = 3;

	 public static final int ROLE_ADMIN = 2;

	 public static final int ROLE_NORMAL = 1;

	 public static final int ROLE_VIDEO = 0;

	 public static final int ROLE_SUPERVISOR = -1;

	 public static final int ROLE_VISOR = -2;

	 public static final int ROLE_FIRMANTE = -3;

	 public static final int ROLE_VALIDATOR = -4;
	 
	 
	 public static final int TOKEN_TIMEOUT = 20; // 20 MINUTES
	 
	 //MIME
	 public static final String MIME = "application/octet-stream";
}
