/**
 * 
 */
package utils;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import services.digitalFinger.FingerPrintInfoXml;

/**
 * @author pj
 *
 */
public class Xml {

	public static String jaxbArchiveInfoToXML(FingerPrintInfoXml finger) {
    	 
        try 
        {
        	JAXBContext context = JAXBContext.newInstance(FingerPrintInfoXml.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            //for pretty-print XML in JAXB
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            // Write to String
            java.io.StringWriter sw = new StringWriter();
            m.marshal(finger, sw);
            return sw.toString();
            
        } catch (JAXBException e) {
			
        }
        return "";
    }
}
