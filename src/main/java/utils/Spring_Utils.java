/**
 * 
 */
package utils;


import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import bbdd.dto.DomainUserDTO;
import bbdd.tables.OperationEntity;
import exceptions.Forbidden_exception;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * @author pj
 *
 */
@RestController
@RequestMapping("/SpringUtils")
public class Spring_Utils {
	
	//Login para personas sin administracion
	@PostMapping("/loginaut")
	@ResponseBody
	public DomainUserDTO Response (@RequestBody OperationEntity operation , HttpStatus status) throws Exception 
	{
		
		//Session de hibernate
		SessionFactory factory = utils.HibernateUtils.getSessionFactory();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		
		DomainUserDTO dto = new DomainUserDTO();
				
			if (operation.getUuid() != null) 
			{
				
				String uuid = operation.getUuid();
				try 
				{
					
				//Creamos la query que compruebe si existe el uuid (solamente) Luego, esto se pasara al token
				List<?> resultWithAliasedBean = session.createSQLQuery
						("SELECT op.uuid FROM operation op "
						+ "WHERE op.uuid = :uuid ")
						.setParameter("uuid", uuid)
						.setResultTransformer(Transformers.aliasToBean(DomainUserDTO.class))
						.list();
						
				 dto = (DomainUserDTO) resultWithAliasedBean.get(0);
				 
				 uuid = dto.getUuid();

					Date expirationDate = new Date();

					Calendar c = Calendar.getInstance();
					c.setTime(expirationDate);
					c.add(Calendar.MINUTE, Constantes.TOKEN_TIMEOUT);
					expirationDate = c.getTime();
					
				//Generamos el token con el uuid recuperado	
				 String sessionToken = Jwts.builder().setExpiration(expirationDate).setIssuedAt(new Date())
							.setIssuer("ESignO2").setSubject(uuid)
							.claim("sessionData", uuid)
							.signWith(SignatureAlgorithm.HS512, "F\\\\eAtz>hA87TNs)&".getBytes()).compact();

				//Añadimos el Token generado anteriormente, que ira al dto 			
					 dto.setToken(sessionToken);
					 
					 return dto;
					 
				}catch (Exception e)
				 		{
				 		throw new Forbidden_exception("uuid - "+ operation.getUuid());			
				 		}
			}
			
			return null;
		
	}	

}
