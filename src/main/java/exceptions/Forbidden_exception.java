/**
 * 
 */
package exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author pj
 *
 */
@ResponseStatus(HttpStatus.FORBIDDEN)

public class Forbidden_exception extends Exception{

	public Forbidden_exception (String message) {
		super(message);
	}
}
