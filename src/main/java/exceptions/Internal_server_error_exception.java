/**
 * 
 */
package exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author pj
 *
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class Internal_server_error_exception extends Exception{

	public Internal_server_error_exception (String message) {
		super(message);
	}
}
